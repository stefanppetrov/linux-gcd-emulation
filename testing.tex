\chapter{System testing}\label{chap:testing}
Testing and profiling are important parts of every software project in order to validate that the system meets its functional and non-functional requirements. Determining what tests should be conducted is directly based on the requirements as they define the expected behaviour of the system. 
\par
We are primarily concerned with black-box testing the EDA library interface according to the functional requirements. As such the organization of the tests and their results will be according to the requirement grouping and the user-facing system module they belong to. 
\par
No unit testing on internal modules will be carried out. While unit testing is useful for verifying a component's functionality in isolation, it will provide little to information about emergent behaviour from using in concert with others, unlike black-box system testing which provides some checks for the entire system's correct operation.
\par
Before covering the details of how the project was tested and the testing results we'll review the tools used and the file organization.

\section{Tooling choice}
\subsection{Profiling tools} \label{subsec:valgrind}
Profiling is a significant part of the development of high performance systems. As such it is important to choose a profiling tool which provides the programmer with all the required information at the correct level of detail.
\par On Linux Valgrind\cite{valgrind} is an extensible binary instrumentation framework originally created as a memory checker, which is useful in itself for detecting memory related problems. Over time a number of plug-ins were developed for Valgrind. The ones which were used as a part of this project were as follows:
\begin{description}
	\item [Massif] is a heap profiler which we use for determining the memory consumption of a program over its runtime. It also provides information for which function calls are the most frequent memory allocators, which makes it useful for determining where memory consumption comes from.
	\item [Callgrind / Cachegrind] are 2 closely related tools which provide information about the call graph of a process, its cache hit / miss rate, and branch prediction information. This is valuable for finding out which portions of the code consume most of the computational time acting as potential bottlenecks, either due to long computation or poor caching behaviour.
	\item [Helgrind] is a race condition detector, naturally useful for parallel software. However it is rather limited in its applicability as it is mainly concerned with correctness and races in PThreads programs\cite[sec. 7]{man-valgrind} and treats atomic operations as regular data reads or writes. This causes its output to consist of many false positives for the main implementation of EDA making it hard to pick out real data races.
\end{description}
A notable drawback of most of the Valgrind tools is that they are actually simulations and as such their results are not completely accurate and also execution time is multiple times longer. This is especially true for Callgrind and Cachegrind as in some cases the execution time would become on the order of days and the simulated process would be killed due to running out of system memory. As a result, unfortunately, complete profiling data is not always available or had to be gathered from significantly lower input sizes.

\subsection{Testing framework}
While testing can be conducted using plain programs this has the drawback that extra work has to be put towards gathering, organizing and summarizing results, and that tests will often lack a structure. A testing framework is a tool which largely resolves those issues by providing an interface to write tests in a structured manner.
\par
The test framework used is the open-source \textbf{Catch2} library. It provides the functionality to separate test cases into sections and sections which run in isolation and the ability to choose which tests should be run via command line parameters. Consisting of a single header file it is very lightweight and platform and library independent. Another reason for this choice is that it is under the Boost Software License, which is compatible with GPL\cite{gplcompat}.
\par
A minor concern is that this testing framework is actually a C++ header file. This means that the tests will actually have to be compiled in C++ instead of C. Although the 2 languages are largely compatible there are a number of cases where expected behaviour is different and valid C code being invalid C++ code, despite C++ being a (non-strict) superset of C.
\par
To avoid issues stemming from this, the \textbf{eda.h} header defining EDA library function defines them to be exported specifically as C functions so that a C++ compiler can link the library correctly. Furthermore, the tests themselves had to be written so that no C incompatible behaviour was manifested in testing code passed to EDA for execution.

\section{Code files and structure}
The testing files are in the \textbf{tests/} directory which has the the following layout:
\begin{description}
\item [tests/main.t.cpp :] This is the test suite containing the test cases for the core functionality of the library - queues, groups, loop parallelization and one-time execution.
\item [tests/source.t.cpp :] Due to the number of test cases for the core constructs and the importance of the event source submodule and the number of its own test cases, it was decided to split off the event source test suite into an independent file.
\item [tests/catch.hpp :] \textbf{Catch2} testing framework, listed for completeness.
\end{description}
All of these files are provided as part of the supplementary material to this report. For convenience the testing binaries are available under the \textbf{bin/} directory.

\section{Queues}
As queues are the piece of functionality upon which most of the other functionality relies we'll start with them.

\subsection{Creating / getting a queue}
The correctness test for creating a queue or obtaining a predefined one is a trivial check which we'll do in every test for a queue and will not be discussed further.

\subsection{Asynchronous dispatch}
\subsubsection{Parallel} \label{subsubsec:adp}
Testing asynchronous dispatch for parallel queues must verify that
\begin{enumerate*}
\item All dispatched tasks are executed;
\item Tasks are executed in parallel if possible.
\end{enumerate*}
\par
We dispatch $N$ tasks on a system predefined parallel queue. Each of the tasks gets a unique index to an array of $N$ integers, which all have the value 1 initially. Each task sets the array element pointed to by their index to 0. The main thread waits until all array elements become 0. This way there are no unprotected parallel writes to the same memory location.
\par
In order to verify that the tasks are executed in parallel each task sleeps for some measurable time duration $S$ before setting its array element to 0. 
\par
After all tasks have finished running their total measured runtime $T$ should satisfy $T < N*S$, and all array elements should be 0.

\subsubsection{Serial (single)}\label{subsubsec:ads}
Testing asynchronous dispatch for parallel queues must verify that
\begin{enumerate*}
\item All dispatched tasks are executed;
\item Tasks are executed strictly serially.
\end{enumerate*}
\par
$N$ tasks are dispatched on the serial queue. To check that there is no parallel execution we use a PThreads semaphore initially set to 1, and a counter also set to 0. Each dispatched task attempts to acquire the semaphore, and if successful then sleeps for some non-negligible duration $S$ before finally incrementing the counter and releasing the semaphore. 
\par
Tasks which fail to acquire the semaphore return immediately. The reasoning behind this is that if there are 2 tasks executing in parallel they will both try acquiring the semaphore but only 1 will succeed, preventing the other from incrementing the counter. $S$ should be sufficiently large to ensure failed semaphore acquisition if there is parallel execution.
\par
For this test to be successful the measured runtime $T$ of the tasks should be $T \geq S*N$ and the counter value should be $N$.

\subsubsection{Serial (multiple)}
There is also the requirement that multiple serial queues should execute in parallel with respect to each other, which requires a different test case. 
\par
For this test we use 2 serial queues and an atomic shared counter set to 0. $N$ identical tasks are asynchronously dispatched to both queues. The tasks sleep for some measurable time period $S$ before incrementing the counter. The logic for verifying serial execution per queue is the same as in subsection \ref{subsubsec:ads};
\par
The success criteria for the test is the counter value being $2*N$ after all tasks finish execution, and the total observed runtime $T$ satisfying $N*S \leq T < 2*N*S$.

\subsection{Synchronous dispatch}
\subsubsection{Parallel (isolated)} \label{subsubsec:sdpna}
Requirements tested are a superset of the ones for the asynchronous case, with the addition being that we should block on the dispatch call until the task is complete. 
\par
We dispatch $N$ tasks which sleep for some duration $S$ and increment a counter. The main thread does not wait for tasks to complete after dispatching them, and shouldn't need to due to them being blocking.
\par
In order to verify synchronously execution we check that the counter is equal to the number of tasks dispatched so far after each dispatch call, and that it is $N$ after all tasks are dispatched. Additionally, the total test runtime $T$ must satisfy $T \geq S*N$, and the runtime $T_{i}$ of each synchronous dispatch call should satisfy $T_{i} \geq S$.

\subsubsection{Parallel (with asynchronous)}
In this test case we have to ensure that both the asynchronously and the synchronously dispatched tasks all complete. It is is simply a combination of the tests from subsections \ref{subsubsec:adp} and \ref{subsubsec:sdpna}.

\subsubsection{Serial (isolated)}
Practically identical to the test from synchronous dispatch for parallel queues from subsection \ref{subsubsec:sdpna}. The difference being serial execution which is done using a semaphore, see subsection \ref{subsubsec:ads};

\subsubsection{Serial (with async)}
Asynchronous tasks may slow down the execution of synchronous ones and vice versa, but in the presence of either type of tasks all tasks should complete.
\par
$N$ tasks are dispatched asynchronously from another thread, and $N$ are synchronously scheduled from the main thread. The tasks will sleep for some non-negligible duration $S$ and increment a counter. Asynchronous tasks and synchronous ones do not share a counter. 
\par
In order for the test to be successful both counters have to be equal to $N$ after the tasks finish execution, the total runtime $T$ should satisfy $2*N*S \leq T$. Additionally, after each synchronous dispatch the corresponding counter should be equal to the number of synchronous tasks dispatched so far, and that all synchronous tasks had a runtime $T_{i}$ such that $T_{i} \geq S$.

\subsection{Scheduling policies}
As this is not directly observable behaviour, there aren't any tests we can use to verify it on its own. For parallel queues only 1 uncooperative scheduling is supported so we've already tested that implicitly. Serial queues support greedy scheduling, as the default, and cooperative scheduling. Tests for cooperative serial queues are the same as the greedy scheduling ones and as such they will not be discussed further.

\section{Task groups}
Task groups are somewhat specific in that dispatching a task as a part of the group does not on itself constitute any observable behaviour. As a result in order to verify the correctness of groups we have to test the expected behaviour of wait and task deference functionality.

\subsection{Wait (no timeout)} \label{subsec:test-wait}
For waiting without timeout we have to verify that the call blocks until all tasks dispatched as a part of the group have returned.
\subsubsection{Parallel queues}
There are $N$ short tasks which sleep for some time $S$ and increment an atomic counter, and 1 long task which sleeps a duration $S'=N*S$ before incrementing the counter. The long task is dispatched and then the short ones all as a part of a group. The main thread then waits until the group finishes. The success criteria are the counter to have a value of $N+1$, and the total runtime $T$ to be such that $S' \leq T < N*S+S'$.
\subsubsection{Sequential queues}
More or less the same but there is no long task, as tasks are executed on a serial queue. Therefore the success criteria are the counter having the value of $N$ and total running time $T >= N*S$.
\subsubsection{Combined}
This is a combined test for ensuring that group tasks dispatched from another thread and to multiple queues still cause waits in the current one.
\par
$N$ redirection tasks are dispatched to a parallel queue as part of a group, which once executed dispatch a sleep-and-increment tasks with a sleep duration $N$ to a serial queue as part of the group. The main thread waits on the group.
\par
The success criteria are the runtime of the test being $T >= N*S$, the counter incremented being equal to $N$, and the time to dispatch the increment tasks $T'< T$.

\subsection{Wait (with timeout)}
A copy of the tests from subsection \ref{subsec:test-wait}. The single wait call is replaced with a loop over waits with a timeout value equivalent to $S$, and an a wait with instant timeout before the loop. This is sufficient for covering wait calls which both time out and are woken up before timing out. Success criteria are those of the original test, with the addition of the instant timeout wait failing.

\subsection{Deferred dispatch} \label{subsec:test-defer}
For deferred dispatch we have to verify that
\begin{enumerate*}
\item All tasks deferred while the group is active are indeed deferred;
\item No task remains deferred indefinitely if the group has completed;
\item Tasks deferred when the group is empty get dispatched immediately.
\end{enumerate*}
\subsubsection{Guaranteed deferral}
For this test case we emulate tasks on the group by joining it manually and leaving it after some time. $N$ tasks are deferred during the delay in parallel, which will try incrementing a counter. The group is left only after all tasks are deferred.
\par
The success criteria is that before leaving the group the counter is 0, and that some time after the group is left the counter is $N$. 
\subsubsection{No deferral}
$N$ tasks are deferred on an empty group, which try incrementing a counter. The counter should be equal to $N$ at some point afterwards. 
\subsubsection{Combined}
The same as case the guaranteed deferral case but we do not wait for all tasks to be deferred before leaving the group, and after the initial leave we toggle it a preset number of times. This covers all requirements, and potentially triggers some edge cases in the deferral algorithm.

\section{Loop parallelization}
\subsection{Parallel queue}
For this test case we have to verify that all iterations are made and the loop is indeed parallelized. To do so we have an array whose size is equal to the number of iterations and is set to 0. Every iteration of the parallel loop sleeps and sets the array element at the iteration index to the iteration. This test is for both static and dynamic loop schedules. The success criteria are that the total runtime of the loop is less than if it were serialized, and all array items are set to their index after the loop runs.
 
\subsection{Serial queue}
As there is no parallelization when the loop is ran on a serial queue we have to verify that it is indeed running serially. The method performing the check is identical to the one described in section \ref{subsubsec:ads} for asynchronous dispatch on serial queues.

\subsection{One-time execution}
We need to verify for one-time execution that
\begin{enumerate*}
\item The task specified is executed a single time;
\item All threads trying to execute it will block while it is running.
\end{enumerate*}
\par
For this test $N$ tasks are dispatched asynchronously on a parallel queue. The code to be executed once is a long sleep of duration $S$ followed by an increment on a flag set to 0. Each of the dispatched tasks will try to execute this, and report the value of the flag by writing it to a its exclusive element of an array. A group is used to synchronize the tasks with the main thread.
\par
The success criteria are that after the tasks finish execution the flag should have a value of 1, all tasks should have observed the value of the flag to be 1, and an execution time $T$, where $2*S > T \geq S$.

\section{Event sources}
For event sources we'll testing mostly the behaviour of specific event source types as the majority of type independent logic is not accessible otherwise. The exceptions are setting handlers on event sources and cancellation which will be tested as parts of other tests or as stand-alone tests when needed and possible. All handlers must ensure that handlers can't be set after starting the source and that the source will be cancelled only once.
\par
All source types must ensure that there is at most 1 event, registration or cancel handler running at any point in time. In order to verify this in all of the event source tests we'll use the semaphore technique for serial queues, see section \ref{subsubsec:ads}.
\par
For all event sources we must ensure that all expected events will be received either coalesced or not. If it is not possible to get the number of events precisely, then an appropriate range should be used instead.

\subsection{Signals}
Before deciding on the exact testing logic on should bear in mind one peculiarity of signals. If an signal has been delivered to the process it becomes pending. The pending signals are a set, which means that any signal delivered while it is already pending it will be discarded\cite{signal}. As a result if we depend on a signal being fired a specific number of times, care should be taken not to overlap them.

\subsubsection{Single source}
For this test we'll use a single source over the \textbf{SIGUSR1} signal. We'll attempt setting the event and registration handlers twice, once before starting the source and once after.
\par
The registration handler will fire the signal the first time, and the event handler will keep firing once per execution until it accumulates $N$ signal fires. The event handler will also sleep for some measurable duration after signaling, so as to allow detecting parallel handler execution. Note that no coalescing should occur, due to the signal being fired only by the event handler.
\par
Success criteria are the signal being handled $N$ times, no event coalescing observed, serial execution of handlers, the second attempts to set the handlers failing, and the test must finish execution in some sufficiently small time frame.

\subsubsection{Two sources}
This test must verify that multiple event sources can be attached to the same event watch. It must also verify coalescing can occur.
\par
There will be 2 signal sources on the \textbf{SIGUSR1} signal. The first will have a registration and an event handler set. Its registration handler will fire the signal for the first time, and the event handler will keep firing once per execution, until it accumulates $N$ signal fires. This event handler should not observe coalescing.
\par
The second signal source will only have an event handler set, which will count the signal fires until they reach $N$. This handler will not fire the signal, and in order to check for coalescing it will sleep for some comparatively long duration before exiting.
\par
For this test case to pass the signal counts for both sources must be $N$, event coalescing must have been observed at least once on the second source, the execution of handlers should be serial for both sources, and the test must finish execution in some sufficiently small time frame.

\subsection{Timers}
\subsubsection{Basic timer}
This test verifies that the basic behaviour of timer sources is correct.
\par
A timer firing every $10\mu sec$, beginning when the timer is started is created. It has an event handler which counts the timer fires, accounting for coalescing, and cancels the timer if they exceed $N$. The cancellation handler increments an atomic cancel counter. Two attempts to set the timer are made - 1 before starting the timer source, and one after starting it.
\par
The main success criterion is the coalesced count of timer fires $T$ satisfying $N \leq T \leq 1.1*N$. Other success criteria include the cancel counter being 1, the second attempt to set the timer failing, serial execution of handlers, and the test terminating within $N*2*10\mu sec$.

\subsubsection{Timer drift}
This test is about verifying that timer drift is within tolerable limits. The timer drift is the average deviation of the timer from its expected fire times over a given period. The tolerance is a maximum of 10\% drift for a timer firing every $1msec$ over a period of $10sec$.
\par
In order to calculate drift the event handler calculates the difference between the current time and the projected fire time of the timer. This value is then subtracted from the difference from the previous handler invocation, initially set to 0. That final difference is then added to the total drift, which we convert to the actual drift by dividing it by the non-coalesced time expiration count. The timer is set in accordance with the tolerance parameters.
\par
This test is considered to pass is drift is tolerable as defined above, the timer handler was executed serially, the cancel handler of timer executed only once, and the test terminates within 11 seconds.

\subsection{File descriptors}
For file descriptors we need to test writer and reader sources, that they write and read the same amount of data. With this test case we can also verify that event sources are canceled by errors occurring on the underlying file descriptors.
\par
In order to verify the above and avoid depending on external events we define a reader and a writer source on the corresponding file descriptors on a in-process pipe, with a capacity of a single memory page, or 4096 bytes, starting empty. 
\par
The writer event source has all 3 handlers set. The registration handler ensures the pipe is configured properly. The event handler attempts writing an entire memory page to the pipe in a loop, incrementing the write count, until an error occurs. If the error is not device busy (EAGAIN) we abort the test. On reaching or exceeding $N$ writes we cancel the source. The cancellation handler closes the write end of the pipe and signals a completion semaphore.
\par
The read source's event handler reads an entire page from the pipe in a loop, incrementing the read count, until an error occurs. The cancellation handler closes the read end of the pipe and signals the completion semaphore. This source is not cancelled by the test code.
\par
The success criteria are both the read and the write counts being $N$ or more, and being equal, the test terminating within a total of 2 seconds of waiting on the completion semaphore twice, both cancellation handlers being called once, and event handlers executing serially for each source.

\subsection{One-time cancellation} \label{subsec:test-cancel}
So far no test has verified that cancellation handlers are called only once when multiple threads attempt cancelling the source.
\par
A single signal event source source is created with a registration handler that dispatches $N$ tasks attempting to cancel the event source to a parallel queue. After that it fires the signal for the source and sleeps for some small duration before exiting. The event handler for the source, if it executes, cancels the source. The cancellation handler simply counts the number of cancellations.
\par
Using this test we get all possible interleavings for canceling the source in parallel, which have a chance of succeeding due to happening after the source is installed.
\par
The test is successful if the cancel handler was executed only once, all other handles executed serially, and execution terminate within some sufficiently small time frame.

\section{Others}
\subsection{Reference counting}
This is an essential part of the EDA system as it ensures that all resources in use are not reclaimed and yet does not leak them. As it is used implicitly in all other tests the requirement about resources not being reclaimed while in use should hold. We can't make any statements about the resources bing leaked without examining the reference counts of objects after the tests, but this does not fit well with our black-box testing approach. 
\par
The method used for verification here is the Valgrind memory checker on the test suite. If there are any leaked resources or invalid reads/writes which do not crash the process it will detect at least some of them. However, as its use slows down execution we get a number of false positives as well due to timing sensitive tests failing and thus not releasing references correctly.

\subsection{Semaphores}
As said previously we simply wrap PThreads semaphores with reference counting, and since they are expected to be correct there is little to no reason to test this functionality.

\section{Challenges and bugs identified}
As expected there were many problems uncovered over the course of testing and not all tests passed the first few runs. But having tests in the first place is what allowed identifying bugs in the code, attempting a fix, re-testing and repeating until all the entire test suite was successful.
Due to the inherent non-determinism associated with parallel programming some of the initial tests were not sufficient as different test runs could succeed or fail without changing input or code.
\par
 A major example of this was the event source cancellation test (sec. \ref{subsec:test-cancel}). Although initially it was considered to show that cancel handlers would be called exactly once, on some rare occasions of running the test suite it was noticed that either no cancel handlers would be run, or they would be called twice. This lead to the discovery that the memory order used for the atomic operations in algorithms \ref{alg:evnotify} and \ref{alg:evhandle} was not sufficiently strict and that it had to be changed to sequentially consistent.
\par
Nevertheless, not all errors were found to be directly due to non-determinism. When testing task deferral until group completion (sec. \ref{subsec:test-defer}), it was found that queue consumer threads would often try executing garbage tasks or the same task twice. Other issues were lost tasks and the queue list becoming circular.
\par
After multiple failed attempts to reason about data races in the group defer and wakeup functions, the problem turned out to be caused by correct data race prevention logic in algorithms \ref{alg:group-wakeup} and \ref{alg:group-defer}, and implicit queue invariants. The deferred tasks are placed on a stack and as such they point to the previous item on the stack, which can be any memory location. The enqueue algorithm \ref{alg:qpush} however expects that the (single item) linked list of tasks passed as an argument ends with null. The wakeup and the defer calls did not ensure that for the tasks they dispatched which caused the behaviour. This required the simple fix of setting the tasks' next pointer to null, in lines \algref{alg:group-wakeup}{wakeup:null} and \algref{alg:group-defer}{defer:null}.
\par
This just goes on to show how difficult maintaining parallel software is - one not only has to deal with parallelism problems, but also has to remember that plain serial code errors can also cause failures.

\section{Results and requirement fulfilment}
As of time of writing all the tests are passing successfully and consistently, and the system fulfil all of its high priority ``must'' and ``should'' requirements. There are a number of ``could'' requirements which are either not fulfilled or partially so. None of the ``would'' requirements were met due to lack of time. We'll discuss examples of unfulfilled low priority requirements, like \ref{enum:req-limits} for having an upper limit of consumers, as part of future work.
\par
Due to the sheer number of assertions the testing output report is not presentable neither in the main body of the report or as an appendix, so it is provided in the supplementary materials.
\par
And finally despite the test suite passing without failures we should keep in mind that the tests simply reveal the presence of bugs, but not their absence. Thus we can only say that the system works correctly with respect to our test suite, but we cannot prove it will be error free for every possible use case in real applications.