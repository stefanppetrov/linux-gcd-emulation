\chapter{Requirements specification} \label{chap:reqs}

In a typical software engineering project requirements specification and analysis are of critical importance to the success of the project, as they provide the criteria to which the product should conform to. This chapter mainly covers the approach to deriving and organizing the requirements, and provides a formal specification for the proposed EDA parallelism library. The specification shall consist of functional and non-functional requirements prioritized accordingly which provide a basis to proceed with design and implementation of the system. Discussion of the thought process behind the prioritization and notable omissions is provided where relevant.
\par
Other important benefits of specifying requirements beforehand are that it allows the system to be tested for correctness against the specification and that it sets the scope of the project, thus preventing the scope creep by defining a concrete list of requirements.

\section{Requirement elicitation}
It is important to note that due to the nature of the project some methods of requirement gathering would be inappropriate. While the project has some usability aspects such as providing a simplified programming interface for parallelism and viewing programmers as end-users, providing a fast and resource efficient parallelism library is the core project goal. As a consequence of that usability focused requirement gathering methods such as stakeholder interviews will not be a primary requirements source. Another specific of this project is that we want a produce a set of programming interfaces based on the concepts from an already existing software project.

\subsection{Existing systems}
When specifying a domain system it can be illuminating to examine previous systems in the same domain  as they are a good source of information about the domain and the potential requirements. Of course this is not always possible as there might not be publicly available existing systems. However this is not a concern in the case of this project.
\par
As mentioned previously the project deals with implementing the programming paradigm of an existing parallelism framework, namely GCD. As such a majority of the requirements will be derived from examining GCD materials and documentation. Other parallelism systems and paradigms, such as the ones in subsection \ref{subsec:related} can also be a valuable source of requirements, and may also provide a point of view not considered by GCD.
\subsection{Stakeholders}
The potential stakeholders in this case are the group of software developers working on applications targeting the Linux platform who could benefit from using task oriented parallelism library with support for event asynchronous event handling. While this is already a very narrow range of users, an even more specific example are web service developers who can benefit from parallel asynchronous event handling.

\section{Requirements analysis}
Once a sufficient number of requirements have been gathered they should be analysed, in order organize them in a structured manner. A list of requirements which is classified and prioritized correctly gives a much clearer direction of how the project should progress by allowing us to distinguish between requirements of primary importance and secondary ones.

\subsection{Classification}\label{sec:reqclass}
The two types of requirements which we will consider for this project are functional and non-functional requirements. Functional requirements postulate the functions the system should provide and its observable behaviour. Non-functional requirements typically describe operational characteristics such as performance, usability, security but can also include organizational and legal requirements. To further organize the requirements we will first give high-level requirements and list their more detailed sub-requirements under them in a hierarchical manner. This allows conceptually related requirements to be grouped together.

\subsection{Prioritization}
As discussed prioritization is an important part of the requirement analysis stage as it provides us with information as to which requirements are truly important to the success of the project. This reduces the level of risk during development by focusing on the critical requirements first and leaving beneficial but non-vital requirements for later consideration. Thus a working product can still be provided even if not completely polished. For requirement prioritization in this project we shall use the MoSCoW technique\cite{moscow}. This method for prioritization defines 4 priorities:
\begin{description}
\item [Must:] Critical requirement, if not met it causes project failure.
\item [Should:] High priority, should be included in the final product if possible to do so in the project's time frame without expending any \textbf{must} requirements.
\item [Could:] A desirable but not core requirement, that can be implemented time permitting.
\item [Would:] Future work for which the current time frame is decidedly insufficient.
\end{description}
In order for requirements to be assigned a meaningful priority by considering criteria  such as value delivered, cost and ease of implementation, likelihood of success, and dependencies with other requirements. By examining each requirement with respect to each of these criteria we can arrive at a reasonable priority for the requirement and the effects it will have on the progress of the project. In the formal list of requirements for this project they will be sorted in a descending order in terms of priority so as to further emphasize the importance of top priority requirements.
\par
With regards to the hierarchical structure discussed in section \ref{sec:reqclass} an additional restriction will be imposed on assigning priorities. Sub-requirements can have a priority higher than their parent requirement, but that priority will hold only in the case their parent requirement is met. In all other case the sub-requirement's priority should be considered the same as their parent's priority or lesser.

\section{Requirements Specification}\label{sec:freqs}
\subsection{Functional}\label{subsec:freqs}
\begin{enumerate}[label=F.\arabic*]
\item Must support scheduling tasks for execution via dispatch queues in shared memory.
	\begin{enumerate}\label{list:freqs-queue}
		\item Must support scheduling tasks for asynchronous execution on dispatch queues with zero or more parallel consumer threads at any given time, to be referred as parallel queues.
		\item Must support scheduling tasks for asynchronous execution on dispatch queues with at most one consumer thread at any given time, to be referred as serial or sequential queues.
		\item Must support scheduling tasks for synchronous (blocking) execution on parallel dispatch queues.
		\item Must support scheduling tasks for synchronous execution on serial dispatch queues.
		\item Must ensure that tasks on the same parallel dispatch queue can run in parallel if possible.
		\item Must ensure that tasks on unrelated serial dispatch queues can run in parallel with respect to the queues.
		\item Must specify a set of a priorities and their precedence which determine the order in which tasks from the queues are executed.
		\item Must be able to create valid new dispatch queues, both parallel and serial.
		\begin{enumerate}
			\item Should be able to specify the level of priority at which tasks from the queue run.
			\item Could be able to specify scheduling attributes of the queue.
			\item Could be able to set an upper limit to the number of parallel consumer threads on a parallel queue.	\label{enum:req-limits}
		\end{enumerate}
		\item Should be provide access to a number of system-defined parallel dispatch queues, no less than the number of priorities for queues and their tasks.
		\item Could support a scheduling attribute for queues which denotes a queue as cooperative. Queues with this attribute will yield their consumer threads to other queues between tasks. \label{enum:req-coop}
		\item Could support a scheduling attribute for queues which denotes a queue as uncooperative. Queues with this attribute will not yield their consumer threads until they run out of tasks. \label{enum:req-nocoop}
		\item Could support a scheduling attribute for queues which denotes a queue as greedy. Queues with this attribute will behave as uncooperative queues and will request consumer threads without regard for system. \label{enum:req-greedy}
		\item Would support suspending execution of dispatch queue.
		\item Would support resuming execution of dispatch queue.
		\item Would support submitting tasks for either asynchronous or synchronous execution, which consumed from their dispatch queue will prevent other threads from consuming from the queue until it finishes execution, and will block the current thread until other threads have finished executing tasks from the same queue. This type of tasks will be referred to as barrier tasks.
		\item Would support modifying dispatch queue attributes after its creation.
		\item Would support setting target / parent queue for another queue to offload its tasks to.
		\item Would support running a dedicated queue on the main program thread of execution.
	\end{enumerate}

\item Must support using groups of tasks as a synchronization mechanism.
	\begin{enumerate}
		\item Must support creating new empty task group.
		\item Must support scheduling tasks to a dispatch group as a part of a task group.
		\item Must support waiting on a group of tasks to complete.
		\begin{enumerate}
			\item Must finish waiting immediately if all tasks in the group have finished executing, or if the group is empty.
			\item Must finish waiting immediately if all tasks in the group have finished executing.
			\item Should support specifying an optional absolute timeout when waiting. If the timeout is equal to current system time or before it, the waiting operation shall timeout with success if all tasks have finished, or with failure otherwise.
		\end{enumerate} 
		\item Should support specifying tasks to be dispatched for execution to a given queue once a task group has finished executing or is empty.
	\end{enumerate}

\item Must support parallelizing loops over ranges as tasks scheduled to a given dispatch queue.
	\begin{enumerate}
		\item Must block calling thread until all subtasks of the loop have finished executing.
		\item Could support specifying an optional loop parallelization strategy.
		\item Would support specifying loop granularity.
	\end{enumerate}

\item Must support executing a task only once during the runtime of the process.
	\begin{enumerate}
		\item Must proceed immediately if task has already been executed.
		\item Must wait the task to finish execution if it is in progress.
	\end{enumerate}

\item Must support asynchronous event handling.
	\begin{enumerate}
		\item Must support creating a new event source.
		\begin{enumerate}
			\item Must be able to specify a valid source type.
			\item Must be able to specify an opaque event source handle.
			\item Must be able to specify a target dispatch queue responsible for executing the event source's handler.
			\item Must create event sources in a disabled state. Events should not be published by the source and it should not schedule event handling tasks to its target queue.
			\item Could be able to specify optional flags for the event sources.
		\end{enumerate}

		\item Must support setting the event handler for a given source.

		\item Must ensure that event sources have at most 1 event handler, including registration and cancellation if applicable, scheduled or running on the target queue at any given instant.

		\item Must support enabling an event source.
		\begin{enumerate}
			\item Must implicitly register the event source with the underlying system's native event notification mechanism if it hasn't already been done.
			\item Must schedule a sufficient number of handler tasks to the target queue for all pending events if any.
		\end{enumerate}

		\item Must support cancelling an event source deregistering it from the underlying system's native event notification mechanism.
		\begin{enumerate}
			\item Must not deregister the source twice if it has already been cancelled.
			\item Must not schedule any event handlers from the event source post cancellation.
			\item Should not execute any pending event handler tasks scheduled on the target queue if the source has been cancelled.
			\item Should cancel event source if an error condition occurs on the watched handle.
		\end{enumerate}

		\item Should support a signal event source type.
		\begin{enumerate}
			\item Must accept a valid signal number of an asynchronous signal as a handle when creating a signal event source.
			\item Must allow multiple event sources to be registered for the same signal number.
			\item Must ensure that the signal is only ever handled by the event handling tasks for registered event sources.
			\item Should allow the default signal handler to run if there are no signal event sources registered.
			\item Could provide the number of times the signal has been received since the last event handler invocation to the invocation of the event handler.
		\end{enumerate}

		\item Should support a timer event source type.
		\begin{enumerate}
			\item Must provide an interface for setting a timer configuration for a timer event sources providing an absolute start time with respect to some clock, and an interval.
			\item Should provide the number of times the timer has fired since the last event handler invocation to the invocation of the event handler. 
			\item Could allow an optional parameter when setting the timer specifying whether the timer firing is a one time event.
			\item Would allow the timer to be set after the event source is registered and events are delivered and event handler scheduled.
		\end{enumerate}

		\item Should support a file descriptor event source type.
		\begin{enumerate}
			\item Must accept any valid non-blocking file descriptor as a handle when creating a file descriptor event source.
			\item Must ensure that either a read readiness flag, or a write readiness flag are specified but not both are provided when creating a file descriptor event source.
			\item Must only deliver read readiness events to event source which has the read readiness flag set.
			\item Must only deliver write readiness events to event source which has the write readiness flag set.
			\item Should cancel the source if the underlying file descriptor is closed or an error condition is triggered.
			\item Could allow zero or more reader and writer event sources created with the same file descriptor.
			\item Would provide the number of bytes ready to be read from the file descriptor to the event handler of a read event source.
			\item Would provide the number of bytes available for writing on the file descriptor to the event handler of a write event source.
		\end{enumerate}

		\item Could support setting a registration handler.
		\begin{enumerate}
			\item Must run registration handler only once immediately after the event source is registered with the underlying system's event notification mechanism, but before any events have been delivered and event handlers have been scheduled.
			\item Should not run registration handler if event source has been canceled prior to that.
		\end{enumerate}

		\item Could support setting a cancellation handler.
		\begin{enumerate}
			\item Must run cancellation handler only once immediately before the event source is unregistered from the underlying system's event notification mechanism.
			\item Must not run any pending event handlers after the cancellation handler has run.
		\end{enumerate}

		\item Would support a file system event source.
		\begin{enumerate}
			\item Must accept a valid file descriptor pointing to a file system object as an handle when creating an event source.
			\item Should support a flag for file deletion events when creating the event source.
			\item Should support a flag for file modified events when creating the event source.	
			\item Should support a flag for file attribute changed events when creating the event source.
			\item Should support a flag for file linked events when creating the event source.	
			\item Should support a flag for file renamed events when creating the event source.
		\end{enumerate}
	\end{enumerate}

\item Should support guaranteed one-time execution of parallel sections of code.

\item Should support reference counting for system created objects.
	\begin{enumerate}
		\item Should support manual retaining and release of references by the user.
	\end{enumerate}

\item Could support library managed semaphores.

\end{enumerate}

\subsection{Non-functional}\label{sec:nfreq}
\begin{enumerate}[label=NF.\arabic*]
\item Must provide a system implementation in the C programming language in compliance with the GNU C11 standard or the ISO C11 standard.
\item Must compile without errors using the Clang compiler collection using version 4.0 or later.
\item Must compile and link under any Linux distribution using a Linux kernel of the 4.0 series.
\begin{enumerate}
	\item Must compile under the Debian-based Ubuntu 17.10 Linux distribution.
\end{enumerate}
\item Must produce a valid shared library object and / or a static library archive as a result of compilation.
\item Must link or load resulting library correctly when used in other binaries.
\item Must compile without errors when used in conjunction with Blocks language extension and runtime enabled. 
\item Must use the \textbf{epoll} system interface for event handling directly.
\item Should not spawn more threads than efficiently schedulable to the number of processors available, unless explicitly required. 
\item Should result in reduced code complexity when compared to equivalent code using the PThreads library. \label{nfreq:complexity}
\item Should have execution times better or comparable to that of an equivalent GCD program. \label{nfreq:performance}
\item Should have the same or greater throughput of handled events per second of an equivalent GCD program. \label{nfreq:events}
\item Should have the same or lower latency when handling event than an equivalent GCD program.\label{nfreq:latency}
\item Should be licensed under an open-source license compatible with the GPL 2 or later licenses.
\item Would provide a system implementation in the C++ programming language in compliance with the ISO C++11 standard or newer.
\item Would provide a system implementation in the Rust programming language.
\end{enumerate}