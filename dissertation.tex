\documentclass[11pt, twoside, openright, a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[backend=biber, block=ragged, sorting=nyt, style=numeric]{biblatex}
\usepackage{csquotes}
\usepackage[english]{babel}
\usepackage[inline]{enumitem}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{parskip}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage[a4paper, margin=30mm, bindingoffset=8mm, heightrounded]{geometry}
\usepackage[toc,page]{appendix}
\usepackage{algpseudocode}
\usepackage[section]{algorithm}
\usepackage[section]{placeins}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{subcaption}

\usetikzlibrary{arrows,automata}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}

\setlength{\footskip}{1cm}
\setlength{\headheight}{1cm}
\pagestyle{fancy}
\fancyhf{}
\fancyhf[RE,RO]{\thepage}
\fancyhf[HLE,HLO]{\leftmark}
\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{1pt}

\fancypagestyle{plain}{ %
  \fancyhf{} % remove everything
  \fancyhf[FRE,FRO]{\thepage}
  \renewcommand{\headrulewidth}{0pt} % remove lines as well
  \renewcommand{\footrulewidth}{1pt}
}

\setlength{\parindent}{0pt}
\renewcommand{\floatpagefraction}{.9}

\addbibresource{references.bib}

\definecolor{mGreen}{rgb}{0,0.6,0} \definecolor{mGray}{rgb}{0.5,0.5,0.5} \definecolor{mPurple}{rgb}{0.58,0,0.82} \definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}
\lstdefinestyle{cstyle}{ backgroundcolor=\color{backgroundColour}, commentstyle=\color{mGreen}, keywordstyle=\color{magenta}, numberstyle=\tiny\color{mGray}, stringstyle=\color{mPurple}, basicstyle=\footnotesize\ttfamily, breakatwhitespace=false, breaklines=true, captionpos=b, keepspaces=true, numbers=left, numbersep=5pt, showspaces=false, showstringspaces=false, showtabs=false, tabsize=2, language=C }

\lstset{style=cstyle}

\algnewcommand\Lock[1]{\State \textbf{lock} #1}
\algnewcommand\SpinLock[1]{\State \textbf{spinlock} #1}
\algnewcommand\Unlock[1]{\State \textbf{unlock} #1}

%%
%% Definitions to provide layout in the dissertation title pages
%%
\newenvironment{spaced}[1]
  {\begin{minipage}[c]{\textwidth}\vspace{#1}}
  {\end{minipage}}


\newenvironment{centrespaced}[2]
  {\begin{center}\begin{minipage}[c]{#1}\vspace{#2}}
  {\end{minipage}\end{center}}


\newcommand{\declaration}[2]{
  \thispagestyle{empty}
  \begin{spaced}{4em}
    \begin{center}
      \LARGE\textbf{#1}
    \end{center}
  \end{spaced}
  \begin{spaced}{3em}
    \begin{center}
      Submitted by: #2
    \end{center}
  \end{spaced}
  \begin{spaced}{5em}
    \section*{COPYRIGHT}

    Attention is drawn to the fact that copyright of this dissertation rests
    with its author. The Intellectual Property Rights of the products
    produced as part of the project belong to the author unless otherwise specified
    below, in accordance with the University of Bath's policy on intellectual property 
   (see http://www.bath.ac.uk/ordinances/22.pdf).

    This copy of the dissertation has been supplied on condition that anyone
    who consults it is understood to recognise that its copyright rests with its
    author and that no quotation from the dissertation and no information
    derived from it may be published without the prior written consent of
    the author.

    \section*{Declaration}
    This dissertation is submitted to the University of Bath in accordance
    with the requirements of the degree of Bachelor of Science in the
    Department of Computer Science. No portion of the work in this dissertation
    has been submitted in support of an application for any other degree
    or qualification of this or any other university or institution of learning.
    Except where specifically acknowledged, it is the work of the author.
  \end{spaced}

  \begin{spaced}{5em}
    Signed:
  \end{spaced}
  }


\newcommand{\consultation}[1]{%
\thispagestyle{empty}
\begin{centrespaced}{0.8\textwidth}{0.4\textheight}
\ifnum #1 = 0
This dissertation may be made available for consultation within the
University Library and may be photocopied or lent to other libraries
for the purposes of consultation.
\else
This dissertation may not be consulted, photocopied or lent to other
libraries without the permission of the author for #1 
\ifnum #1 = 1
year
\else
years
\fi
from the date of submission of the dissertation.
\fi
\vspace{4em}

Signed:
\end{centrespaced}
}

%%
%% END OF DEFINITIONS
%%

\title{An Experimental Alternative to Grand Central Dispatch on Linux}
\author{Stefan Petrov}
\date{Bachelor of Science in Computer Science with Honours\\The University of Bath\\May 2018}

\begin{document}

\pagenumbering{roman}

\maketitle
\cleardoublepage

\consultation{0}
\cleardoublepage

\declaration{An Experimental Alternative to Grand Central Dispatch on Linux}{Stefan Petrov}
\cleardoublepage

\abstract
Apple's Grand Central Dispatch (GCD) is a task-based parallelism framework aiming to reduce the complexity of parallel code. While supported on the Linux operating system the majority of the implementations depend on compatibility layers for system services not present on Linux, introducing potential overhead. This project proposes the Experimental Dispatch Alternative (EDA) library which implements the GCD programming model using the standard C language and Linux features. EDA and a Linux GCD implementation are then evaluated against each other and the system multi-threading library on a set of benchmarks in order to assess their performance and usability, and determine if any overheads are introduced by the GCD compatibility layers. Our results show that both GCD and EDA offer a compromise between usability and performance compared to using threads directly, and that the compatibility layers for GCD do cause an increased memory footprint, also revealing some areas for improvement in both GCD and EDA.

\tableofcontents
\listofalgorithms
\listoffigures

\chapter*{Acknowledgments}
First of all I would like to thank my project supervisor Dr. Russell Bradford for the guidance he provided throughout the project, reminding me not to keep the larger picture in mind, and his patience with my rambling explanations.
\par
I would also like to thank my housemates and close friends Damyan and Slaveya, who provided me the much needed emotional support and comedic relief to maintain my sanity throughout the project, in addition to acting as part time reviewers, while also dealing with their own dissertations.
\par
Finally, I'd like to thank all my friends and relatives who helped me in one way or another to get through university, and especially this grueling final year. 

\cleardoublepage
\pagenumbering{arabic}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Introduction}
Over the past few decades raw processor speeds have plateaued, due to engineering constraints such as energy efficiency and physical limitations on transistor size. To continue scaling performance manufacturers have focused on producing multiprocessor hardware capable of true parallelism. However writing parallel software which makes efficient use of such hardware using the primitives provided by the target platform, has proven to be far more difficult than writing the sequential equivalent. This has given rise to libraries and frameworks which try to abstract away the low-level primitives such as threads, locks, semaphores, and others.
\par
Apple's Grand Central Dispatch (GCD) is an example of such a higher-level abstraction over the system primitives, and is the main topic of this project. More specifically GCD focuses primarily on task based parallelism and abstracting away the threading model by introducing the concept of first-in first-out (FIFO) dispatch queues\cite{gcdintro, gcdapi}. Apple claims that this parallelism paradigm is not only simpler to use than the threading paradigm, but it is also utilizes hardware more efficiently\cite{gcdintro}. 
\par
GCD was originally developed for Apple's UNIX-like MacOS, but since the initial release a number of Linux ports and implementations have been made available\cite{deb-package, mlba-gcd, apple-gcd-sources}. There are some slight differences between the implementations, but they all provide the same core programming interfaces. However, most of these implementations seem to be dependent on a compatibility layer\cite{linux-kqueue} for the MacOS event notification mechanism \textbf{kqueue}\cite{kqueue-paper, xnu-kqueue} which is different from Linux's own event notification facility\cite{linux-epoll}. This presents a potential overhead which may cause increased resource usage and poorer performance. What's more the license under which all of these implementations are released is the Apache 2.0 license. This license is incompatible with the GNU General Purpose License (GPL) 2 used by a significant portion of Linux-based software projects\cite{gpl-adoption, gplcompat}, thus impeding adoption.
\par
Considering the above this project has 2 main goals:
\begin{enumerate}
	\item Provide a Linux alternative to GCD which retains the paradigm of tasks being dispatched to FIFO queues for asynchronous execution. This will be done using solely tooling and system facilities available on a modern Linux distribution, without sharing any code with existing GCD implementations in order to allow for the library to be distributed under a GPL-compatible license.   We'll refer to this alternative library as the Experimental Dispatch Alternative (EDA).
	\item Ascertain the validity of the claims about the usability and efficiency of GCD over the traditional threading model, and to determine whether the compatibility layer truly presents any measurable overheads. This will be done by comparing the GCD, EDA, and Portable Operating System Interface (POSIX) Threads (PThreads) implementations of a set of benchmark problems.
\end{enumerate}
\par
With the goals of the project stated in this way it is mostly a typical software engineering project, with some experimental research at the end. As such it will mostly focus on the requirements, design, implementation and testing of EDA which will then be used for experimental purposes, with the structure and content distribution of this report reflecting this. 
\begin{itemize}
	\item The literature and technology survey will cover background information such as parallelism and concurrency concepts, patterns, and paradigms, the state of the art and will expand on topics briefly touched on in the introduction.
	\item The requirements and specification chapter will cover how the requirements and the specification for our proposed library are derived from the GCD specification, and explanations for any deviations.
	\item The design chapter will outline the architecture of the library, and the components thereof.
	\item The implementation chapter will include in-depth explanation and discussion of implementation specifics.
	\item The testing chapter will describe the tests used to determine whether the implementation meets the functional requirements specification, and the results from the testing process.
	\item The evaluation chapter will cover the experimental design used to evaluate EDA's and GCD's usability and performance, and will then present and interpret the results of the experiment.    This chapter will also serve to present the results of the EDA performance test as part of the experimental results.
	\item We will conclude with summarizing results and identifying areas for future research and improvements on the project topic.
\end{itemize}

\include{lit-survey}
\include{requirements}
\include{design}
\include{implementation}
\include{testing}
\include{eval}
\include{conclusions}

\printbibliography

\begin{appendices}
\include{thrpool}
\include{extrafig}
\end{appendices}

\end{document}
