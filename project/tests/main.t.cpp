//
// Created by spp on 04/02/18.
//
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <sys/time.h>
#include <eda.h>
#include <semaphore.h>

#define XSTR(a) STR(a)
#define STR(a) #a

uint32_t USLEEP_DURATION = 1000;

EDA_EXPORT void increment(uint32_t* p) {
    usleep(USLEEP_DURATION );
    *p = *p + 1;
    (void)p;
}


EDA_EXPORT void increment_no_sleep(uint32_t* p){
    *p = *p + 1;
}


EDA_EXPORT void concurrent_test(uint32_t *p){
    usleep(USLEEP_DURATION);
    *p = 0;
}

#define TIME_USEC(x, var) { \
    struct timeval start, end; \
    gettimeofday(&start, NULL); \
    x; \
    gettimeofday(&end, NULL); \
    timersub(&end, &start, &(var)); \
}

#define TIMEVAL_INIT(usec) {\
    .tv_usec = long(usec % USEC_PER_SEC), \
    .tv_sec  = long(usec / USEC_PER_SEC) \
}

TEST_CASE("Async", "[async]") {


    SECTION("Concurrent queue"){
        const uint32_t MAX_ITER = 100;

        struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);
        struct timeval runtime;

        uint32_t completions[MAX_ITER];
        uint32_t* ptr = completions;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        REQUIRE(q != NULL);

        TIME_USEC({
            for(uint32_t iter = 0; iter < MAX_ITER; iter++){
                completions[iter] = 1;
                eda_async(q, ^{
                    ptr[iter] = 0;
                });
            }

            uint32_t local_counter = 0;
            while(local_counter < MAX_ITER){
                if( completions[local_counter] == 0 ) {
                    local_counter++;
                }
                else {
                    usleep(10);
                }
            }
        }, runtime);


        for(uint32_t iter = 0; iter < MAX_ITER; iter++) {
            REQUIRE ( completions[iter] == 0);
        }
        CHECK( timercmp(&runtime, &MAX_EXPECTED_TIME, <=));
    }

    SECTION("Serial queue"){
        const int USLEEP_DURATION = 100;
        const uint32_t MAX_ITER = 1000;

        __block uint32_t counter = 0;
        sem_t* sem = (sem_t*)alloca(sizeof(sem_t));
        sem_init(sem, 0, 1);

        struct timeval runtime;
        struct timeval expected_time = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);

        eda_queue_t q = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_LOW, EDA_QUEUE_FLAG_SERIAL);
        REQUIRE(q != NULL);

        TIME_USEC({
            for(int iter = 0; iter < MAX_ITER; iter++){
                eda_async(q, ^{
                    if(!sem_trywait(sem)) {
                        usleep(USLEEP_DURATION);
                        counter++;
                        sem_post(sem);
                    }
                });
            }

            while(counter < MAX_ITER) {
                usleep(10);
            }
        }, runtime);

        CHECK( timercmp(&runtime, &expected_time, >=) );
        REQUIRE( counter == MAX_ITER);

        eda_obj_release(q);
    }

    SECTION("2 serial queues"){
        const int USLEEP_DURATION = 5;
        const uint32_t MAX_ITER = 1000;

        struct timeval runtime;
        struct timeval expected_time = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);
        struct timeval max_expected_time = TIMEVAL_INIT(2*MAX_ITER*USLEEP_DURATION);

        eda_queue_t q1 = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_LOW, EDA_QUEUE_FLAG_SERIAL);
        REQUIRE(q1 != NULL);

        eda_queue_t q2 = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_LOW, EDA_QUEUE_FLAG_SERIAL);
        REQUIRE(q2 != NULL);

        sem_t* sems = (sem_t*)alloca(2*sizeof(sem_t));
        sem_init(sems,0,1);
        sem_init(sems+1,0,1);

        __block _Atomic(int) counter = 0;
        auto task = ^(sem_t* sem){
            if(!sem_trywait(sem)) {
                usleep(USLEEP_DURATION);
                counter++;
                sem_post(sem);
            }
        };

        TIME_USEC({
              for(int iter = 0; iter < MAX_ITER; iter++){
                  eda_async(q1, ^{
                      task(sems);
                  });
                  eda_async(q2, ^{
                      task(sems+1);
                  });
              }

              while(counter < 2*MAX_ITER) {
                      usleep(5);
                  }
              }, runtime);

        CHECK( timercmp(&runtime, &expected_time, >=) );
        REQUIRE( counter == 2*MAX_ITER);

        eda_obj_release(q1);
        eda_obj_release(q2);
    }

}

TEST_CASE("Cooperative queues", "[coop]") {

    eda_queue_t q = eda_queue_create(XSTR(__LINE__),
                                      EDA_QUEUE_PRIO_HIGH,
                                      EDA_QUEUE_FLAG_SERIAL|EDA_QUEUE_FLAG_COOP);

    SECTION("Single queue"){
        const int USLEEP_DURATION = 100;
        const uint32_t MAX_ITER = 1000;

        __block uint32_t counter = 0;
        sem_t* sem = (sem_t*)alloca(sizeof(sem_t));
        sem_init(sem, 0, 1);

        struct timeval runtime;
        struct timeval expected_time = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);

        TIME_USEC({
                      for(int iter = 0; iter < MAX_ITER; iter++){
                          eda_async(q, ^{
                              if(!sem_trywait(sem)) {
                                  usleep(USLEEP_DURATION);
                                  counter++;
                                  sem_post(sem);
                              }
                          });
                      }

                      while(counter < MAX_ITER) {
                          usleep(10);
                      }
                  }, runtime);

        CHECK( timercmp(&runtime, &expected_time, >=) );
        REQUIRE( counter == MAX_ITER);

    }

    eda_obj_release(q);
}

TEST_CASE("Sync", "[sync]") {

    SECTION("Concurrent queue, no async") {
        const uint32_t MAX_ITER = 100;

        struct timeval runtime;
        struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);

        uint32_t counter = 0;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);

        TIME_USEC({
            for(uint32_t iter = 0; iter < MAX_ITER; iter++) {
               TIME_USEC(
                   eda_sync_f(q, (eda_func_t)increment, &counter),
                   runtime);
               REQUIRE( counter == iter+1);
            }
        }, runtime);

        REQUIRE( counter == MAX_ITER );
        CHECK( timercmp(&runtime, &MIN_EXPECTED_TIME, >=));
    }

   SECTION("Concurrent queue, with async") {

       const uint32_t MAX_ITER = 200;
       const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(MAX_ITER * USLEEP_DURATION);
       // We only have so many threads, leave some leeway
       const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT((MAX_ITER + MAX_ITER/2) * USLEEP_DURATION);

       uint32_t counter = 0;
       uint32_t completions[MAX_ITER], completion_iter = 0;
       struct timeval runtime;

       eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
       REQUIRE( q != NULL);

       for(uint32_t iter = 0; iter < MAX_ITER; iter++) {
           eda_async_f(q, (eda_func_t)concurrent_test, completions+iter);
       }

       TIME_USEC({
           for(uint32_t iter = 0; iter < MAX_ITER; iter++) {
               TIME_USEC(
                   eda_sync_f(q, (eda_func_t)increment, &counter),
                   runtime);
               REQUIRE( counter == iter+1);
           }

           while(completion_iter < MAX_ITER){
               if( completions[completion_iter] == 0 ) {
                   completion_iter++;
               }
           }

       }, runtime);


       REQUIRE( completion_iter == MAX_ITER );
       REQUIRE( counter == MAX_ITER );
       CHECK( timercmp(&runtime, &MAX_EXPECTED_TIME, <=) );
       CHECK( timercmp(&runtime, &MIN_EXPECTED_TIME, >=) );
   }

   sem_t* sem = (sem_t*)alloca(sizeof(sem_t));
   sem_init(sem, 0 ,1);

   SECTION("Sequential queue, no async") {
       const uint32_t MAX_ITER = 1000;
       __block uint32_t counter = 0;

       const struct timeval TASK_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION);
       const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);
       struct timeval runtime;

       eda_queue_t q = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_LOW, EDA_QUEUE_FLAG_SERIAL);
       REQUIRE(q != NULL);

       auto task = ^{
           if(!sem_trywait(sem)){
               usleep(USLEEP_DURATION);
               counter++;
               sem_post(sem);
           }
       };

       TIME_USEC({
           for(int iter = 0; iter < MAX_ITER; iter++) {
               TIME_USEC(eda_sync(q, task), runtime);
               REQUIRE( timercmp(&runtime, &TASK_EXPECTED_TIME, >=));
           }
       }, runtime);

       REQUIRE( counter == MAX_ITER);
       CHECK( timercmp(&runtime, &MIN_EXPECTED_TIME, >=) );
       eda_obj_release(q);
   }

   SECTION("Sequential queue, with async") {
        const uint32_t MAX_ITER = 1000;
        __block uint32_t counter1 = 0, counter2 = 0;

        const struct timeval TASK_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION);
        const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(2*MAX_ITER*USLEEP_DURATION);
        const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT((5*MAX_ITER/2)*USLEEP_DURATION);
        struct timeval runtime;

        eda_queue_t q = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_LOW, EDA_QUEUE_FLAG_SERIAL);
        REQUIRE(q != NULL);

        auto task = ^(uint32_t* counter){
            if(!sem_trywait(sem)){
                usleep(USLEEP_DURATION);
                *counter = *counter + 1;
                sem_post(sem);
            }
        };

        eda_queue_t gq = eda_queue_get_global(EDA_QUEUE_PRIO_DEFAULT, EDA_QUEUE_FLAG_DEFAULT);
        eda_async(gq, ^{
            for (int iter = 0; iter < MAX_ITER; iter++) {
                eda_async(q, ^{task(&counter1);});
            }
        });

        TIME_USEC({
            for(int iter = 0; iter < MAX_ITER; iter++){
                TIME_USEC(eda_sync(q, ^{task(&counter2);}), runtime);
                CHECK( timercmp(&runtime, &TASK_EXPECTED_TIME, >=));
                REQUIRE( counter2 == iter+1);
            }

        }, runtime);

        // Wait for async tasks to finish
        while(counter1 < MAX_ITER)
            usleep(10);

        REQUIRE( counter1 == MAX_ITER);
        REQUIRE( counter2 == MAX_ITER);
        CHECK( timercmp(&runtime, &MIN_EXPECTED_TIME, >=));
        CHECK( timercmp(&runtime, &MAX_EXPECTED_TIME, <=));
        eda_obj_release(q);
    }
}

TEST_CASE("Once", "[once]") {
    const int USLEEP_DURATION = 10000;
    const uint32_t MAX_ITER = 1000;

    const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(3*USLEEP_DURATION/2);
    const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION);
    struct timeval runtime;

    eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
    REQUIRE(q != NULL);

    static eda_completion_t done;
    __block int flag = 0;
    int* seen = (int*)alloca(sizeof(int)*MAX_ITER);

    eda_group_t g = eda_group_create();

    TIME_USEC({
        for (uint32_t iter = 0; iter < MAX_ITER; iter++) {
            eda_group_async(g, q, ^{
                eda_once(&done, ^{
                    usleep(USLEEP_DURATION);
                    flag++;
                });
                seen[iter] = flag;
            });
        }

        eda_group_wait(g, EDA_TIME_NEVER);
    }, runtime);

    eda_obj_release(g);

    REQUIRE (flag == 1);
    for(uint32_t iter = 0; iter < MAX_ITER; iter++) {
        REQUIRE ( seen[iter] == flag);
    }
    CHECK( timercmp(&runtime, &MIN_EXPECTED_TIME, >=));
    CHECK( timercmp(&runtime, &MAX_EXPECTED_TIME, <=) );
}

TEST_CASE("Group wait", "[wait]") {

    SECTION("Concurrent queue, 1 group") {
        const uint32_t USLEEP_DURATION = 10;
        const uint32_t MAX_ITER = 1000;
        // There are only so many threads we can use, so that increases in a bigger potential runtime
        const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(3*USLEEP_DURATION*MAX_ITER);
        const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);

        struct timeval runtime;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        REQUIRE(q != NULL);

        eda_group_t g = eda_group_create();
        REQUIRE(g != NULL);

        __block _Atomic(uint32_t) counter = 0;
        TIME_USEC({
            eda_group_async(g, q, ^{
                usleep(MAX_ITER*USLEEP_DURATION);
                counter++;
            });
            for(uint32_t i = 0; i<MAX_ITER; i++)
                eda_group_async(g, q, ^{
                    usleep(USLEEP_DURATION);
                    counter++;
                });

            eda_group_wait(g, EDA_TIME_NEVER);

        }, runtime);

        REQUIRE( counter == MAX_ITER+1);
        CHECK( timercmp(&MAX_EXPECTED_TIME, &runtime, >) );
        CHECK( timercmp(&MIN_EXPECTED_TIME, &runtime, <=) );
        eda_obj_release(g);
    }

    SECTION("Sequential queue, 1 group") {
        const uint32_t MAX_ITER = 1000;
        // There are only so many threads we can use, so that increases in a bigger potential runtime
        const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION * MAX_ITER);

        struct timeval runtime;
        __block uint32_t counter = 0;

        eda_queue_t q = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_SERIAL);
        REQUIRE(q != NULL);

        eda_group_t g = eda_group_create();
        REQUIRE(g != NULL);

        TIME_USEC({
            for (uint32_t i = 0; i < MAX_ITER; i++)
                eda_group_async(g, q, ^{
                    usleep(USLEEP_DURATION);
                    counter++;
                });
            eda_group_wait(g, EDA_TIME_NEVER);
        }, runtime);

        CHECK( timercmp(&MIN_EXPECTED_TIME, &runtime, <=));
        REQUIRE( counter == MAX_ITER );
        eda_obj_release(g);
        eda_obj_release(q);
    }

    SECTION("Concurrent queue, 1 group, timeouts") {
        const uint32_t MAX_ITER = 1000;
        // There are only so many threads we can use, so that increases in a bigger potential runtime
        const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(2*USLEEP_DURATION*MAX_ITER);
        const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(MAX_ITER*USLEEP_DURATION);

        struct timeval runtime;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        REQUIRE(q != NULL);

        eda_group_t g = eda_group_create();
        REQUIRE(g != NULL);

        __block _Atomic(uint32_t) counter = 0;
        TIME_USEC({
            eda_group_async(g, q, ^{
                usleep(MAX_ITER*USLEEP_DURATION);
                counter++;
            });

            for(uint32_t i = 0; i<MAX_ITER; i++)
                eda_group_async(g, q, ^{
                    usleep(USLEEP_DURATION);
                counter++;
            });
            int rval = eda_group_wait(g, EDA_TIME_NOW);
            REQUIRE(rval == -1);
            do {
                eda_time_t timeout = eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, USLEEP_DURATION*USEC_PER_SEC);
                rval = eda_group_wait(g, timeout);
            } while(rval != 0);

        }, runtime);

        CHECK( timercmp(&MAX_EXPECTED_TIME, &runtime, >) );
        CHECK( timercmp(&MIN_EXPECTED_TIME, &runtime, <=) );
        eda_obj_release(g);
    }

    SECTION("Concurrent & sequential queue, 2 groups") {
        const uint32_t MAX_ITER = 1000;
        // There are only so many threads we can use, so that's means bigger potential runtime
        const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION*3*MAX_ITER/2);
        const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION*MAX_ITER);

        struct timeval runtime, runtime_partial;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        REQUIRE(q != NULL);

        eda_queue_t qs = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_SERIAL);
        REQUIRE(qs != NULL);

        eda_group_t g = eda_group_create();
        REQUIRE(g != NULL);

        __block uint32_t counter = 0;
        TIME_USEC({

            TIME_USEC({
                for(uint32_t i = 0; i<MAX_ITER; i++)
                    eda_group_async(g, q, ^{
                        eda_group_async(g, qs, ^{
                            usleep(USLEEP_DURATION);
                            counter++;
                        });
                    });

            }, runtime_partial);

            // The actual increments are far from being submitted
            REQUIRE( timercmp(&MIN_EXPECTED_TIME, &runtime_partial, >=) );

            eda_group_wait(g, EDA_TIME_NEVER);

        }, runtime);

        REQUIRE( counter == MAX_ITER );
        CHECK( timercmp(&MAX_EXPECTED_TIME, &runtime, >=) );
        CHECK( timercmp(&MIN_EXPECTED_TIME, &runtime, <=) );

        eda_obj_release(g);
        eda_obj_release(qs);
    }
 }

TEST_CASE("Group defer", "[defer]") {

    const uint32_t MAX_ITER = 1000;
    const uint32_t NUM_PAR = 4;

    eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);

    eda_queue_t qs = eda_queue_create(XSTR(__LINE__), EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_SERIAL);

    eda_group_t g = eda_group_create(),
                aux = eda_group_create();
    eda_sem_t sem = eda_sem_create(0);

    SECTION("Defer, guaranteed deferral") {

        __block uint32_t counter = 0;
        eda_block_t inc = ^{
            counter++;
            if(counter == NUM_PAR*MAX_ITER){
                eda_sem_post(sem);
            }
        };

        eda_group_join(g);

        // Parallelize deferral attempts
        for (int i=0; i<NUM_PAR; i++) {
            eda_group_async(aux, q, ^{
                for(int j=0; j<MAX_ITER; j++){
                    eda_group_defer(g, qs, inc);
                }
            });
        }
        // Ensure all parallel tasks are deferred already
        eda_group_wait(aux, EDA_TIME_NEVER);

        REQUIRE(counter == 0);
        eda_group_leave(g);

        eda_sem_wait(sem, EDA_TIME_NEVER);
        REQUIRE(counter == MAX_ITER*NUM_PAR);
    }

    SECTION("Defer, no deferral") {
        __block uint32_t counter = 0;
        eda_sem_t sem = eda_sem_create(0);
        eda_block_t inc = ^{
            counter++;
            if(counter == NUM_PAR*MAX_ITER){
                eda_sem_post(sem);
            }
        };

        // Parallelize deferral attempts
        for (int i=0; i<NUM_PAR; i++) {
            eda_async(q, ^{
                for(int j=0; j<MAX_ITER; j++){
                    eda_group_defer(g, qs, inc);
                }
            });
        }
        REQUIRE(counter >= 0);

        eda_sem_wait(sem, EDA_TIME_NEVER);
        REQUIRE(counter == MAX_ITER*NUM_PAR);
    }

    SECTION("Defer, mixed") {
        __block uint32_t counter = 0;
        eda_block_t inc = ^{
            counter++;
            if(counter == NUM_PAR*MAX_ITER){
                eda_sem_post(sem);
            }
        };

        eda_group_join(g);
        // Parallelize deferral attempts
        for (int i=0; i<NUM_PAR; i++) {
            eda_async(q, ^{
                for(int j=0; j<MAX_ITER; j++){
                    eda_group_defer(g, qs, inc);
                }
            });
        }
        // Give them some leeway
        usleep(5);
        REQUIRE(counter == 0);
        eda_group_leave(g);
        for(int i=0; i<5; i++) {
            eda_group_join(g);
            usleep(2);
            eda_group_leave(g);
        }

        eda_sem_wait(sem, EDA_TIME_NEVER);
        REQUIRE(counter == MAX_ITER*NUM_PAR);
    }
    eda_obj_release(g);
    eda_obj_release(qs);
    eda_obj_release(aux);
    eda_obj_release(sem);
}

TEST_CASE("Apply loops", "[apply]") {


    SECTION("Sequential execution") {
        const uint32_t MAX_ITER = 1000;
        const struct timeval MIN_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION * MAX_ITER);
        struct timeval runtime;

        eda_queue_t q = eda_queue_create("", EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_SERIAL);

        sem_t* sem = (sem_t*)alloca(sizeof(sem_t));
        sem_init(sem,0,1);

        __block uint32_t counter = 0;
        TIME_USEC( eda_apply(q, MAX_ITER, ^(size_t i){
            if(!sem_trywait(sem)){
                usleep(USLEEP_DURATION);
                counter++;
                sem_post(sem);
            }
        }), runtime);

        REQUIRE(counter == MAX_ITER);
        CHECK(timercmp(&runtime, &MIN_EXPECTED_TIME, >=));
        eda_obj_release(q);
    }

    SECTION("Parallel execution") {

        const uint32_t MAX_ITER = 10000;
        // There are only so many threads we can use, so that increases in a bigger potential runtime
        const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION * MAX_ITER);

        struct timeval runtime;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        REQUIRE(q != NULL);

        int* array = (int*)calloc(MAX_ITER, sizeof(int));
        TIME_USEC(eda_apply(q, MAX_ITER, ^(size_t i){
            usleep(USLEEP_DURATION);
            array[i] = i+1;
        }), runtime);

        int i;
        for (i = 0; i < MAX_ITER; i++)
            CHECK(array[i] == i + 1);
        free(array);

        CHECK(timercmp(&runtime, &MAX_EXPECTED_TIME, <=));
    }

    SECTION("Dynamic parallel schedule") {

        const uint32_t MAX_ITER = 10000;
        // There are only so many threads we can use, so that increases in a bigger potential runtime
        const struct timeval MAX_EXPECTED_TIME = TIMEVAL_INIT(USLEEP_DURATION * MAX_ITER);

        struct timeval runtime;

        eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        REQUIRE(q != NULL);

        int* array = (int*)calloc(MAX_ITER, sizeof(int));
        TIME_USEC(eda_apply_sched(q, MAX_ITER, EDA_SCHED_DYNAMIC, ^(size_t i){
            usleep(USLEEP_DURATION);
            array[i] = i+1;
        }), runtime);

        int i;
        for (i = 0; i < MAX_ITER; i++)
            CHECK(array[i] == i + 1);
        free(array);

        CHECK(timercmp(&runtime, &MAX_EXPECTED_TIME, <=));
    }

}
