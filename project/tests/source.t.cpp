#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <eda.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>

#define TIME_USEC(x, var) { \
    struct timeval start, end; \
    gettimeofday(&start, NULL); \
    x; \
    gettimeofday(&end, NULL); \
    timersub(&end, &start, &(var)); \
}

#define TIMEVAL_INIT(usec) {\
    .tv_usec = long(usec % USEC_PER_SEC), \
    .tv_sec  = long(usec / USEC_PER_SEC) \
}

TEST_CASE("Signals", "[signal]") {
    eda_sem_t ctlsem = eda_sem_create(1);
    eda_sem_t sem = eda_sem_create(0);

    eda_blockev_t reghandler = ^(intptr_t){
        if(!eda_sem_wait(ctlsem, EDA_TIME_NOW)) {
            kill(getpid(), SIGUSR1);
            eda_sem_post(ctlsem);
        }
        else {
            abort();
        }
    };

    SECTION("Single source") {
        const int MAX_SIGNAL_FIRES = 100;
        int rval = 0;

        eda_source_t sigsrc = eda_source_create(EDA_SIGNAL_SOURCE, SIGUSR1, 0, NULL);
        REQUIRE(sigsrc != NULL);

        __block int counter = 0;
        eda_blockev_t evhandler = ^(intptr_t sigfire) {
            if(!eda_sem_wait(ctlsem, EDA_TIME_NOW)) {
                counter += sigfire;
                if (counter < MAX_SIGNAL_FIRES) {
                    kill(getpid(), SIGUSR1);
                    usleep(20);
                }
                else {

                    eda_source_cancel(sigsrc);
                    eda_sem_post(sem);
                }
                eda_sem_post(ctlsem);
            }
            else{
                printf("Multiple signal handlers");
                abort();
            }
        };


        rval = eda_source_set_event_handler(sigsrc, evhandler);
        REQUIRE(rval == 0);
        rval = eda_source_set_register_handler(sigsrc, reghandler);
        REQUIRE(rval == 0);

        eda_source_start(sigsrc);

        rval = eda_source_set_event_handler(sigsrc, reghandler);
        REQUIRE(rval != 0);
        rval = eda_source_set_register_handler(sigsrc, evhandler);
        REQUIRE(rval != 0);

        eda_sem_wait(sem, eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, 20*NSEC_PER_MSEC));

        REQUIRE(counter == MAX_SIGNAL_FIRES);
        eda_obj_release(sigsrc);
    }

    SECTION("Two sources") {
        const int MAX_SIGNAL_FIRES = 100;
        int rval = 0;

        // First source
        eda_source_t sigsrc1 = eda_source_create(EDA_SIGNAL_SOURCE, SIGUSR1, 0, NULL);

        __block int counter1 = 0;
        eda_blockev_t evhandler = ^(intptr_t sigfire) {
            REQUIRE(sigfire == 1);
            if(!eda_sem_wait(ctlsem, EDA_TIME_NOW)) {
                counter1 += sigfire;
                if (counter1 < MAX_SIGNAL_FIRES) {
                    kill(getpid(), SIGUSR1);
                }
                else {

                    eda_source_cancel(sigsrc1);
                    eda_sem_post(sem);
                }

                eda_sem_post(ctlsem);
            }
            else {
                printf("Multiple signal handlers");
                abort();
            }
        };

        eda_source_set_event_handler(sigsrc1, evhandler);
        eda_source_set_register_handler(sigsrc1, reghandler);

        // Second source
        eda_source_t sigsrc2 = eda_source_create(EDA_SIGNAL_SOURCE, SIGUSR1, 0, NULL);

        eda_sem_t sem2 = eda_sem_create(0), ctlsem2 = eda_sem_create(1);
        __block int counter2 = 0, coalesced = 0;
        eda_blockev_t evhandler2 = ^(intptr_t sigfire) {
            REQUIRE(sigfire >= 1);
            coalesced+=(sigfire >= 1);
            if(!eda_sem_wait(ctlsem2, EDA_TIME_NOW)) {
                counter2 += sigfire;
                if (counter2 < MAX_SIGNAL_FIRES) {
                    usleep(50);
                }
                else{
                    eda_source_cancel(sigsrc2);
                    eda_sem_post(sem2);
                }

                eda_sem_post(ctlsem2);
            }
            else {
                printf("Multiple signal handlers");
                abort();
            }
        };

        eda_source_set_event_handler(sigsrc2, evhandler2);

        // Start both sources
        eda_source_start(sigsrc1);
        eda_source_start(sigsrc2);

        eda_time_t to = eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, 20*NSEC_PER_MSEC);
        eda_sem_wait(sem, to);
        eda_sem_wait(sem2, to);
        eda_obj_release(sem2);
        eda_obj_release(ctlsem2);

        REQUIRE(counter1 == MAX_SIGNAL_FIRES);
        REQUIRE(counter2 == MAX_SIGNAL_FIRES);
        REQUIRE(coalesced > 0);

        eda_obj_release(sigsrc1);
        eda_obj_release(sigsrc2);
    }
    eda_obj_release(sem);
    eda_obj_release(ctlsem);
}

typedef struct timer_test_ctxt_s {
    eda_source_t src;
    int counter;
    int max_fires;
    eda_sem_t sem;
}* ttc_t;

void timer_fired_handler(ttc_t ctxt, uintptr_t fires) {
    ctxt->counter += fires;
    if(ctxt->max_fires <= ctxt->counter) {
        eda_source_cancel(ctxt->src);
    }
}

void timer_cancel_handler(ttc_t ctxt, uintptr_t unused) {
    ctxt->counter++;
    eda_sem_post(ctxt->sem);
}

TEST_CASE("Timers", "[timer]") {
    eda_sem_t sem = eda_sem_create(0);
    eda_sem_t ctl = eda_sem_create(1);
    SECTION("Base timer")
    {
        const int MAX_TIMER_FIRES = 1000;
        const int USEC_DELAY = 10;
        int rval = 0;

        eda_source_t timersrc = eda_source_create(EDA_TIMER_SOURCE, 0, 0, NULL);
        REQUIRE(timersrc != NULL);

        __block int count = 0;
        rval = eda_source_set_event_handler(timersrc, ^(intptr_t fires){
            if(!eda_sem_wait(ctl, EDA_TIME_NOW)){
                if((count += fires) >= MAX_TIMER_FIRES){
                    eda_source_cancel(timersrc);
                }
                eda_sem_post(ctl);
            }
            else {
                printf("Multiple timer handlers");
                abort();
            }

        });
        REQUIRE(rval == 0);

        __block _Atomic(int) cancels = 0;
        rval = eda_source_set_cancel_handler(timersrc, ^(intptr_t){
            cancels++;
            if(!eda_sem_wait(ctl, EDA_TIME_NOW)){
                eda_sem_post(sem);
                eda_sem_post(ctl);
            }
            else {
                abort();
            }
        });
        REQUIRE(rval == 0);

        rval = eda_source_set_timer(timersrc, EDA_TIME_NOW, USEC_DELAY * NSEC_PER_USEC);
        REQUIRE(rval == 0);

        eda_source_start(timersrc);

        rval = eda_source_set_timer(timersrc, EDA_TIME_NOW, NSEC_PER_USEC);
        REQUIRE(rval != 0);

        eda_time_t timeout = eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, 20*NSEC_PER_MSEC);
        REQUIRE( !eda_sem_wait(sem, timeout) );
        REQUIRE(cancels == 1);
        REQUIRE(count <= 1.1 * MAX_TIMER_FIRES );
        REQUIRE(count >= MAX_TIMER_FIRES );

        eda_obj_release(timersrc);
    }

    SECTION("Drift") {

        const int SECS = 10;

        __block int cancelled_times = 0;
        __block uint32_t count = 0;
        __block double last_jitter = 0;
        __block double drift_sum = 0;
        // 1000 times a second
        uint64_t interval = NSEC_PER_MSEC;
        // Part of a second of the interval
        double interval_d = interval / double(NSEC_PER_SEC);
        // Period of 10 seconds
        uint64_t target = SECS*NSEC_PER_SEC / interval;

        eda_source_t t = eda_source_create(EDA_TIMER_SOURCE, 0, 0, NULL);

        eda_source_set_timer(t, eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, interval), interval);

        eda_source_set_event_handler(t, ^(intptr_t fire_cnt) {
            struct timeval now_tv;
            static double first = 0;

            if(eda_sem_wait(ctl, EDA_TIME_NOW)){
                printf("Multiple timer handlers");
                abort();
            }

            gettimeofday(&now_tv, NULL);
            double now = now_tv.tv_sec + now_tv.tv_usec / double(USEC_PER_SEC);

            if (count == 0) {
                // Because this is taken at 1st timer fire,
                // later jitter values may be negative.
                // This doesn't effect the drift calculation.
                first = now;
            }
            double goal = first + interval_d * count;
            double jitter = goal - now;
            double drift = jitter - last_jitter;
            drift_sum += drift;

            if (target <= ++count) {
                eda_source_cancel(t);
            }
            last_jitter = jitter;

            eda_sem_post(ctl);
        });

        eda_source_set_cancel_handler(t, ^(intptr_t unused){
            cancelled_times++;

            if(eda_sem_wait(ctl, EDA_TIME_NOW)){
                return;
            }

            drift_sum /= count - 1;
            if (drift_sum < 0) {
                drift_sum = -drift_sum;
            }
            eda_sem_post(sem);
            eda_sem_post(ctl);
        });

        eda_source_start(t);
        eda_obj_release(t);

        eda_time_t timeout = eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, (SECS+1)*NSEC_PER_SEC);
        // Less than 10% drift
        REQUIRE(! eda_sem_wait(sem, timeout) );
        REQUIRE( cancelled_times == 1);
        REQUIRE(drift_sum <= interval_d / 10);
    }
    eda_obj_release(sem);
    eda_obj_release(ctl);
}

TEST_CASE("pipes", "[pipes]") {
    eda_sem_t completion = eda_sem_create(0);
    __block int page_size;
    __block void *readbuf, *writebuf;
    __block uint32_t writes = 0, reads = 0;
    __block int *pipefd = (int*)alloca(2*sizeof(int));

    uint32_t total_io = 100;

    REQUIRE(pipe2(pipefd, O_CLOEXEC|O_NONBLOCK) >= 0);

    eda_sem_t wctl = eda_sem_create(1);
    eda_source_t writer = eda_source_create(EDA_WRITE_SOURCE, pipefd[1], 0, NULL);
    REQUIRE(writer);

    eda_source_set_register_handler(writer, ^(intptr_t){
        if(eda_sem_wait(wctl, EDA_TIME_NOW)){
            abort();
        }
        // Make sure we're capped at 1 page
        page_size = fcntl(pipefd[1], F_SETPIPE_SZ, 1);
        if(page_size <= 1)
            abort();
        readbuf = malloc(page_size);
        writebuf = malloc(page_size);
        eda_sem_post(wctl);
    });

    __block _Atomic int write_cancel = 0;
    eda_source_set_cancel_handler(writer, ^(intptr_t){
        write_cancel++;
        if(eda_sem_wait(wctl, EDA_TIME_NOW)){
            abort();
        }
        close(pipefd[1]);
        free(writebuf);
        eda_sem_post(completion);
        eda_sem_post(wctl);
    });

    eda_source_set_event_handler(writer, ^(intptr_t data){
        if(eda_sem_wait(wctl, EDA_TIME_NOW)){
            return;
        }
        if(writes < total_io) {
            while(write(pipefd[1], writebuf, page_size) != -1) writes ++ ;
            if(errno != EAGAIN) {
                perror(NULL);
                abort();
            }
        }
        else {
            eda_source_cancel(writer);
        }
        eda_sem_post(wctl);
    });

    eda_sem_t rctl = eda_sem_create(1);
    eda_source_t reader = eda_source_create(EDA_READ_SOURCE, pipefd[0], 0, NULL);
    REQUIRE(reader);

    __block _Atomic int read_cancel = 0;
    eda_source_set_cancel_handler(reader, ^(intptr_t){
        read_cancel++;
        if(eda_sem_wait(rctl, EDA_TIME_NOW)){
            abort();
        }
        close(pipefd[0]);
        free(readbuf);
        eda_sem_post(completion);
        eda_sem_post(rctl);
    });

    eda_source_set_event_handler(reader, ^(intptr_t){
        if(eda_sem_wait(rctl, EDA_TIME_NOW)){
            abort();
        }
        while(read(pipefd[0], readbuf, page_size) != -1) {
            reads ++ ;
        }
        eda_sem_post(rctl);
    });

    eda_source_start(writer);
    eda_source_start(reader);

    eda_obj_release(writer);
    eda_obj_release(reader);

    eda_time_t to = eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, NSEC_PER_SEC);
    eda_sem_wait(completion, to);
    eda_sem_wait(completion, to);
    eda_obj_release(completion);
    eda_obj_release(wctl);
    eda_obj_release(rctl);

    REQUIRE(write_cancel == 1);
    REQUIRE(read_cancel == 1);
    REQUIRE(writes >= total_io);
    REQUIRE(reads >= total_io);
    REQUIRE(reads == writes);
}

TEST_CASE("Cancel pummel", "[cancel]"){

    const int ITERATIONS = 500, INNER_ITERS=1000;
    const eda_sem_t sem = eda_sem_create(0);
    const eda_sem_t ctl = eda_sem_create(1);

    for(int i = 0; i < ITERATIONS; i++) {
        eda_queue_t hpq = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
        eda_source_t source = eda_source_create(EDA_SIGNAL_SOURCE, SIGUSR2, 0, NULL);
        __block _Atomic(int) cancels = 0;

        // Make sure the source still exists
        eda_obj_retain(source);
        eda_source_set_register_handler(source, ^(intptr_t){
            if(eda_sem_wait(ctl, EDA_TIME_NOW)){
                printf("Multiple handlers!");
                abort();
            }

            for(int j = 0; j < INNER_ITERS; j++){
                eda_obj_retain(source);
                eda_async(hpq, ^{
                    eda_source_cancel(source);
                    eda_obj_release(source);
                });
            }

            kill(getpid(), SIGUSR2);
            usleep(10);
            eda_sem_post(ctl);
        });

        eda_source_set_event_handler(source, ^(intptr_t){
            if(eda_sem_wait(ctl, EDA_TIME_NOW)){
                printf("Multiple handlers!");
                abort();
            }
            eda_source_cancel(source);
            eda_sem_post(ctl);
        });

        eda_source_set_cancel_handler(source, ^(intptr_t){
            cancels++;
            if(eda_sem_wait(ctl, EDA_TIME_NOW)){
                abort();
            }
            eda_sem_post(sem);
            eda_sem_post(ctl);
        });
        eda_source_start(source);

        eda_sem_wait(sem, eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, 1000*USEC_PER_SEC));

        REQUIRE(cancels == 1);

        eda_obj_release(source);
    }
    eda_obj_release(sem);
    eda_obj_release(ctl);
}
