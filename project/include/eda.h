#ifndef EDA_LIBRARY_H
#define EDA_LIBRARY_H

#ifndef __cplusplus
    #include <stdint.h>
    #include <stddef.h>
    #include <time.h>
#else
    #include <cstdint>
    #include <cstddef>
    #include <ctime>
#endif

typedef void (*eda_func_t)(void*);
typedef void (*eda_func2_t)(void*,size_t);
typedef void (*eda_funcev_t)(void*,intptr_t);

#ifdef __BLOCKS__
typedef void (^eda_block_t)();
typedef void (^eda_block2_t)(size_t);
typedef void (^eda_blockev_t)(intptr_t);
#endif

typedef uint32_t eda_flags_t;

typedef void *eda_obj_t;
typedef struct eda_queue_s *eda_queue_t;
typedef _Atomic uintptr_t eda_completion_t;
typedef struct eda_group_s *eda_group_t;
typedef struct eda_source_s *eda_source_t;
typedef struct eda_sem_s *eda_sem_t;

#ifdef __cplusplus
#define EDA_EXPORT extern "C"
#else
#define EDA_EXPORT
#endif

/***************************************************
 * General API
 ***************************************************/

// Timescale constants
#define NSEC_PER_SEC (1000000000ull)
#define NSEC_PER_MSEC (1000000ull)
#define USEC_PER_SEC (1000000ull)
#define NSEC_PER_USEC (1000ull)

/**
 * Clock constants:
 * * EDA_CLOCK_WALL - the monotonically increasing system clock, should correspond to "wall clock" time.
 * * EDA_CLOCK_SYS - the default settable real-time system clock.
 */
typedef clockid_t eda_clock_t;
#define EDA_CLOCK_WALL CLOCK_MONOTONIC
#define EDA_CLOCK_SYS CLOCK_REALTIME

/**
 * An opaque absolute time value encoding in an integer format.
 * Lowest 1 bit is reserved for specifying the clock giving the time.
 * Other 63 bits specify the time in nanoseconds.
 * EDA_TIME_NOW - always refers to current time, clock independent.
 * EDA_TIME_FOREVER - essentially never, clock independent.
 */
typedef uint64_t eda_time_t;
#define EDA_TIME_NOW (0ull)
#define EDA_TIME_NEVER (~0ull)

/**
 * Creates a new opaque time value based on an offset from an existing one and a clock.
 * @param clock The clock which to use for creating the new value.
 * @param when The initial time value.
 *             If EDA_TIME_NOW, gets the current time according to the specified.
 *             If EDA_TIME_FOREVER returns it immediately.
 *             Otherwise must be given by the same clock as the one specified.
 * @param delta Nanoseconds into the future by which to offset the "when" parameter.
 * @return The offset time value or EDA_TIME_FOREVER if it was the given time value.
 */
EDA_EXPORT eda_time_t eda_time(eda_clock_t clock, eda_time_t when, uint64_t delta);

/**
 * Returns the between 2 time values specified in the same clock.
 * @param t1 Some time value. If EDA_TIME_NEVER is given result is undefined.
 * @param t2 Some time value. If EDA_TIME_NEVER is given result is undefined.
 * @return Returns the absolute nanosecond difference between the 2 time values.
 */
EDA_EXPORT uint64_t eda_time_diff(eda_time_t t1, eda_time_t t2);

/**
 * Increase the reference count of a given EDA object. If reference count was 0 behaviour is undefined.
 * @param obj
 */
EDA_EXPORT void eda_obj_retain(eda_obj_t obj);

/**
 * Decrease the reference count of a given EDA object. If reference count was 0 behaviour is undefined.
 * If the reference count becomes 0, release resources held by object.
 * @param obj
 */
EDA_EXPORT void eda_obj_release(eda_obj_t obj);


/***************************************************
 * Queues API
 ***************************************************/

/**
 * Define queue flags.
 * * EDA_QUEUE_FLAG_DEFAULT - parallel, non-cooperative queues.
 * * EDA_QUEUE_FLAG_SERIAL - serial, greedy queues.
 * * EDA_QUEUE_FLAG_COOP - cooperative queues.
 *                         Can be specified with EDA_QUEUE_FLAG_COOP to override greediness.
 */
#define EDA_QUEUE_FLAG_DEFAULT     ((uint32_t)0ul)
#define EDA_QUEUE_FLAG_SERIAL      ((uint32_t)0x1ul)
#define EDA_QUEUE_FLAG_COOP   ((uint32_t)0x2ul)

// Queue priorities
#define EDA_QUEUE_PRIO_BACKGROUND  0ul
#define EDA_QUEUE_PRIO_LOW         1ul
#define EDA_QUEUE_PRIO_DEFAULT     2ul
#define EDA_QUEUE_PRIO_HIGH        3ul

/**
 * Creates a new dispatch queue based on parameters.
 * @param name - Any string shorter than 64 characters, the name of the queue created. Useful for debugging.
 * @param priority - The scheduling priority of the queue, must be one of the EDA_QUEUE_PRIO_* values.
 * @param flags - Queue specific flags specifying scheduling constraints, e.g. parallel, serial, cooperative,
 * non-cooperative, greedy. Multiple flags are OR-ed bitwise.
 * @return NULL if queue could not be created, or a pointer to a valid queue.
 */
EDA_EXPORT eda_queue_t eda_queue_create(const char *name, uint32_t priority, eda_flags_t flags);

/**
 * Get one of the system predefined parallel queues.
 * @param priority The priority of the desired system queue.
 * @param flags The flags of the desired system queue. Should be EDA_QUEUE_FLAG_DEFAULT for now.
 * @return A pointer to the system queue which has the specified flags and priority, NULL if it does not exist.
 */
EDA_EXPORT eda_queue_t eda_queue_get_global(uint32_t priority, eda_flags_t flags);

/**
 * Dispatch a function task to a queue for asynchronous execution.
 * @param q The target dispatch queue.
 * @param func The task function which to execute.
 * @param ctxt A pointer to any data which has to be passed to the function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_async_f(eda_queue_t q, eda_func_t func, void *ctxt);

/**
 * Dispatch a function task to a queue for synchronous execution.
 * The task is not guaranteed to be executed on the same thread.
 * @param q The target dispatch queue.
 * @param func The task function which to execute.
 * @param ctxt A pointer to any data which has to be passed to the function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_sync_f(eda_queue_t q, eda_func_t func, void* ctxt);

/**
 * Get the number of items on a dispatch queue. Not guaranteed to be up-to-date.
 * @param q The target dispatch queue.
 * @return Last observed task count on the queue.
 */
EDA_EXPORT uint32_t eda_queue_get_length(eda_queue_t q);

/**
 * Get the name of a queue.
 * @param q The target dispatch queue.
 * @return A constant 0-terminated string which is the name.
 */
EDA_EXPORT const char* eda_queue_get_name(eda_queue_t q);

#ifdef __BLOCKS__
/**
 * Dispatch a closure task to a queue for asynchronous execution.
 * @param q The target dispatch queue.
 * @param block  The task closure.
 */
EDA_EXPORT void eda_async(eda_queue_t q, eda_block_t block);
/**
 * Dispatch a closure task to a queue for synchronous execution.
 * Task is not guaranteed to be executed on current thread.
 * @param q The target dispatch queue.
 * @param block  The task closure.
 */
EDA_EXPORT void eda_sync(eda_queue_t q, eda_block_t block);
#endif

/***************************************************
 * Task group API
 ***************************************************/

/**
 * Creates an empty task group.
 * @return A pointer to the new task group, or NULL if one could not be created.
 */
EDA_EXPORT eda_group_t eda_group_create(void);

/**
 * Dispatch a function task to a queue for asynchronous execution as a part of a group.
 * @param group The group which the task will join.
 * @param q The target queue on which the task will execute.
 * @param func The task function to execute.
 * @param ctxt A pointer to any data which has to be passed to the function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_group_async_f(eda_group_t group, eda_queue_t q, eda_func_t func, void* ctxt);

/**
 * Wait for all tasks in a group to finish running.
 * @param group The task group on which to wait.
 * @param timeout An absolute timeout.
 *                * If EDA_TIME_FOREVER is given waits indefinitely.
 *                * If EDA_TIME_NOW is given returns immediately, regardless of group tasks being run.
 *                * For other values wait until the time is reached.
 * @return 0 if no timeout occurred, -1 if there was a timeout, including EDA_TIME_NOW when group had running tasks.
 */
EDA_EXPORT int  eda_group_wait(eda_group_t group, eda_time_t timeout);

/**
 * Defer a task on the group for dispatching when the group tasks finish executing.
 * Instantly dispatch if no tasks are currently running in the group.
 * @param group The task group which handles the deferral.
 * @param q The target queue to which to dispatch when the group finishes execution.
 * @param func The task function to defer.
 * @param ctxt A pointer to any data which has to be passed to the function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_group_defer_f(eda_group_t group, eda_queue_t q, eda_func_t func, void *ctxt);

/**
 * Manually increment the running count of a group, as if a new task began executing in it.
 * @param group
 */
EDA_EXPORT void eda_group_join(eda_group_t group);

/**
 * Manually decrement the running count of a group, as if a group task was completed.
 * @param group
 */
EDA_EXPORT void eda_group_leave(eda_group_t group);

#ifdef __BLOCKS__
/**
 * Dispatch a function task to a queue for asynchronous execution as a part of a group.
 * @param group The group which the task will join.
 * @param q The target queue on which the task will execute.
 * @param block The closure task which to execute.
 */
EDA_EXPORT void eda_group_async(eda_group_t group, eda_queue_t q, eda_block_t func);

/**
 * Defer a task on the group for dispatching when the group tasks finish executing.
 * Instantly dispatch if no tasks are currently running in the group.
 * @param group The task group which handles the deferral.
 * @param q The target queue to which to dispatch when the group finishes execution.
 * @param block The closure task which to defer.
 */
EDA_EXPORT void eda_group_defer(eda_group_t group, eda_queue_t q, eda_block_t func);
#endif

/***************************************************
 * Loop parallelization API
 ***************************************************/

/**
 * Specify loop parallelization strategies:
 * * EDA_SCHED_STATIC - static parallelization, iterations are pre-distributed.
 * * EDA_SCHED_DYNAMIC - dynamic parallelization, iterations distributed during loop execution.
 */
typedef enum {
    EDA_SCHED_STATIC = 0,
    EDA_SCHED_DYNAMIC
} eda_apply_sched_t;

/**
 * Execute a loop on a queue.
 * Loop may be parallelized if queue is parallel and there are enough processors to do so.
 * @param q The queue to which to dispatch loop tasks.
 * @param repetitions How many iterations the loop has to do.
 * @param func Loop body function, must take iteration and context arguments.
 * @param ctxt A pointer to any data which has to be passed to the body function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_apply_f(eda_queue_t q, size_t repetitions, eda_func2_t func, void *ctxt);

/**
 * Execute a loop on a queue, with a specified loop scheduling strategy.
 * Loop may be parallelized if queue is parallel and there are enough processors to do so.
 * @param q The queue to which to dispatch loop tasks.
 * @param repetitions How many iterations the loop has to do.
 * @param sched Must be EDA_SCHED_DYNAMIC or EDA_SCHED_STATIC.
 * @param func Loop body function, must take iteration and context arguments.
 * @param ctxt A pointer to any data which has to be passed to the body function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_apply_sched_f(eda_queue_t q, size_t repetitions, eda_apply_sched_t sched, eda_func2_t func,
                                  void *ctxt);

#ifdef __BLOCKS__
/**
 * Execute a loop on a queue.
 * Loop may be parallelized if queue is parallel and there are enough processors to do so.
 * @param q The queue to which to dispatch loop tasks.
 * @param repetitions How many iterations the loop has to do.
 * @param block Loop body closure, must take an iteration argument.
 */
EDA_EXPORT void eda_apply(eda_queue_t q, size_t repetitions, eda_block2_t block);

/**
 * Execute a loop on a queue, with a specified loop scheduling strategy.
 * Loop may be parallelized if queue is parallel and there are enough processors to do so.
 * @param q The queue to which to dispatch loop tasks.
 * @param repetitions How many iterations the loop has to do.
 * @param sched Must be EDA_SCHED_DYNAMIC or EDA_SCHED_STATIC.
 * @param block Loop body closure, must take an iteration argument.
 */
EDA_EXPORT void eda_apply_sched(eda_queue_t q, size_t repetitions, eda_apply_sched_t sched, eda_block2_t block);
#endif

/***************************************************
 * One-time execution API
 ***************************************************/

/**
 * Ensure a task is executed only once during the execution of the program.
 * If called more than once when the task is being executed, blocks until execution finishes.
 * @param complete The predicate which determines whether task has been executed or is executing.
 * @param func The task function to execute.
 * @param ctxt A pointer to any data which has to be passed to the function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_once_f(eda_completion_t* complete, eda_func_t func, void* ctxt);

#ifdef __BLOCKS__
/**
 * Ensure a task is executed only once during the execution of the program.
 * If called more than once when the task is being executed, blocks until execution finishes.
 * @param complete The predicate which determines whether task has been executed or is executing.
 * @param func The task function to execute.
 * @param ctxt A pointer to any data which has to be passed to the function.
 *             If pointing to heap memory has to be freed by application code.
 */
EDA_EXPORT void eda_once(eda_completion_t* complete, eda_block_t block);
#endif

/***************************************************
 * Event source API
 ***************************************************/

/**
 * Event source types:
 * * EDA_READ_SOURCE - watch any file descriptor for read readiness;
 * * EDA_WRITE_SOURCE - watch any file descriptor for write readiness;
 * * EDA_SIGNAL_SOURCE - listen for a signal being delivered;
 * * EDA_SIGNAL_TIMER - listen for (periodic) timer experiations;
 */
typedef enum {
    EDA_READ_SOURCE = 0,
    EDA_WRITE_SOURCE,
    EDA_SIGNAL_SOURCE,
    EDA_TIMER_SOURCE
} eda_source_type_t;

/**
 * Create a new event source.
 * @param type The type of the event source, one of the event source type constants.
 * @param handle An opaque identifier, interpreted according to event source type.
 * @param flags A set of flags, interpreted according to event source type.
 * @param target The queue to which to dispatch event handlers.
 * @return Returns an event source pointer if input parameters were a valid combination and no errors occured.
 *         Null pointer otherwise. Event sources returned in an active state.
 */
EDA_EXPORT eda_source_t eda_source_create(eda_source_type_t type,
                                          uintptr_t handle,
                                          eda_flags_t flags,
                                          eda_queue_t target);

/**
 * Enable an event source for receiving events and dispatching handlers.
 * Triggers registration handler asynchronously.
 * Has no effect on already started event sources.
 * @param src The source to be started. Must be a valid source.
 */
EDA_EXPORT void eda_source_start(eda_source_t src);

/**
 * Cancels an event source, preventing events from being delivered to it.
 * Triggers cancellation handler asynchronously.
 * Has no effect on already cancelled or not yet registered event sources.
 * @param src Source to be cancelled. Must be a valid source.
 */
EDA_EXPORT void eda_source_cancel(eda_source_t src);

/**
 * Set the event handler to be dispatched when events are received.
 * Has no effect on started sources.
 * @param src The source for which to set the handler.
 * @param handler New event handler function. Has to take an event data and a context argument.
 * @param ctxt Any additional parameters which need to be passed to the handler function.
 * @return -1 if handler could not be set, and 0 otherwise.
 */
EDA_EXPORT int eda_source_set_event_handler_f(eda_source_t src, eda_funcev_t handler, void *ctxt);

/**
 * Set the handler to be dispatched when the event source is ready to receive events.
 * Has no effect on started sources.
 * @param src The source for which to set the handler.
 * @param handler New event handler function. Has to take an event data and a context argument.
 * @param ctxt Any additional parameters which need to be passed to the handler function.
 * @return -1 if handler could not be set, and 0 otherwise.
 */
EDA_EXPORT int eda_source_set_register_handler_f(eda_source_t src, eda_funcev_t handler, void *ctxt);

/**
 * Set the handler to be dispatched when the event source is cancelled.
 * Has no effect on started sources.
 * @param src The source for which to set the handler.
 * @param handler New event handler function. Has to take an event data and a context argument.
 * @param ctxt Any additional parameters which need to be passed to the handler function.
 * @return -1 if handler could not be set, and 0 otherwise.
 */
EDA_EXPORT int eda_source_set_cancel_handler_f(eda_source_t src, eda_funcev_t handler, void *ctxt);

#ifdef __BLOCKS__
/**
 * Set the event handler to be dispatched when events are received.
 * Has no effect on started sources.
 * @param src The source for which to set the handler.
 * @param handler New event handler closure. Has to take an event data argument.
 * @return -1 if handler could not be set, and 0 otherwise.
 */
EDA_EXPORT int eda_source_set_event_handler(eda_source_t src, eda_blockev_t handler);

/**
 * Set timer start time and expiry interval. Works only for timer event sources.
 * Has no effect on a started source.
 * @param src Timer source for which to set timer configuration.
 * @param start An absolute starting time according to some clock.
 * @param interval Nanosecond interval between timer expirations.
 *                 If 0 timer expires only when start is reached.
 * @return -1 if timer could be set, 0 otherwise.
 */
EDA_EXPORT int eda_source_set_timer(eda_source_t src, eda_time_t start, uint64_t interval);

/**
 * Set the handler to be dispatched when the event source is ready to receive events.
 * Has no effect on started sources.
 * @param src The source for which to set the handler.
 * @param handler New event handler closure. Has to take an event data argument.
 * @return -1 if handler could not be set, and 0 otherwise.
 */
EDA_EXPORT int eda_source_set_register_handler(eda_source_t src, eda_blockev_t handler);

/**
 * Set the handler to be dispatched when the event source is cancelled.
 * Has no effect on started sources.
 * @param src The source for which to set the handler.
 * @param handler New event handler closure. Has to take an event data argument.
 * @return -1 if handler could not be set, and 0 otherwise.
 */
EDA_EXPORT int eda_source_set_cancel_handler(eda_source_t src, eda_blockev_t handler);
#endif

/***************************************************
 * Semaphore API
 ***************************************************/

/**
 * Creates a new EDA semaphore.
 * @param val The initial value of the semaphore.
 * @return A semaphore pointer if successful, NULL otherwise.
 */
EDA_EXPORT eda_sem_t eda_sem_create(unsigned int val);

/**
 * Signal a semaphore, increasing its value and  waking up any waiters.
 * @param sem  semaphore.
 */
EDA_EXPORT void eda_sem_post(eda_sem_t sem);

/**
 * Wait for the semaphore value to become non-zero and decrement it.
 * @param sem Semaphore to wait on.
 * @param timeout An absolute timeout value.
 *                If EDA_TIME_NEVER, wait without timing out.
 *                If EDA_TIME_NOW, just try decreasing the semaphore value, timing out if it is 0.
 *                If some other time value, wait until the semaphore is signaled or the timeout is passed.
 * @return -1 on timeout, 0 otherwise.
 */
EDA_EXPORT int eda_sem_wait(eda_sem_t sem, eda_time_t timeout);

#endif