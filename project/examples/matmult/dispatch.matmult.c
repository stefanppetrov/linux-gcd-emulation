#include <stdio.h>
#include <dispatch/dispatch.h>
#include <shared/mat.h>

void mat_pmult(mat_t A, mat_t B, mat_t* C) {
    if(*C==NULL) {
        *C = mat_create(A->r, B->c, 0);
    }

    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);

    dispatch_apply(A->r, q, ^(size_t i){
        for (int j = 0; j < B->c; j++)
            for (int k = 0; k < A->c; k++)
                *mat_at(*C, i, j) += *mat_at(A, i, k) * *mat_at(B, k, j);
    });

}

int main(void){
    mat_t A = mat_load();

    mat_t B = NULL;
    uint64_t _, avg;
    BENCHMARK(mat_pmult(A, A, &B), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    mat_free(B);
    mat_free(A);

    return 0;
}
