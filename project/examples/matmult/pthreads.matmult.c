#include <pthread.h>
#include <stdio.h>
#include <shared/mat.h>

#define NUM_THREADS 8

typedef struct mult_part_s{
   mat_t A, B, C;
   int step_sz;
   int start_idx;
}*mult_part_t;

void* mat_pmult_thread(mult_part_t part) {
    mat_t A = part->A, B = part->B, C = part->C;
    int start = part->start_idx, step = part->step_sz;

    for(int i = start; i < A->r; i += step)
        for(int j = 0; j < B->c; j++)
            for(int k = 0; k < A->c; k++)
                *mat_at(C, i, j) += *mat_at(A, i, k) * *mat_at(B, k, j);
    return NULL;
}

void mat_pmult(mat_t A, mat_t B, mat_t* C) {
    if(*C==NULL) {
        *C = mat_create(A->r, B->c, 0);
    }

    pthread_t tid[NUM_THREADS];
    struct mult_part_s part[NUM_THREADS];

    for(int i=0; i < NUM_THREADS; i++) {
        part[i].A = A;
        part[i].B = B;
        part[i].C = *C;
        part[i].step_sz = NUM_THREADS;
        part[i].start_idx = i;
        pthread_create(&tid[i], NULL, (void*(*)(void*))mat_pmult_thread, &part[i]);
    }

    for(int i=0; i < NUM_THREADS; i++)
        pthread_join(tid[i], NULL);

}

int main() {
    mat_t A = mat_load();

    mat_t B = NULL;
    uint64_t _, avg;
    BENCHMARK(mat_pmult(A, A, &B), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    mat_free(B);
    mat_free(A);

    return 0;
}


