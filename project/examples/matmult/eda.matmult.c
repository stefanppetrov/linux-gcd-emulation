#include <eda.h>
#include <stdio.h>
#include <shared/mat.h>

void mat_pmult(mat_t A, mat_t B, mat_t* C) {
    if(*C==NULL) {
        *C = mat_create(A->r, B->c, 0);
    }

    eda_queue_t q = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);

    eda_apply_sched(q, A->r, EDA_SCHED_DYNAMIC, ^(size_t i) {
        for (size_t j = 0; j < B->c; j++)
            for (size_t k = 0; k < A->c; k++)
                *mat_at(*C, i, j) += *mat_at(A, i, k) * *mat_at(B, k, j);
        });

}

int main(void){
    mat_t A = mat_load();

    mat_t B = NULL;
    uint64_t _, avg;
    BENCHMARK(mat_pmult(A, A, &B), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    mat_free(B);
    mat_free(A);

    return 0;
}

