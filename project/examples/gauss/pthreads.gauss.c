//
// Created by spp on 10/03/18.
//

#include <pthread.h>
#include <shared/mat.h>

#define PMAX 8

typedef struct row_eliminator_s{
    mat_t M;
    size_t row;
    size_t offset;
    size_t stride;
}* row_eliminator_t;

void* row_eliminate(row_eliminator_t elim) {
    mat_t A = elim->M;
    size_t r = elim->row;

    for(size_t i = r+elim->offset; i < A->r; i+=elim->stride){
        double factor = *mat_at(A, i, r);
        for (size_t c = r; c < A->c; c++) {
            *mat_at(A, i, c) = *mat_at(A, i, c) - factor * (*mat_at(A, r, c));
        }
    }
}

int mat_pgauss(mat_t A) {
    size_t elem_sz = sizeof(*A->data);
    double* tmp = malloc(A->c * elem_sz);

    for(size_t r = 0; r < A->r; r++){

        if(!*mat_at(A, r, r)) {
            int found=0;
            for(size_t i = r+1; i < A->r; i++)
                if(*mat_at(A, i, r)){
                    memcpy(tmp, mat_at(A, r, 0), A->c * elem_sz );
                    memcpy(mat_at(A, r, 0), mat_at(A, i, 0), A->c * elem_sz);
                    memcpy(mat_at(A, i, 0), tmp, A->c * elem_sz);
                    found = 1;
                    break;
                }
            if(!found)
                return -1;
        }

        double scale = *mat_at(A,r,r);
        for(size_t c = 0; c < A->c ;c++)
            *mat_at(A, r, c) /= scale;


        struct row_eliminator_s elims[PMAX];
        pthread_t tids[PMAX];

        size_t max_threads = PMAX < A->r - r - 1 ? PMAX : A->r - r -1;

        for(size_t i = 0; i < max_threads; i++) {
            elims[i].M = A;
            elims[i].row = r;
            elims[i].offset = i+1;
            elims[i].stride = max_threads;

            if(i>0)
                pthread_create(&tids[i], NULL, row_eliminate, &elims[i]);
        }

        if(max_threads) {
           row_eliminate(&elims[0]);
        }

        for(size_t i = 1; i < max_threads; i++)
            pthread_join(tids[i], NULL);
    }

    free(tmp);
    return 0;
}

int main() {

    mat_t A = mat_create(2000,2000,1);
    mat_t B = mat_cpy(A);

    struct timeval sec, par;
    TIME_USEC(mat_gauss(B), sec);
    TIME_USEC(mat_pgauss(A), par);

    printf("%lf\n", TIME2FLOAT(sec)/TIME2FLOAT(par));
}
