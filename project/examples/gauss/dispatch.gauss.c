#include <dispatch/dispatch.h>
#include <shared/mat.h>

int mat_pgauss(mat_t A) {
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    size_t elem_sz = sizeof(*A->data);
    double* tmp = malloc(A->c * elem_sz);

    for(size_t r = 0; r < A->r; r++){

        if(!*mat_at(A, r, r)) {
            int found=0;
            for(size_t i = r+1; i < A->r; i++)
                if(*mat_at(A, i, r)){
                    memcpy(tmp, mat_at(A, r, 0), A->c * elem_sz );
                    memcpy(mat_at(A, r, 0), mat_at(A, i, 0), A->c * elem_sz);
                    memcpy(mat_at(A, i, 0), tmp, A->c * elem_sz);
                    found = 1;
                    break;
                }
            if(!found)
                return -1;
        }

        double scale = *mat_at(A,r,r);
        for(size_t c = 0; c < A->c ;c++)
            *mat_at(A, r, c) /= scale;

        dispatch_apply(A->r - (r+1), q, ^(size_t iter){
            size_t i = iter + r + 1;

            double factor = *mat_at(A, i, r);
            for(size_t c = r; c < A->c; c++) {
                *mat_at(A, i, c) = *mat_at(A, i, c) - factor * (*mat_at(A, r, c));
            }
        });
    }

    free(tmp);
    return 0;
}

int main() {

    mat_t A = mat_load();

    uint64_t _, avg;
    BENCHMARK(mat_pgauss(A), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    mat_free(A);
}
