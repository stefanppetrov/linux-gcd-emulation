#!/bin/bash

(
BENCHMARK=${1}
cd "./${BENCHMARK}"
ALTS=$(ls | grep "\.${BENCHMARK}$")

POSITIONAL=(${1})
while [[ $# -gt 0 ]]; do
    shift
    key=${1}
    case ${key} in
        -p|--profile-only)
        PROFILE_ONLY=1
        shift; shift
        ;;

        -*)
        printf "Unknown option: %s\n" ${key}
        shift; shift
        ;;

        *)
        POSITIONAL+=(${key})
        shift; shift
        ;;
    esac
done
set -- "${POSITIONAL[@]}"

if [[ -f ./limits.cfg ]]
then
    source ./limits.cfg
fi

START=${3:-${START:-100}}
END=${2:-${END:-1000}}
STEP=${4:-${STEP:-25}}
PROFILE_SAMPLE=${PROFILE_SAMPLE:-${END}}

if [[ -z ${PROFILE_ONLY} ]]
then
    for sample in $(seq ${START} ${STEP} ${END}); do
        printf "Sample %s\n" "${sample}"
        ./gen-${BENCHMARK} ${sample} > ${sample}.in
        for alt in ${ALTS} ; do
            printf '== %s ==\n' "${alt}"
            ./${alt} ${sample} < ${sample}.in >> ${alt}.out
        done
    done
else
    ./gen-${BENCHMARK} ${PROFILE_SAMPLE} > ${PROFILE_SAMPLE}.in
fi

echo "Profiling using input for sample ${PROFILE_SAMPLE}"
for alt in ${ALTS} ; do
    printf '== %s ==\n' "${alt}"
    valgrind -q --tool=massif --time-unit=ms --massif-out-file=massif.${alt}.out ./${alt} ${PROFILE_SAMPLE} < ${PROFILE_SAMPLE}.in
    valgrind -q --tool=callgrind --callgrind-out-file=callgrind.${alt}.out ./${alt} ${PROFILE_SAMPLE} < ${PROFILE_SAMPLE}.in
done
rm *.in
)
