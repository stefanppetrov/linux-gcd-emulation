//
// Created by spp on 09/03/18.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <shared/graph.h>

int main(int argc, char* argv[]) {
    if(argc < 2)
        return 0;

    size_t n = strtoul(argv[1], NULL, 10);

    graph_t graph = graph_create(n, 1);

    printf("%zu\n", n);
    for(size_t i = 0; i < n; i++){
        for(size_t j = 0; j < n; j++) {
            printf("%d ", graph_edge(graph, i, j));
        }
        printf("\n");
    }
    return 0;
}