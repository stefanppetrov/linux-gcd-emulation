#include <shared/graph.h>
#include <dispatch/dispatch.h>
#include <stdio.h>

void _pdfs(graph_t g, 
           size_t start,
           ssize_t *parents,
           dispatch_queue_t taskq,
           dispatch_queue_t syncq,
           dispatch_group_t grp,
           size_t *visited) {

    for(size_t i = 0; i < g->n; i++){
        if(*visited == g->n) {
            return;
        }

        if(! graph_edge(g, start, i)) {
            continue;
        }

        __block int visit = 0;
        dispatch_sync(syncq, ^{
            if(parents[i] < 0){
                parents[i] = start;
                visit = 1;
                ++*visited;
            }
        });

        if(visit) {
            dispatch_group_async(grp, taskq, ^{
                _pdfs(g, i, parents, taskq, syncq, grp, visited);
            });
        }
   }
}

void pdfs(graph_t g, size_t start, ssize_t *parents) {
    dispatch_queue_t taskq = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_queue_t syncq = dispatch_queue_create("sync", NULL);

    dispatch_group_t group = dispatch_group_create();

    memset(parents, -1, g->n * sizeof(ssize_t));
    parents[start] = start;

    __block size_t visited = 1;
    dispatch_group_async(group, taskq, ^{
        _pdfs(g, start, parents, taskq, syncq, group, &visited);
    });

    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    dispatch_release(syncq);
    dispatch_release(group);
}


int main() {

    graph_t graph = graph_load();
    ssize_t* parents = malloc(graph->n * sizeof(ssize_t));

    uint64_t _, avg;
    BENCHMARK(pdfs(graph, 0, parents), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    free(parents);
    graph_free(graph);
    return 0;
}
