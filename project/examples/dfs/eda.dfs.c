#include <shared/graph.h>
#include <eda.h>
#include <stdio.h>

void _dfs_para(graph_t g,
               size_t start,
               ssize_t* parents,
               eda_queue_t taskq,
               eda_queue_t syncq,
               eda_group_t grp,
               size_t* visited) {

    for(size_t i = 0; i < g->n; i++) {
        if(*visited == g->n) {
            return;
        }

        if (!graph_edge(g, start, i)) {
            continue;
        }

        __block int visit = 0;
        eda_sync(syncq, ^{
            if (parents[i] < 0) {
                parents[i] = start;
                visit = 1;
                ++*visited;
            }
        });

        if (visit) {
            eda_group_async(grp, taskq, ^{
                _dfs_para(g, i, parents, taskq, syncq, grp, visited);
            });
        }
    }
}

void pdfs(graph_t g, size_t start, ssize_t *parents) {
    eda_queue_t taskq = eda_queue_get_global(EDA_QUEUE_PRIO_DEFAULT, EDA_QUEUE_FLAG_DEFAULT);
    eda_queue_t syncq = eda_queue_create("", EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_SERIAL);
    eda_group_t grp = eda_group_create();

    memset(parents, -1, g->n * sizeof(ssize_t));
    parents[start] = start;

    __block size_t visited = 1;
    eda_group_async(grp, taskq, ^{
        _dfs_para(g, start, parents, taskq, syncq, grp, &visited);
    });

    eda_group_wait(grp, EDA_TIME_NEVER);

    eda_obj_release(grp);
    eda_obj_release(syncq);
}

int main() {

    graph_t graph = graph_load();
    ssize_t* parents = malloc(graph->n * sizeof(ssize_t));

    uint64_t _, avg;
    BENCHMARK(pdfs(graph, 0, parents), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    free(parents);
    graph_free(graph);
    return 0;
}
