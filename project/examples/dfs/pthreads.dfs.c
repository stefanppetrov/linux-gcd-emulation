#include "../shared/benchmark.h"

#include "../shared/graph.h"
#include <pthread.h>
#include <semaphore.h>

#define P 8

typedef struct pdfs_task_s {
    ssize_t origin;
    struct pdsfs_task_s* next;
}* pdfs_task_t;

typedef struct pdfs_q_s{
    pthread_mutex_t qlock;
    pthread_mutex_t visited_lock;
    pthread_cond_t condvar;
    sem_t sem;
    size_t len;
    graph_t graph;
    ssize_t* parents;
    size_t visited;
    pdfs_task_t head;
    pdfs_task_t tail;
}* pdfs_q_t;

void _pdfs_push(pdfs_q_t q, ssize_t node) {
    pdfs_task_t task = calloc(1, sizeof(struct pdfs_task_s));
    task->origin = node;
    pthread_mutex_lock(&q->qlock);
    if(!q->head)
        q->head = task;
    else
        q->tail->next = task;
    q->tail = task;
    if(++q->len == 0){
        pthread_cond_signal(&q->condvar);
    }
    pthread_mutex_unlock(&q->qlock);
}

int _pdfs_task(pdfs_q_t q, size_t origin){
    graph_t graph = q->graph;

    if(origin<0){
        return -1;
    }

    for(size_t i=0; i<graph->n; i++) {
        if(q->visited >= graph->n){
            _pdfs_push(q, -1);
            return -1;
        }

        if(q->parents[i]>=0){
            continue;
        }

        if(!graph_edge(graph, origin, i)){
            continue;
        }

        pthread_mutex_lock(&q->visited_lock);
        if(q->parents[i]<0) {
            q->parents[i]=origin;
            q->visited++;
            _pdfs_push(q, i);
        }
        pthread_mutex_unlock(&q->visited_lock);
    }
    return 0;
}

void* _pdfs_worker(pdfs_q_t q){
    pdfs_task_t task;

    while(1){
        pthread_mutex_lock(&q->qlock);
            while(!q->len){
                pthread_cond_wait(&q->condvar, &q->qlock);
            }
            q->len--;
            task = q->head;
            q->head = task->next;
            if(task == q->tail) q->tail = NULL;
        pthread_mutex_unlock(&q->qlock);

        int rval = _pdfs_task(q, task->origin);
        free(task);

        if(rval){
            sem_post(&q->sem);
            return 1;
        }
    }
}

void pdfs(graph_t g,
           size_t start,
           ssize_t* parents){
    memset(parents, -1, g->n*sizeof(ssize_t));
    parents[start]=start;

    pthread_t* tids = calloc(P, sizeof(pthread_t));

    pdfs_q_t q = calloc(1, sizeof(struct pdfs_q_s));
    pthread_mutex_init(&q->visited_lock, NULL);
    pthread_mutex_init(&q->qlock, NULL);
    pthread_cond_init(&q->condvar, NULL);
    sem_init(&q->sem, 0, 0);
    q->graph=g;
    q->parents=parents;
    q->visited = 1;

    _pdfs_task(q, start);

    for(int i=0; i<P; i++) {
        pthread_create(&tids[i], NULL, _pdfs_worker, q);
    }

    for(int i=0; i<P; i++) {
        sem_wait(&q->sem);
    }

    while(q->head){
        pdfs_task_t head = q->head->next;
        free(q->head);
        q->head = head;
    }
    free(q);
}

int main(){
    graph_t graph = graph_load();
    ssize_t* parents = malloc(graph->n * sizeof(ssize_t));

    uint64_t _, avg;
    BENCHMARK(pdfs(graph, 0, parents), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    free(parents);
    graph_free(graph);
    return 0;
}
