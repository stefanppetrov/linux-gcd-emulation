#include <eda.h>
#include <stdio.h>
#include <stdatomic.h>
#include "shared/benchmark.h"

#define F 5

static atomic_uint_fast64_t c = 0;

void task(int i, eda_queue_t q, eda_group_t g) {
    int dummy = 0;

    if(i <= 0) {
        for (int j = 0; j < F*100; j++) {
            dummy ^= i;
        }
        return;
    }

    for(int j = 0; j < F*10; j ++) {
        dummy ^= i;
    }

    eda_group_async(g, q, ^{
        task(i-2, q, g);
    });

    for(int j = 0; j < F*50; j ++) {
        dummy ^= i;
    }

    eda_group_async(g, q, ^{
        task(i-1, q, g);
    });

    for(int j = 0; j < F*100; j ++) {
        dummy ^= i;
    }
    c++;
}

void synthetic(int k) {
    eda_queue_t qh = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);
    eda_queue_t qd = eda_queue_get_global(EDA_QUEUE_PRIO_DEFAULT, EDA_QUEUE_FLAG_DEFAULT);
    eda_queue_t ql = eda_queue_get_global(EDA_QUEUE_PRIO_LOW, EDA_QUEUE_FLAG_DEFAULT);

    eda_group_t g = eda_group_create();

    for(int i=0; i<k; i++) {
        eda_block_t t = ^{
            task(i, qh, g);
        };

        eda_group_async(g, ql, t);
        eda_group_async(g, qd, t);
        eda_group_async(g, qh, t);
    }

    eda_group_wait(g, EDA_TIME_NEVER);
    eda_obj_release(g);
}

int main() {

    int k=10;

    scanf("%d", &k);

    uint64_t _, avg;
    BENCHMARK(synthetic(k), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    return 0;
}
