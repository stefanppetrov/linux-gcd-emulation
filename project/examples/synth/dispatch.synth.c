#include <dispatch/dispatch.h>
#include <stdio.h>
#include <stdatomic.h>
#include "shared/benchmark.h"

#define F 5

static atomic_uint_fast64_t c = 0;

void task(int i, dispatch_queue_t q, dispatch_group_t g) {
    int dummy = 0;

    if(i <= 0) {
        for (int j = 0; j < 300; j++) {
            dummy ^= i;
        }
        return;
    }

    for(int j = 0; j < F*10; j ++) {
        dummy ^= i;
    }

    dispatch_group_async(g, q, ^{
        task(i-2, q, g);
    });

    for(int j = 0; j < F*50; j ++) {
        dummy ^= i;
    }

    dispatch_group_async(g, q, ^{
        task(i-1, q, g);
    });

    for(int j = 0; j < F*100; j ++) {
        dummy ^= i;
    }
    c++;
}

void synthetic(int k) {
    dispatch_queue_t qh = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_queue_t qd = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_queue_t ql = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);

    dispatch_group_t g = dispatch_group_create();

    for(int i=0; i<k; i++) {
        dispatch_block_t t = ^{
            task(i, qh, g);
        };

        dispatch_group_async(g, ql, t);
        dispatch_group_async(g, qd, t);
        dispatch_group_async(g, qh, t);
    }

    dispatch_group_wait(g, DISPATCH_TIME_FOREVER);
    dispatch_release(g);
}

int main() {
    int k=10;
    scanf("%d", &k);

    uint64_t _, avg;
    BENCHMARK(synthetic(k), 10, _, avg, _);
    printf("%zu.%06zu\n", avg/1000000, avg%1000000);

    return 0;
}