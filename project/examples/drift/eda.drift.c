//
// Created by spp on 28/03/18.
//

#include <eda.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <malloc.h>
#include <stdlib.h>
#include <shared/benchmark.h>

struct timer_metrics_s {
    uint64_t count;
    struct timeval first;
    double jitter;
    double drift;
};

#define INTERVAL_CNT 6
const uint64_t USEC_INTERVALS[INTERVAL_CNT] = {10, 100, 1000, 10000, 100000, 1000000};
eda_source_t* timers[INTERVAL_CNT] = {NULL};
struct timer_metrics_s* timer_metrics[INTERVAL_CNT] = {NULL};

_Atomic uint64_t serviced = 0;
_Atomic uint64_t missed = 0;

eda_source_t drift_timer(int interval, struct timer_metrics_s* metrics) {
    static int first = 0;

    eda_queue_t tq = NULL;
    if(interval == 0) {
        tq = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, 0);
    }

    eda_source_t timer = eda_source_create(EDA_TIMER_SOURCE, 0, 0, tq);
    eda_source_set_timer(timer, EDA_TIME_NOW, USEC_INTERVALS[interval]*NSEC_PER_USEC);

    eda_source_set_event_handler(timer, ^(intptr_t fire_cnt){
        struct timeval now;
        gettimeofday(&now, NULL);

        if(!metrics->count){
            metrics->first = now;
        }

        struct timeval target = TIMEVAL_INIT(USEC_INTERVALS[interval] * metrics->count), diff;
        timeradd(&target, &metrics->first, &target);
        timersub(&target, &now, &diff);
        double jitter = TIME2FLOAT(diff);
        metrics->drift += jitter - metrics->jitter;
        metrics->jitter = jitter;
        metrics->count++;

        serviced++;
        missed += fire_cnt-1;
    });
    eda_source_start(timer);
    return timer;
}

int main() {

    eda_sem_t sem = eda_sem_create(0);

    int timer_cnt = 3;
    scanf("%d", &timer_cnt);

    for(int i=0; i < INTERVAL_CNT; i++) {
        timers[i] = calloc(timer_cnt, sizeof(eda_source_t));
        timer_metrics[i] = calloc(timer_cnt, sizeof(struct timer_metrics_s));

        for(int timer = 0; timer < timer_cnt; timer++) {

            timers[i][timer] = drift_timer(i, &timer_metrics[i][timer]);
        }
    }

    eda_time_t timeout = eda_time(EDA_CLOCK_SYS, EDA_TIME_NOW, 20*NSEC_PER_SEC);
    eda_sem_wait(sem, timeout);
    eda_obj_release(sem);

    double avg_drift = 0.0;
    for(int i=0; i < INTERVAL_CNT; i++) {
        for(int t=0; t < timer_cnt; t++)
            avg_drift += fabs(timer_metrics[i][t].drift / (timer_metrics[i][t].count-1));
    }
    avg_drift /= INTERVAL_CNT*timer_cnt;

    printf("%zu %zu %lf\n", serviced, missed, avg_drift);
    return 0;
}

