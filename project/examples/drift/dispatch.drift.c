//
// Created by spp on 10/03/18.
//

#include <dispatch/dispatch.h>
#include <stdio.h>
#include <stdio.h>
#include <sys/time.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <shared/benchmark.h>

struct timer_metrics_s {
    uint64_t count;
    struct timeval first;
    double jitter;
    double drift;
};

#define INTERVAL_CNT 6
const uint64_t USEC_INTERVALS[INTERVAL_CNT] = {10, 100, 1000, 10000, 100000, 1000000};
dispatch_source_t* timers[INTERVAL_CNT] = {NULL};
struct timer_metrics_s* timer_metrics[INTERVAL_CNT] = {NULL};

_Atomic uint64_t serviced = 0;
_Atomic uint64_t missed = 0;

dispatch_source_t drift_timer(int interval, struct timer_metrics_s* metrics) {
    dispatch_queue_t tq = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    if(interval == 0) {
        tq = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    }

    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, tq);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, USEC_INTERVALS[interval]*NSEC_PER_USEC, USEC_PER_SEC);

    dispatch_source_set_event_handler(timer, ^{
        uint64_t fire_cnt = dispatch_source_get_data(timer);

        struct timeval now;
        gettimeofday(&now, NULL);

        if(!metrics->count){
            metrics->first = now;
        }

        struct timeval target = TIMEVAL_INIT(USEC_INTERVALS[interval] * metrics->count), diff;
        timeradd(&target, &metrics->first, &target);
        timersub(&target, &now, &diff);
        double jitter = TIME2FLOAT(diff);
        metrics->drift += jitter - metrics->jitter;
        metrics->jitter = jitter;
        metrics->count++;

        serviced++;
        missed += fire_cnt-1;
    });
    dispatch_resume(timer);
    return timer;
}

int main() {

    dispatch_semaphore_t sem = dispatch_semaphore_create(0);

    int timer_cnt = 3;
    scanf("%d", &timer_cnt);

    for(int i=0; i < INTERVAL_CNT; i++) {
        timers[i] = calloc(timer_cnt, sizeof(dispatch_source_t));
        timer_metrics[i] = calloc(timer_cnt, sizeof(struct timer_metrics_s));

        for(int timer = 0; timer < timer_cnt; timer++) {
            timers[i][timer] = drift_timer(i, &timer_metrics[i][timer]);
        }
    }

    dispatch_semaphore_wait(sem, dispatch_time(DISPATCH_TIME_NOW, 20*NSEC_PER_SEC));
    dispatch_release(sem);

    double avg_drift = 0.0;
    for(int i=0; i < INTERVAL_CNT; i++) {
        for(int t=0; t < timer_cnt; t++)
            avg_drift += fabs(timer_metrics[i][t].drift / (timer_metrics[i][t].count-1));
    }
    avg_drift /= INTERVAL_CNT*timer_cnt;

    printf("%zu %zu %lf\n", serviced, missed, avg_drift);
    return 0;
}
