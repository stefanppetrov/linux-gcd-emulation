//
// Created by spp on 10/03/18.
//
#include <shared/mat.h>
#include <pthread.h>

#define PMAX 8

typedef struct mat_minmax_s{
    double mmax, mmin;
    size_t start, stride;
    mat_t M;
} *mat_minmax_t;

void* mat_pminmax(mat_minmax_t minmax) {
    mat_t M = minmax->M;

    for (size_t row = minmax->start; row < M->r; row += minmax->stride) {
        for (size_t col = 0; col < M->c; col++) {

            double elem = *mat_at(M, row, col);
            if (minmax->mmax < elem) {
                minmax->mmax = elem;
            }

            if (minmax->mmin > elem) {
                minmax->mmin = elem;
            }
        }
    }
}

typedef struct mat_phist_s {
    mat_t M;
    double mmin;
    double mrange;
    size_t start, stride;
    size_t bincnt;
    double *binsizes;
    size_t *bins;
} * mat_phist_t;

void* mat_phist_local(mat_phist_t histinfo) {
    mat_t M = histinfo->M;

    for (size_t row = histinfo->start; row < M->r; row+=histinfo->stride) {
        for (size_t col = 0; col < M->c; col++) {

            double elem = *mat_at(M, row, col);

            size_t bin = 0;
            for (; bin < histinfo->bincnt; bin++) {
                if (histinfo->binsizes[bin] > elem - histinfo->mmin) {
                    break;
                }
            }

            histinfo->bins[bin]++;
        }
    }
    return NULL;
}

void mat_phist(mat_t A, size_t* bins, size_t bincnt) {
    memset(bins, 0, bincnt*sizeof(*bins));

    double mmax = INT_MIN, mmin = INT_MAX;

    pthread_t threads[PMAX];

    mat_minmax_t minmax = malloc(PMAX * sizeof(struct mat_minmax_s));

    for(size_t i = 0; i < PMAX; i++) {
        minmax[i].M = A;
        minmax[i].start = i;
        minmax[i].stride = PMAX;
        minmax[i].mmax = INT_MIN;
        minmax[i].mmin = INT_MAX;

        pthread_create(&threads[i], NULL, mat_pminmax, &minmax[i]);
    }

    for(size_t i = 0; i < PMAX; i++) {
        pthread_join(threads[i], NULL);
        if(mmax < minmax[i].mmax) mmax = minmax[i].mmax;
        if(mmin > minmax[i].mmin) mmin = minmax[i].mmin;
    }

    free(minmax);

    double mrange = mmax - mmin + 1;

    double* binsizes = calloc(bincnt, sizeof(double));
    binsizes[0] = mrange / bincnt;
    for(size_t i = 1; i < bincnt; i++) {
        binsizes[i] = (mrange / bincnt);
        binsizes[i] += binsizes[i-1];
    }

    mat_phist_t histinfo = calloc(PMAX, sizeof(struct mat_phist_s));
    for(size_t i = 0; i < PMAX; i++) {
        histinfo[i].M = A;
        histinfo[i].start = i;
        histinfo[i].stride = PMAX;
        histinfo[i].bincnt = bincnt;
        histinfo[i].bins = calloc(bincnt, sizeof(double));
        histinfo[i].binsizes = binsizes;
        histinfo[i].mmin = mmin;
        histinfo[i].mrange = mrange;

        pthread_create(&threads[i], NULL, mat_phist_local, &histinfo[i]);
    }

    for(size_t i = 0; i < PMAX; i++)
        pthread_join(threads[i], NULL);

    for(size_t bin = 0; bin < bincnt; bin++)
        for(size_t locale = 0; locale < PMAX; locale++)
            bins[bin] += histinfo[locale].bins[bin];

    for(size_t i = 0; i < PMAX; i++){
        free(histinfo[i].bins);
    }
    free(histinfo);
}

#define HIST_SIZE 131

int main() {

    mat_t A = mat_load();

    size_t  phist[HIST_SIZE] = {0};

    uint64_t worst, avg, best;
    BENCHMARK( mat_phist(A, phist, HIST_SIZE), 20, worst, avg, best);

    printf("%zu.%06zu\n", avg/1000000, avg%1000000);
}

