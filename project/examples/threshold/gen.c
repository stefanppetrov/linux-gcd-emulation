#include <stdio.h>
#include <shared/mat.h>

int main(int argc, char* argv[]){
    size_t n = strtoul(argv[1], NULL, 10);

    mat_t mat = mat_create(n,n,1);

    printf("%zu\n", n);
    for(size_t i = 0; i < n; i++) {
        for(size_t j = 0; j < n; j++)
            printf("%lf ", *mat_at(mat, i, j));
        printf("\n");
    }
}

