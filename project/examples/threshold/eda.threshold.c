//
// Created by spp on 10/03/18.
//
#include <shared/mat.h>
#include <eda.h>

#define PMAX 8

void mat_phist(mat_t A, size_t* bins, size_t bincnt) {

    eda_queue_t globq = eda_queue_get_global(EDA_QUEUE_PRIO_HIGH, EDA_QUEUE_FLAG_DEFAULT);

    eda_group_t syncg = eda_group_create();

    __block double mmax = INT_MIN, mmin = INT_MAX;

    double *mmax_local = malloc(PMAX * sizeof(double)),
           *mmin_local = malloc(PMAX * sizeof(double));

    for(size_t i = 0; i < PMAX; i++) {
        mmax_local[i] = INT_MIN;
        mmin_local[i] = INT_MAX;
        eda_group_async(syncg, globq, ^{
            for (size_t row = i; row < A->r; row += PMAX) {
                for (size_t col = 0; col < A->c; col++) {

                    double elem = *mat_at(A, row, col);
                    if (mmax_local[i] < elem) {
                        mmax_local[i] = elem;
                    }

                    if (mmin_local[i] > elem) {
                        mmin_local[i] = elem;
                    }
                }
            }
        });
    }

    eda_group_wait(syncg, EDA_TIME_NEVER);

    for(size_t i = 0; i < PMAX; i++) {
        if(mmax < mmax_local[i]) mmax = mmax_local[i];
        if(mmin > mmin_local[i]) mmin = mmin_local[i];
    }

    free(mmax_local);
    free(mmin_local);

    double mrange = mmax - mmin + 1;

    double* binsizes = calloc(bincnt, sizeof(double));
    binsizes[0] = mrange / bincnt;
    for(size_t i = 1; i < bincnt; i++) {
        binsizes[i] = (mrange / bincnt);
        binsizes[i] += binsizes[i-1];
    }

    size_t *bins_local = calloc(PMAX*bincnt, sizeof(size_t));
    for(size_t i = 0; i < PMAX; i++) {

        eda_group_async(syncg, globq, ^{
            for (size_t row = i; row < A->r; row+=PMAX) {
                for (size_t col = 0; col < A->c; col++) {

                    double elem = *mat_at(A, row, col);

                    size_t bin = 0;
                    for (; bin < bincnt; bin++) {
                        if (binsizes[bin] > elem - mmin) {
                            break;
                        }
                    }

                    bins_local[i * bincnt + bin]++;
                }
            }
        });
    }

    eda_group_wait(syncg, EDA_TIME_NEVER);
    for(size_t bin = 0; bin < bincnt; bin++)
        for(size_t locale = 0; locale < PMAX; locale++)
            bins[bin] += bins_local[locale*bincnt + bin];
    free(bins_local);
    eda_obj_release(syncg);
}

#define HIST_SIZE 133

int main() {
    mat_t A = mat_load();

    size_t  phist[HIST_SIZE] = {0};

    uint64_t worst, avg, best;
    BENCHMARK( mat_phist(A, phist, HIST_SIZE), 20, worst, avg, best);

    printf("%zu.%06zu\n", avg/1000000, avg%1000000);
}

