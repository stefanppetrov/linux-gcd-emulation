//
// Created by spp on 10/03/18.
//
#include <shared/mat.h>
#include <dispatch/dispatch.h>

#define PMAX 8

void mat_phist(mat_t A, size_t* bins, size_t bincnt) {

    dispatch_queue_t globq = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);

    dispatch_group_t syncg = dispatch_group_create();

    __block double mmax = INT_MIN, mmin = INT_MAX;

    double *mmax_local = malloc(PMAX * sizeof(*A->data)),
           *mmin_local = malloc(PMAX * sizeof(*A->data));

    for(size_t i = 0; i < PMAX; i++) {
        mmax_local[i] = INT_MIN;
        mmin_local[i] = INT_MAX;
        dispatch_group_async(syncg, globq, ^{
            for (size_t row = i; row < A->r; row += PMAX) {
                for (size_t col = 0; col < A->c; col++) {

                    double elem = *mat_at(A, row, col);
                    if (mmax_local[i] < elem) {
                        mmax_local[i] = elem;
                    }

                    if (mmin_local[i] > elem) {
                        mmin_local[i] = elem;
                    }
                }
            }
        });
    }

    dispatch_group_wait(syncg, DISPATCH_TIME_FOREVER);

    for(size_t i = 0; i < PMAX; i++) {
        if(mmax < mmax_local[i]) mmax = mmax_local[i];
        if(mmin > mmin_local[i]) mmin = mmin_local[i];
    }

    free(mmax_local);
    free(mmin_local);

    double mrange = mmax - mmin + 1;

    double* binsizes = calloc(bincnt, sizeof(double));
    binsizes[0] = mrange / bincnt;
    for(size_t i = 1; i < bincnt; i++) {
        binsizes[i] = (mrange / bincnt);
        binsizes[i] += binsizes[i-1];
    }

    size_t *bins_local = calloc(PMAX*bincnt, sizeof(size_t));
    for(size_t i = 0; i < PMAX; i++) {

        dispatch_group_async(syncg, globq, ^{
            for (size_t row = i; row < A->r; row+=PMAX) {
                for (size_t col = 0; col < A->c; col++) {

                    double elem = *mat_at(A, row, col);

                    size_t bin = 0;
                    for (; bin < bincnt; bin++) {
                        if (binsizes[bin] > elem - mmin) {
                            break;
                        }
                    }

                    bins_local[i * bincnt + bin]++;
                }
            }
        });
    }

    dispatch_group_wait(syncg, DISPATCH_TIME_FOREVER);
    for(size_t bin = 0; bin < bincnt; bin++)
        for(size_t locale = 0; locale < PMAX; locale++)
            bins[bin] += bins_local[locale*bincnt + bin];
    free(bins_local);
    dispatch_release(syncg);

}

#define HIST_SIZE 133

int main() {

    mat_t A = mat_load();

    size_t  phist[HIST_SIZE] = {0};

    uint64_t worst, avg, best;
    BENCHMARK( mat_phist(A, phist, HIST_SIZE), 20, worst, avg, best);

    printf("%zu.%06zu\n", avg/1000000, avg%1000000);
}

