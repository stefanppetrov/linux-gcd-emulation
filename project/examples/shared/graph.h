//
// Created by spp on 07/03/18.
//

#ifndef EDA_GRAPH_H
#define EDA_GRAPH_H

#include "benchmark.h"
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct graph_s {
    size_t n;
    uint8_t  *edges;
}* graph_t;

__always_inline uint8_t graph_edge(graph_t g, size_t u, size_t v) {
    return g->edges[u*g->n + v] | g->edges[v*g->n + u];
}

graph_t graph_create(size_t n, int fill) {
    graph_t result = malloc(sizeof(struct graph_s));
    result->n = n;
    result->edges = calloc(n*n, sizeof(uint8_t));

    if(!fill){
        return result;
    }

    size_t log2n = (sizeof(size_t)<<3) - __builtin_clzl(n);
    srandom(n);
    for(size_t i = 0; i < n; i++)
        for(size_t j=i+1; j<n; j++){
            result->edges[i*n+j] = (result->edges[n*j+i] = !(random() % log2n));

        }

    return result;
}

void graph_free(graph_t g) {
    free(g->edges);
    free(g);
}

void _dfs(graph_t g, size_t start, ssize_t* parents) {

    for(size_t i=0; i < g->n; i++) {
        if(! graph_edge(g, start, i)) {
            continue;
        }

        if(parents[i] < 0) {
            parents[i] = start;
            _dfs(g, i, parents);
        }
    }
}

void dfs(graph_t g, size_t start, ssize_t* parents) {
    memset(parents, -1, g->n * sizeof(ssize_t));
    parents[start] = start;
    _dfs(g, start, parents);
}

int validate_dfs(graph_t g, ssize_t* parents){
    for(size_t i=0; i<g->n; i++) {
        size_t start = i, cnt=0;
        while(start != parents[start]){
            if(cnt>g->n){
                return -2;
            }
            if(!graph_edge(g, start, parents[start])){
                printf("%zu %zu %d\n", start, parents[start], graph_edge(g, start, parents[start]));
                return -1;
            }

            start = parents[start];
            cnt++;
        }
    }
    return 0;
}

void pdfs(graph_t, size_t, ssize_t*);

graph_t graph_load() {
    size_t size;
    graph_t res;

    scanf("%zu", &size);

    res = graph_create(size, 0);

    for(size_t i = 0; i < size; i++)
        for(size_t j = 0; j < size; j++) {
            scanf("%2"SCNu8, &(res->edges[size*j+i]));
            res->edges[size*i + j] = res->edges[size*j + i];
        }

    return res;
}

#endif //EDA_GRAPH_H
