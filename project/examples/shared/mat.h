#ifndef EDA_MAT_H
#define EDA_MAT_H

#include "benchmark.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#define MAT_MAX 2018

typedef struct mat_s{
    size_t r,c;
    double* data;
}*mat_t;

mat_t mat_create(size_t r, size_t c, int populate) {
    mat_t res = malloc(sizeof(struct mat_s));
    res->data = calloc(r*c,sizeof(double));
    res->r = r;
    res->c = c;

    if(!populate)
        return res;

    srandom(r*c);
    for(size_t i = 0; i < r; i++)
        for(size_t j = 0; j < c; j++)
            res->data[c*i + j] = random() % MAT_MAX;
    return res;
}

void mat_free(mat_t mat) {
    free(mat->data);
    free(mat);
}

static __always_inline double* mat_at(mat_t mat, size_t r, size_t c){
    return &mat->data[mat->c * r + c];
}

mat_t mat_cpy(mat_t A) {
    mat_t copy = mat_create(A->r, A->c, 0);
    memcpy(copy->data, A->data, A->r*A->c*sizeof(*A->data));
    return copy;
}

int mat_cmp(mat_t A, mat_t B) {
    if(A->r != B->r || A->c != B->c){
        return -1;
    }

    for(size_t i = 0; i < A->r; i++)
        for(size_t j = 0; j < A->c; j++)
            if(*mat_at(A,i,j) - *mat_at(B,i,j) > 0.001){
                return -1;
            }
    return 0;
}

mat_t mat_mult(mat_t A, mat_t B) {
    mat_t C = malloc(sizeof(struct mat_s));
    C->data = calloc(A->r*B->c, sizeof(*A->data));

    for(size_t i = 0; i < A->r; i++)
        for(size_t j = 0; j < B->c; j++)
            for(size_t k = 0; k < A->c; k++)
                *mat_at(C, i, j) += *mat_at(A, i, k) * *mat_at(B, k, j);
    return C;
}

void mat_hist(mat_t A, size_t* bins, size_t bincnt) {

    double mmax = INT_MIN, mmin = INT_MAX;

    for(size_t i = 0; i < A->r; i++)
        for(size_t j = 0; j < A->c; j++) {

            double elem = *mat_at(A, i, j);
            mmax = mmax < elem ? elem : mmax;
            mmin = mmin > elem ? elem : mmin;
        }

    double mrange = mmax - mmin + 1;

    double* binsizes = calloc(bincnt, sizeof(*A->data));
    binsizes[0] = mrange / bincnt;
    for(size_t i = 1; i < bincnt; i++) {
        binsizes[i] = (mrange / bincnt);
        binsizes[i] += binsizes[i-1];
    }

    for(size_t i = 0; i < A->r; i++)
        for(size_t j = 0; j < A->c; j++) {

            double norm = *mat_at(A, i, j) - mmin;
            for(size_t k=0; k < bincnt; k++) {
                if(binsizes[k] > norm) {
                    bins[k]++;
                    break;
                }
            }
        }
    free(binsizes);
}

void mat_phist(mat_t A, size_t* bins, size_t bincnt);

int mat_gauss(mat_t A) {

    size_t elem_sz = sizeof(*A->data);
    int* tmp = malloc(A->c * elem_sz);

    for(size_t r = 0; r < A->r; r++){

        if(!*mat_at(A, r, r)) {
            int found=0;
            for(size_t i = r+1; i < A->r; i++)
                if(*mat_at(A, i, r)){
                    memcpy(tmp, mat_at(A, r, 0), A->c * elem_sz );
                    memcpy(mat_at(A, r, 0), mat_at(A, i, 0), A->c * elem_sz);
                    memcpy(mat_at(A, i, 0), tmp, A->c * elem_sz);
                    found = 1;
                    break;
                }
            if(!found)
                return -1;
        }

        double scale = *mat_at(A,r,r);
        for(size_t c = 0; c < A->c; c++)
            *mat_at(A,r,c) /= scale;

        for(size_t i = r+1; i < A->r; i++) {
            double factor = *mat_at(A, i, r);
            for(size_t c = r; c < A->c; c++) {
                *mat_at(A, i, c) = *mat_at(A, i, c) - factor * (*mat_at(A, r, c));
            }
        }
    }

    free(tmp);
    return 0;
}

int mat_pgauss(mat_t A);

mat_t mat_load() {
    size_t size;
    mat_t res;

    scanf("%zu", &size);

    res = mat_create(size, size, 0);

    for(size_t i = 0; i < size; i++)
        for(size_t j = 0; j < size; j++) {
            scanf("%lf", mat_at(res, i, j));
        }

    return res;
}

#endif //EDA_MAT_H
