//
// Created by spp on 09/03/18.
//

#ifndef EDA_BENCHMARKING_H
#define EDA_BENCHMARKING_H

#include <sys/time.h>
#include <stdint.h>

#define TIME_USEC(x, var) { \
    struct timeval start, end; \
    gettimeofday(&start, NULL); \
    x; \
    gettimeofday(&end, NULL); \
    timersub(&end, &start, &(var)); \
}

#define TIMEVAL_INIT(usec) \
{ \
    .tv_usec = ((long)( (usec) % USEC_PER_SEC)), \
    .tv_sec  = ((long)( (usec) / USEC_PER_SEC)) \
}

#define TIME2FLOAT(tv) \
    (tv.tv_sec + tv.tv_usec/1000000.0)

#define BENCHMARK(CODE, S, WORST, AVG, BEST) \
{ \
    struct timeval __runtime;\
    (BEST) = UINT64_MAX, (WORST) = 0, (AVG) = 0; \
    for(int __sample = 0; __sample < (S); __sample++) {\
        TIME_USEC(CODE, __runtime);\
        uint64_t current = __runtime.tv_sec * 1000000 + __runtime.tv_usec;  \
        (BEST) = (BEST) > current ? current : (BEST); \
        (WORST) = (WORST) < current ? current : (WORST); \
        (AVG) += current; \
    } \
    (AVG) /= S; \
}
#endif //EDA_BENCHMARKING_H
