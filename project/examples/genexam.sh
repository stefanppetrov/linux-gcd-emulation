#!/usr/bin/env bash

mkdir ${1}

if [ ${?} -ne 0 ]; then
    exit -1
fi

cp ./template/* ./${1}/ -r
rename "s/template/${1}/g" ./${1}/*
