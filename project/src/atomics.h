//
// Created by spp on 11/03/18.
//

#ifndef EDA_ATOMICS_H
#define EDA_ATOMICS_H

#include <stdatomic.h>

// Convenience macros for atomic operations

#define CAS_STR atomic_compare_exchange_strong
#define CAS_STR_EXPL atomic_compare_exchange_strong_explicit

#define CAS_WEAK atomic_compare_exchange_weak
#define CAS_WEAK_EXPL atomic_compare_exchange_weak_explicit

#define XCHG_RLXD(X,Y) atomic_exchange_explicit(X,Y,memory_order_relaxed)
#define XCHG_ACQ(X,Y) atomic_exchange_explicit(X,Y,memory_order_acquire)
#define XCHG_REL(X,Y) atomic_exchange_explicit(X,Y,memory_order_release)
#define XCHG_ACQREL(X,Y) atomic_exchange_explicit(X,Y,memory_order_acq_rel)
#define XCHG(X,Y) atomic_exchange(X,Y)

#define ADD_RLXD(X,Y) atomic_fetch_add_explicit(X,Y,memory_order_relaxed)
#define ADD_ACQ(X,Y) atomic_fetch_add_explicit(X,Y,memory_order_acquire)
#define ADD_REL(X,Y) atomic_fetch_add_explicit(X,Y,memory_order_release)
#define ADD_ACQREL(X,Y) atomic_fetch_add_explicit(X,Y,memory_order_acq_rel)
#define ADD(X,Y) atomic_fetch_add(X,Y)

#define SUB_RLXD(X,Y) atomic_fetch_sub_explicit(X,Y,memory_order_relaxed)
#define SUB_ACQ(X,Y) atomic_fetch_sub_explicit(X,Y,memory_order_acquire)
#define SUB_REL(X,Y) atomic_fetch_sub_explicit(X,Y,memory_order_release)
#define SUB_ACQREL(X,Y) atomic_fetch_sub_explicit(X,Y,memory_order_acq_rel)
#define SUB(X,Y) atomic_fetch_sub(X,Y)

#define OR_RLXD(X,Y) atomic_fetch_or_explicit(X,Y,memory_order_relaxed)
#define OR_ACQ(X,Y) atomic_fetch_or_explicit(X,Y,memory_order_acquire)
#define OR_REL(X,Y) atomic_fetch_or_explicit(X,Y,memory_order_release)
#define OR_ACQREL(X,Y) atomic_fetch_or_explicit(X,Y,memory_order_acq_rel)
#define OR(X,Y) atomic_fetch_or(X,Y)

#define AND_RLXD(X,Y) atomic_fetch_and_explicit(X,Y,memory_order_relaxed)
#define AND_ACQ(X,Y) atomic_fetch_and_explicit(X,Y,memory_order_acquire)
#define AND_REL(X,Y) atomic_fetch_and_explicit(X,Y,memory_order_release)
#define AND_ACQREL(X,Y) atomic_fetch_and_explicit(X,Y,memory_order_acq_rel)
#define AND(X,Y) atomic_fetch_and(X,Y)

#define XOR_RLXD(X,Y) atomic_fetch_xor_explicit(X,Y,memory_order_relaxed)
#define XOR_ACQ(X,Y) atomic_fetch_xor_explicit(X,Y,memory_order_acquire)
#define XOR_REL(X,Y) atomic_fetch_xor_explicit(X,Y,memory_order_release)
#define XOR_ACQREL(X,Y) atomic_fetch_xor_explicit(X,Y,memory_order_acq_rel)
#define XOR(X,Y) atomic_fetch_xor(X,Y)

#define LOAD_RLXD(X) atomic_load_explicit(X,memory_order_relaxed)
#define LOAD_ACQ(X) atomic_load_explicit(X,memory_order_acquire)
#define LOAD_REL(X) atomic_load_explicit(X,memory_order_release)
#define LOAD_ACQREL(X) atomic_load_explicit(X,memory_order_acq_rel)
#define LOAD(X) atomic_load(X)

#define STORE_RLXD(X, V) atomic_store_explicit(X,V,memory_order_relaxed)
#define STORE_ACQ(X, V) atomic_store_explicit(X,V,memory_order_acquire)
#define STORE_REL(X, V) atomic_store_explicit(X,V,memory_order_release)
#define STORE_ACQREL(X, V) atomic_store_explicit(X,V,memory_order_acq_rel)
#define STORE(X, V) atomic_store(X, V)

#endif //EDA_ATOMICS_H
