#ifndef EDA_QUEUE_H
#define EDA_QUEUE_H

#include "commons.h"
#include "task.h"

// 1 global queue for each priority + event source manager queue
#define EDA_GLOBAL_QUEUE_CNT 5

typedef struct eda_queue_s {
    EDA_OBJ_HEADER(struct eda_queue_s);
    atomic_uint              runlock; // Semaphore-like field for controlling consumers
    uint32_t                 priority;
    eda_flags_t              flags; // Specify queue type and scheduling parameters
    uint32_t                 serial; // The serial number of the queue
    uint32_t                 length;
    _Atomic _eda_task_t      head;
    _Atomic _eda_task_t      tail;
    eda_func_t               invoke; // Consumer function for the queue
    char                     name[64];
} *eda_queue_t;

// Used internally by all dispatching functions
void _eda_async_task(eda_queue_t q, _eda_task_t task);

// Used internally by function creation
int  _eda_queue_init(eda_queue_t, const char* name, uint32_t priority, eda_flags_t flags);

// Michael-Scott queue enqueue
void _eda_queue_push(eda_queue_t queue, _eda_task_t task);

// Michael-Scott queue enqueue extended to enqueue multiple items at once
size_t _eda_queue_push_list(eda_queue_t queue, _eda_task_t h, _eda_task_t tail);

// Optimized dequeue operation for single-consumer / serial queues
_eda_task_t _eda_queue_pull_sc(eda_queue_t queue);

// Michael-Scott queue dequeue
_eda_task_t _eda_queue_pull_mc(eda_queue_t queue);

// Call the right consumer function for the given queue
void _eda_queue_invoke(eda_queue_t edaq);

// Release queue resources
void _eda_queue_finalize(eda_queue_t q);

#ifdef __BLOCKS__
// Wrappers to release the block / closure reference count after calling it
void _eda_call_async_block(eda_block_t block);
void _eda_call_sync_block(eda_block_t block);
#endif __BLOCKS__

#endif //EDA_QUEUE_H
