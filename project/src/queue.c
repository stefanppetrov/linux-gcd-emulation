//
// Created by spp on 12/04/18.
//

#include <Block.h>
#include "queue.h"
#include "scheduler.h"

const struct _eda_object_s EDA_QUEUE_VTABLE = {
    .refcnt = 1,
    .finalizer = _eda_queue_finalize
};

// Consumer functions for the various queue types
static EDA_INLINE void _eda_queue_serial_invoke(eda_queue_t);
static EDA_INLINE void _eda_queue_concurrent_invoke(eda_queue_t);
static EDA_INLINE void _eda_queue_cooperate_invoke(eda_queue_t);

// Next queue serial number
static atomic_uint_fast32_t EDA_QUEUE_NEXT_SERIAL = EDA_GLOBAL_QUEUE_CNT + 1;

// Initial queue heads for the global queues
static union _eda_task_u _DUMMY_TASK[EDA_GLOBAL_QUEUE_CNT] = {
    { 2, NULL, NULL, NULL, NULL, NULL },
    { 2, NULL, NULL, NULL, NULL, NULL },
    { 2, NULL, NULL, NULL, NULL, NULL },
    { 2, NULL, NULL, NULL, NULL, NULL },
    { 2, NULL, NULL, NULL, NULL, NULL },
};

static struct eda_queue_s _eda_global_queues[EDA_GLOBAL_QUEUE_CNT] = {
    [EDA_QUEUE_PRIO_BACKGROUND] = {
        .refcnt = INT_FAST64_MAX / 2,
        .finalizer = NULL,
        .runlock = 0,
        .flags = EDA_QUEUE_FLAG_DEFAULT,
        .priority = EDA_QUEUE_PRIO_BACKGROUND,
        .name = "eda.globals.background-priority",
        .head = &_DUMMY_TASK[EDA_QUEUE_PRIO_BACKGROUND],
        .tail = &_DUMMY_TASK[EDA_QUEUE_PRIO_BACKGROUND],
        .invoke = _eda_queue_concurrent_invoke,
        .serial = 1
    },
    [EDA_QUEUE_PRIO_LOW] = {
        .refcnt = INT_FAST64_MAX / 2,
        .finalizer = NULL,
        .runlock = 0,
        .flags = EDA_QUEUE_FLAG_DEFAULT,
        .priority = EDA_QUEUE_PRIO_LOW,
        .name = "eda.globals.low-priority",
        .head = &_DUMMY_TASK[EDA_QUEUE_PRIO_LOW],
        .tail = &_DUMMY_TASK[EDA_QUEUE_PRIO_LOW],
        .invoke = _eda_queue_concurrent_invoke,
        .serial = 2
    },
    [EDA_QUEUE_PRIO_DEFAULT] = {
        .refcnt = INT_FAST64_MAX / 2,
        .finalizer = NULL,
        .runlock = 0,
        .flags = EDA_QUEUE_FLAG_DEFAULT,
        .priority = EDA_QUEUE_PRIO_DEFAULT,
        .name = "eda.globals.default-priority",
        .head = &_DUMMY_TASK[EDA_QUEUE_PRIO_DEFAULT],
        .tail = &_DUMMY_TASK[EDA_QUEUE_PRIO_DEFAULT],
        .invoke = _eda_queue_concurrent_invoke,
        .serial = 3,
    },
    [EDA_QUEUE_PRIO_HIGH] = {
        .refcnt = INT_FAST64_MAX / 2,
        .finalizer = NULL,
        .runlock = 0,
        .flags = EDA_QUEUE_FLAG_DEFAULT,
        .priority = EDA_QUEUE_PRIO_HIGH,
        .name = "eda.globals.high-priority",
        .head = &_DUMMY_TASK[EDA_QUEUE_PRIO_HIGH],
        .tail = &_DUMMY_TASK[EDA_QUEUE_PRIO_HIGH],
        .invoke = _eda_queue_concurrent_invoke,
        .serial = 4
    }
};

#ifdef __BLOCKS__

void _eda_call_async_block(eda_block_t block){
    block();
    // Release heap copy of closure
    Block_release(block);
}

void _eda_call_sync_block(eda_block_t block){
    block();
}
#endif

void _eda_queue_push(eda_queue_t queue, _eda_task_t task) {
    // Enqueueing multiple items can be reused
    _eda_queue_push_list(queue, task, task);
}

size_t _eda_queue_push_list(eda_queue_t queue, _eda_task_t h, _eda_task_t t){
    eda_init();
    size_t res = 0;

    // Get the number of enqueued items to retain the right number of references
    _eda_task_t iter = h;
    while(iter) {
        iter->queue = queue;
        iter = iter->next;
        res++;
    }
    _eda_obj_retain_many(queue, res);

    // Try until we succeed setting tail to the first item in the enqueue list
    // Almost the same as the original Michael-Scott algorithm, only difference
    // is that we have to swing the tail pointer to the end of the enqueued list
    while(1){
        _eda_task_t tail = LOAD_RLXD(&queue->tail);
        _eda_task_t next = LOAD_RLXD(&tail->next);

        // Our tail is outdated
        if(tail != LOAD_RLXD(&queue->tail)) {
            continue;
        }

        // If others have enqueued, advance the tail pointer for them if they haven't
        if(next) {
            CAS_WEAK_EXPL(&queue->tail, &tail, next, memory_order_relaxed, memory_order_relaxed);
            continue;
        }

        // Try setting the tail's next pointer to the head of our list
        // If we fail someone has raced us and we need to try again
        if(CAS_WEAK_EXPL(&tail->next, &next, h, memory_order_relaxed, memory_order_relaxed)) {
            // Doesn't matter if this fails - other enqueueing and dequeing threads will advance this
            CAS_STR_EXPL(&queue->tail, &tail, t, memory_order_acquire, memory_order_acquire);
            return res;
        }
    }
}

_eda_task_t _eda_queue_pull_sc(eda_queue_t q) {
    _eda_task_t task, head;

    // No need for complicated looping logic here.
    // There is only 1 consumer and there is at least one fully completed enqueue
    // so the tail pointer will not need to be moved forward
    head = LOAD_RLXD(&q->head);
    task = LOAD_RLXD(&head->next);
  
    STORE_REL(&q->head, task);

    // Release the references
    _eda_obj_release(head);
    _eda_obj_release(q);

    return task;
}

_eda_task_t _eda_queue_pull_mc(eda_queue_t q) {
    _eda_task_t head;
    _eda_task_t tail;
    _eda_task_t next;

    // Unchanged Michael-Scott dequeue algorithm
    while(1)
    {
        head = LOAD_RLXD(&q->head);
        tail = LOAD_RLXD(&q->tail);
        next = LOAD_RLXD(&head->next);

        // Concurrent consumers racing us?
        if(head != LOAD_RLXD(&q->head)) {
            continue;
        }

        // Queue empty, or incomplete enqueues?
        if(head == tail) {
            if(UNLIKELY(next == NULL)) {
                return NULL;
            }

            // Advance tail to ensure we do not leave the queue in an inconsistent state
            CAS_WEAK_EXPL(&q->tail, &tail, next, memory_order_relaxed, memory_order_relaxed);
            continue;
        }

        // Try dequeuing the head unless someone beats us to it, in which case retry from scratch
        if(CAS_WEAK_EXPL(&q->head, &head, next, memory_order_relaxed, memory_order_relaxed)) {
            _eda_obj_release(head);
            _eda_obj_release(q);
            return next;
        }

    }
}

void _eda_queue_cooperate_invoke(eda_queue_t edaq) {

    _eda_task_t curr_task = NULL;
    // Get one task, execute it and release the reference to it
    curr_task = _eda_queue_pull_sc(edaq);

    _eda_task_invoke(curr_task);
    _eda_obj_release(curr_task);

    // Reschedule if needed, allowing other queues to be run in the interim
    if (SUB_ACQREL(&edaq->runlock, 1) > 1) {
        _eda_sched_schedule_queue(edaq);
    }
}

void _eda_queue_serial_invoke(eda_queue_t edaq) {
    _eda_task_t curr_task = NULL;

    // Keep running tasks from the queue while there are any,
    // consuming only as many as there are fully enqueued and runlocked ones
    do {
        curr_task = _eda_queue_pull_sc(edaq);

        _eda_task_invoke(curr_task);
        _eda_obj_release(curr_task);

    }while(SUB_REL(&edaq->runlock, 1) > 1);

    // At this point incomplete enqueues can reschedule the queue
    // Flush the task cache halfway, so as not to consume too much memory,
    // but to leave enough tasks for the next queue to reuse for producer tasks
    _eda_task_flush_cache(0);
}

void _eda_queue_concurrent_invoke(eda_queue_t edaq) {
    _eda_task_t curr_task = NULL;

    // Try running tasks from the queue as long as there are any fully enqueued and runlocked
    do {
        curr_task = _eda_queue_pull_mc(edaq);

        // We might have ran out of tasks due to parallel consumers
        if(!curr_task){
            break;
        }

        _eda_task_invoke(curr_task);
        _eda_obj_release(curr_task);

    }while(SUB_REL(&edaq->runlock, 1) > 1);

    // Flush the task cache halfway, so as not to consume too much memory,
    // but to leave enough tasks for the next queue to reuse for producer tasks
    _eda_task_flush_cache(0);
}

void _eda_queue_invoke(eda_queue_t edaq) {
    // Call the right consumer queue
    edaq->invoke(edaq);

    //Release the scheduler's reference to the queue
    _eda_obj_release(edaq);
}

void _eda_async_task(eda_queue_t q, _eda_task_t task) {
    uint32_t sequential = q->flags & EDA_QUEUE_FLAG_SERIAL;

    // Enqueue first, worry about consumer correctness later
    _eda_queue_push(q, task);

    // Is the queue sequential?
    if(sequential) {

        // No threads are consuming from the queue at the moment, schedule it and prevent others from doing so
        if (!ADD_ACQ(&q->runlock, 1)) {
            _eda_sched_schedule_queue(q);
        }
        return;

    }

    // Check if scheduling the parallel queue again is worthwhile.
    // Based on the number of tasks enqueued / consumers running and available processors
const int runlock = ADD_RLXD(&q->runlock, 1);
    const size_t procs_avail = _eda_sched_procs_avail();

    if(runlock+1 < _eda_sched_procs_avail() && q->head->next != NULL) {
        // For correctness ensure the enqueue occurs after the checks
        atomic_thread_fence(memory_order_acquire);
        _eda_sched_schedule_queue(q);
    }
}

void eda_async_f(eda_queue_t q, eda_func_t func, void* ctxt) {
    // Get a new task from cache / malloc
    _eda_task_t task = _eda_task_create(func, ctxt);
    if(UNLIKELY(task == NULL)) {
        abort();
    }

    // Ensure that the queue is not freed during the dispatch operation
    _eda_obj_retain(q);
    _eda_async_task(q, task);
    _eda_obj_release(q);
}

#ifdef __BLOCKS__
void eda_async(eda_queue_t q, eda_block_t block){
    // Make a heap copy of the closure
    eda_async_f(q, (eda_func_t)_eda_call_async_block, Block_copy(block));
}
#endif

// Needed for synchronous tasks executed on other threads
typedef struct _eda_sync_handle_s {
    eda_func_t func;
    void* ctxt;
    sem_t sem;
} * _eda_sync_handle_t;

static EDA_INLINE void _eda_sync_handle_invoke(_eda_sync_handle_t handle) {
    handle->func(handle->ctxt);
    // Wake up the blocked dispatching thread
    sem_post(&handle->sem);
}

static EDA_INLINE void _eda_sync_f_slow(eda_queue_t q, eda_func_t func, void *ctxt) {
    // In the cases we have to dispatch the synchronous tasks to another thread
    struct _eda_sync_handle_s handle = {
        .func = func,
        .ctxt = ctxt
    };
    sem_init(&handle.sem, 0, 0);

    // Same as regular async + the semaphore signaling wrapper
    _eda_task_t task = _eda_task_create((eda_func_t) _eda_sync_handle_invoke, &handle);
    _eda_async_task(q, task);

    // Wait on the task and destroy the semaphore
    sem_wait(&handle.sem);
    sem_destroy(&handle.sem);
}

void eda_sync_f(eda_queue_t q, eda_func_t func, void* ctxt) {
    // Make sure queue doesn't get released
    _eda_obj_retain(q);

    if(q->flags & EDA_QUEUE_FLAG_SERIAL) {
        // If it's a serial queue try running the task locally as a function.
        // This happens if no other thread is consuming from the queue (when there are tasks on it),
        // or no other thread is trying to run a task locally.
        uint runcnt = 0;
        if (CAS_STR_EXPL(&q->runlock, &runcnt, 1, memory_order_relaxed, memory_order_relaxed)) {
            func(ctxt);

            // We did execute the local fast path, but have to schedule the queue to another thread
            // if there have been tasks dispatched to it.
            if (SUB_ACQREL(&q->runlock, 1) > 1) {
                _eda_sched_schedule_queue(q);
            }
        } else {
            // Go to the slow case of async dispatch with a semaphore
            _eda_sync_f_slow(q, func, ctxt);
        }

        _eda_obj_release(q);
        return;
    }

    // Parallel queues - just execute the task locally always
    func(ctxt);
    _eda_obj_release(q);
}

#ifdef __BLOCKS__
void eda_sync(eda_queue_t q, eda_block_t block){
    eda_sync_f(q, (eda_func_t)_eda_call_sync_block, block);
}
#endif

eda_queue_t eda_queue_get_global(uint32_t priority, eda_flags_t flags) {
    (void)flags;

    if (priority >= EDA_GLOBAL_QUEUE_CNT)
        return NULL;

    return &_eda_global_queues[priority];
}

int _eda_queue_init(eda_queue_t q, const char* name, uint32_t priority, eda_flags_t flags) {
    _eda_obj_init(q, &EDA_QUEUE_VTABLE);

    // Runlock is 0 at creation always
    STORE_RLXD(&q->runlock, 0);
    q->priority = priority;
    q->flags = flags;
    q->serial = ADD_RLXD(&EDA_QUEUE_NEXT_SERIAL, 1);
    q->length = 0;
    strncpy(q->name, name, sizeof(q->name) / sizeof(char));

    // Pick the right consumer function
    if(flags & EDA_QUEUE_FLAG_SERIAL) {
        if (flags & EDA_QUEUE_FLAG_COOP) {
            q->invoke = (eda_func_t)_eda_queue_cooperate_invoke;
        }
        else {
            q->invoke = (eda_func_t)_eda_queue_serial_invoke;
        }
    }
    else {
        q->invoke = (eda_func_t)_eda_queue_concurrent_invoke;
    }

    q->head = q->tail = _eda_task_create_dummy();
}

eda_queue_t eda_queue_create(const char* name, uint32_t priority, eda_flags_t flags) {
    eda_queue_t q = calloc(1, sizeof(struct eda_queue_s));

    if(!q)
        return q;

    _eda_queue_init(q, name, priority, flags);

    return q;
}

void _eda_queue_finalize(eda_queue_t q) {
    // Head always points to a used task item which must be released
    _eda_obj_release(q->head);
    free(q);
}

uint32_t eda_queue_get_length(eda_queue_t q){
    return q->length;
}

const char* eda_queue_get_name(eda_queue_t q){
    return q->name;
}

