#ifndef EDA_GROUP_H
#define EDA_GROUP_H

#include "commons.h"
#include "task.h"

typedef struct eda_group_s {
    EDA_OBJ_HEADER(struct eda_group_s);
    atomic_uint_fast64_t state; // Waiters & running tasks
    _Atomic _eda_task_t  deferred_stack; // Deferred stack
    sem_t                sem; // For signaling waiters
} *eda_group_t;

void _eda_group_finalize(eda_group_t q);
#endif //EDA_GROUP_H
