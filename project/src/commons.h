#ifndef EDA_PRIVATE_H
#define EDA_PRIVATE_H

#include <eda.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <string.h>

#include "atomics.h"

#if !defined(DEBUG)
    #define EDA_INLINE inline __attribute__((always_inline))
#else
    #define EDA_INLINE
#endif

// We can provide more information to the branch predictor in some cases
#define LIKELY(x) __builtin_expect((x), (1L))
#define UNLIKELY(x) __builtin_expect((x), (0L))

// Get the clock based on the clock bit of a time value
#define GET_CLOCK(timex) ((timex) & 1 ? EDA_CLOCK_SYS : EDA_CLOCK_WALL)

// Convert raw nanoseconds to a system clock time
#define SYS_TIME(nanos) (((nanos) << 1) + 1)

// Convert raw nanoseconds to a wall clock time
#define WALL_TIME(nanos) (((nanos) << 1))

// Convert clock time value to raw nanoseconds
#define STRIPPED_TIME(timex) ((timex) >> 1)

//Define the reference counting object header
#define EDA_OBJ_HEADER(TYPE) \
    struct{ \
        atomic_int_fast64_t refcnt; \
        void (*finalizer)(TYPE *); \
    }

/**
 * A generic refcounted structure type to which other refcounted EDA structure types can be casted
 */
typedef struct _eda_object_s {
    EDA_OBJ_HEADER(struct _eda_object_s);
}* _eda_object_t;

/**
 * A refcounted wrapper around POSIX semaphores
 */
typedef struct eda_sem_s {
    EDA_OBJ_HEADER(struct eda_sem_s);
    sem_t sem;
}* eda_sem_t;

/**
 * Frees an EDA semaphore by destroying the internal POSIX semaphore an freeing memory.
 */
void _eda_sem_finalize(eda_sem_t sem);

/**
 * Internals of the one-time execution completion predicate.
 * Allows for building up a stack of waiters during execution which can be signaled via their semaphores.
 */
typedef struct eda_completion_s {
    struct eda_completion_s * next_waiter;
    sem_t sem;
} *_eda_completion_t;

/**
 * Initialize an EDA object with a reference count and finalizer based on a template / vtable object.
 * @param obj The initialized object.
 * @param vtable The template, provides an intial refcount and a finalizer.
 */
static EDA_INLINE void _eda_obj_init(_eda_object_t obj, const _eda_object_t vtable) {
    obj->refcnt = vtable->refcnt;
    obj->finalizer = vtable->finalizer;
}

/**
 * Retains a given number of references to an object.
 * @param obj The object to be retained. Must not have a reference count of 0 or below.
 * @param cnt The number of references to keep.
 */
static EDA_INLINE void _eda_obj_retain_many(_eda_object_t obj, size_t cnt) {
    uint64_t refcnt = ADD_ACQ(&obj->refcnt, cnt);
}

static EDA_INLINE void _eda_obj_retain(_eda_object_t obj) {
    _eda_obj_retain_many(obj, 1);

}

/**
 * Release a single reference to an object. Release resources if no references are left.
 * @param obj
 */
static EDA_INLINE void _eda_obj_release(_eda_object_t obj) {
    // We can't have preivous instructions being reordered after this point.
    // They might try using the object.
    int64_t refcnt = SUB_REL(&obj->refcnt, 1);

    // In most cases the reference count would have been more than 1.
    if(UNLIKELY(refcnt == 1)) {
        // Make sure the finalizer code is not reordered before decrementing the refcount.
        atomic_thread_fence(memory_order_acquire);
        if(obj->finalizer) {
            obj->finalizer(obj);
            return;
        }
        free(obj);
    }
}

// Actual initialization.
void _eda_init(void* unused);

// Define as extern so there is only 1 of these across all translation units
extern eda_completion_t _EDA_INITIALIZED;
/**
 * Inlined implicit one-time initialization, called by other EDA functions.
 */
static EDA_INLINE void eda_init() {
    eda_once_f(&_EDA_INITIALIZED, _eda_init, NULL);
}

#endif //EDA_PRIVATE_H
