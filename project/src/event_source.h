//
// Created by spp on 23/02/18.
//

#ifndef EDA_SOURCE_PRIVATE_H
#define EDA_SOURCE_PRIVATE_H

#include <sys/epoll.h>
#include <sys/queue.h>

#include "commons.h"

typedef void (*_coalesce_t)(atomic_intptr_t* old, intptr_t new);

// Internal watch types
typedef enum {
   EDA_WATCH_FD = 0, // Corresponds to EDA_READ_SOURCE and EDA_WRITE_SOURCE
   EDA_WATCH_SIG,
   EDA_WATCH_TIMER
} _eda_watch_type_t;

typedef struct _eda_watch_s * _eda_watch_t;

// Link between watches and sources
typedef struct _eda_watcher_s{
    TAILQ_ENTRY(_eda_watcher_s) watcher_list;
    eda_source_t                owner;
    _eda_watch_t                watch;
}* _eda_watcher_t;
// Watchers are stored in linked lists in the watch object
typedef TAILQ_HEAD(_eda_watcher_list_s,_eda_watcher_s) *_eda_watcher_list_t;

typedef struct _eda_watch_s{
    _eda_watch_type_t            type;
    uintptr_t                    ident; // Type-specific identifier, typically from the registered event sources
    int                          fd; // A matching file descriptor for the identifer
    struct epoll_event           event; // Used for registering with epoll
    TAILQ_ENTRY(_eda_watch_s)    watch_list; // Watches are in a list themselves
    struct _eda_watcher_list_s   read_watchers; // Event source watchers for read readiness events
    struct _eda_watcher_list_s   write_watchers; // Event source watchers for write readiness events
}* _eda_watch_t;

// The different handler types
#define EDA_HANDLER_REG   0
#define EDA_HANDLER_EV    1
#define EDA_HANDLER_CNCL  2

#define EDA_HANDLER_CNT 3

typedef struct _eda_handler_s{
    uint8_t       is_block; // Do we need to free the context or do a closure release?
    eda_funcev_t  func;
    void*         ctxt;
}* _eda_handler_t;

typedef struct eda_source_s {
    EDA_OBJ_HEADER(struct eda_source_s);
    eda_source_type_t       type;
    uintptr_t               ident; // Type specific identifier
    eda_flags_t             flags; // Type-specific flags
    void*                   config; // Type-specific configuration object
    atomic_uint_fast8_t     started, // Various event source states
                            installed,
                            registered,
                            cancelled,
                            uninstalled,
                            running;
    atomic_intptr_t         data; // Some type-specific representation of coalesced event data
    _coalesce_t             coalesce; // How event data is coalesced, type-specific, must appear as atomic
    struct _eda_watcher_s   watcher; // Watcher registered with a watch object
    struct _eda_handler_s   handlers[EDA_HANDLER_CNT]; //Storage for all 3 handlers
    eda_queue_t             inner_q; // Target queue for dispatching handlers
}* eda_source_t;

typedef struct _eda_timer_config_s {
    clockid_t clockid; // Clock used for this timer
    struct itimerspec timerspec; // Standard linux / unix timer configuration
}* _eda_timer_config_t;

// Initialize event management
int  _eda_manager_init();

// Watch type independent watcher notification mechanism
void _eda_notify_watchers(_eda_watcher_list_t head, intptr_t data);

// Event notification logic per watch type
void _eda_notify_watch_sig(_eda_watch_t w, uint32_t event_mask);
void _eda_notify_watch_timer(_eda_watch_t w, uint32_t event_mask);
void _eda_notify_watch_fd(_eda_watch_t w, uint32_t event_mask);

// Notify watches of events
int  _eda_manager_process_events(struct epoll_event *events, int cnt);

// Execute event source management requests
void _eda_manager_process_queue();
// Actual event loop
void _eda_manager_event_loop(eda_queue_t q);

// Adding / removing watch objects
_eda_watch_t _eda_watch_create(eda_source_t s);
void         _eda_watch_free(_eda_watch_t w);

// Event source management functions
void _eda_source_install(eda_source_t s);
void _eda_source_manage(eda_source_t src);
void _eda_source_free(eda_source_t s);


#endif //EDA_SOURCE_PRIVATE_H
