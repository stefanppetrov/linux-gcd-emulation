//
// Created by spp on 12/04/18.
//

#ifndef EDA_APPLY_H
#define EDA_APPLY_H

#include "commons.h"

typedef struct _eda_apply_s{
    size_t start; // Used for static scheduling, initial iteration for current subloop
    size_t end; // Used for static scheduling, final iteration for current subloop
    size_t stride; // Used for static scheduling, stride of the current subloop
    eda_func2_t body; // Loop body
    void* ctxt;
    void* nesting_ctl; // Make sure we don't parallelize nested loops
    sem_t* sem; // Signal thread which started the loop
    size_t _Atomic * iteration; // Used for dynamic scheduling, next pending iteration
} *_eda_apply_t;

#endif //EDA_APPLY_H
