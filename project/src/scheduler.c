//
// Created by spp on 04/02/18.
//
#include "scheduler.h"
#include "queue.h"

#include <pthread_workqueue.h>
#include <unistd.h>

#define WORKQ_CNT 4

int priorities[WORKQ_CNT] = {
        WORKQ_BG_PRIOQUEUE,
        WORKQ_LOW_PRIOQUEUE,
        WORKQ_DEFAULT_PRIOQUEUE,
        WORKQ_HIGH_PRIOQUEUE
};
pthread_workqueue_t workqueues[WORKQ_CNT];
pthread_workqueue_t overcommit[WORKQ_CNT];

size_t _EDA_PROCS_AVAIL = 0;

#ifdef _SC_NPROCESSORS_ONLN
static EDA_INLINE size_t _eda_sched_procs_avail_init() {
    // This should be run only once
    long rval = sysconf(_SC_NPROCESSORS_ONLN);
    return rval > 0 ? rval : 1;
}
#else
static EDA_INLINE size_t _eda_backend_procs_avail_init() {
    FILE* cpuinfo = fopen("/proc/cpuinfo", "r");
    size_t cpucnt = 0;

    const char text[] = "processor";
    char* lineptr = NULL;
    size_t sz;
    int delim = ':';
    while(getdelim(&lineptr, &sz, delim, cpuinfo) != -1){
        if(strstr(lineptr, text))
            cpucnt++;
    }

    return cpucnt;
}
#endif

static EDA_INLINE pthread_workqueue_t _eda_queue_get_workq(eda_queue_t edaq) {
    // Greedy (serial) queues get mapped to an overcommit work queue of the same priority,
    // so they get a dedicated thread when others are busy.

    // Cooperative (serial) and uncooperative (parallel) queues get mappet to a regular work queue,
    // matching their priority.
    uint32_t ocommit = (edaq->flags & EDA_QUEUE_FLAG_SERIAL) && !(edaq->flags & EDA_QUEUE_FLAG_COOP);
    return ocommit ? overcommit[edaq->priority] : workqueues[edaq->priority];
}

int _eda_sched_schedule_queue(eda_queue_t q){
    int result = 0;

    // Retain a reference to the queue so it doesn't get released before a thread starts consuming from it
    _eda_obj_retain(q);

    // Cooperative serial queues can be scheduled on the parallel EDA queues instead.
    // We don't need a costly work queue scheduling call for them due to their scheduling making them
    // similar to regular tasks (execute 1 task + reschedule if needed).
    if(UNLIKELY(q->flags & EDA_QUEUE_FLAG_COOP)) {
        eda_queue_t global = eda_queue_get_global(q->priority, EDA_QUEUE_FLAG_DEFAULT);
        eda_async_f(global, _eda_queue_invoke, q);
        return 0;
    }

    pthread_workitem_handle_t wih;
    unsigned int genc;

    // Find the right thread pool work queue and schedule the EDA queue on it
    pthread_workqueue_t workq = _eda_queue_get_workq(q);
    result = pthread_workqueue_additem_np(workq, (eda_func_t)_eda_queue_invoke, q, &wih, &genc);
    return result;
}

int _eda_sched_init(void) {

    int result = 0;

    uint8_t it = 0;

    // Initialize thread pool work queus
    pthread_workqueue_attr_t wq_attr;
    result = pthread_workqueue_attr_init_np(&wq_attr);
    if (result != 0)
        goto err;

    result = pthread_workqueue_init_np();
    if (result != 0)
        goto err;

    memset(workqueues, 0, sizeof(workqueues));

    for (; it < WORKQ_CNT; ++it) {
        result = pthread_workqueue_attr_setqueuepriority_np(&wq_attr, priorities[it]);
        if (result != 0)
            goto err;

        result = pthread_workqueue_attr_setovercommit_np(&wq_attr, 0);
        if (result != 0)
            goto err;

        result = pthread_workqueue_create_np(&workqueues[it], &wq_attr);
        if (result != 0)
            goto err;

        // Create overcommit queues (for sequential queues)

        result = pthread_workqueue_attr_setovercommit_np(&wq_attr, 1);
        if (result != 0)
            goto err;

        result = pthread_workqueue_create_np(&overcommit[it], &wq_attr);
        if (result != 0)
            goto err;
    }
    // Get the number of processors

    _EDA_PROCS_AVAIL = _eda_sched_procs_avail_init();
err:
    pthread_workqueue_attr_destroy_np(&wq_attr);
    return result;

}

