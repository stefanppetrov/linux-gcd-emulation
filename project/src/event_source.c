//
// Created by spp on 23/02/18.
//

#include <eda.h>
#include "event_source.h"
#include "scheduler.h"
#include "queue.h"

#include <Block.h>
#include <sys/fcntl.h>
#include <sys/syslog.h>
#include <sys/eventfd.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>

static pthread_t MANAGER_THREAD;

// Epoll file descriptor, and event file descriptor for the manager queue
static int EDA_EPFD, EDA_MANAGER_EVENTS_FD;

extern const struct _eda_obj_s EDA_QUEUE_VTABLE;

static const struct _eda_object_s EDA_SOURCE_VTABLE = {
    .refcnt = 1,
    .finalizer = _eda_source_free
};

// Specialized manager queue for installing / uninstalling event sources
struct eda_queue_s _eda_manager_queue = {
    .runlock = 1,
    .flags = EDA_QUEUE_FLAG_SERIAL,
    .priority = EDA_QUEUE_PRIO_HIGH,
    .head = NULL,
    .tail = NULL,
    .name = "eda.internal.manager",
    .invoke = _eda_manager_event_loop,
    .serial = 0
};

static EDA_INLINE void _eda_source_handle_event(eda_source_t src);
static EDA_INLINE void _eda_source_reschedule(eda_source_t src);

// Call the source's specific event coalescing function
static EDA_INLINE void _eda_source_coalesce(eda_source_t src, intptr_t data) {
    src->coalesce(&src->data, data);
}

// By default coalesce by simply adding old event data to new
static EDA_INLINE void _default_coalesce(atomic_intptr_t* old_data, intptr_t new_data) {
    ADD(old_data, new_data);
}

// Read coalesced event data, without consuming it
static EDA_INLINE intptr_t _eda_source_peek(eda_source_t src) {
    return LOAD(&src->data);
}

// Consume event data, setting it 0 and returning old value
static EDA_INLINE intptr_t _eda_source_consume(eda_source_t src) {
    return XCHG(&src->data, 0);
}

static EDA_INLINE void _eda_source_cancel_inner(eda_source_t src);
static EDA_INLINE void _eda_source_register(eda_source_t src);


// Oddly enough this is missing from sys/queue.h
// Allow removal of list items during traversal of a linked list
#define	TAILQ_FOREACH_SAFE(var, head, field, tvar)			\
	for ((var) = TAILQ_FIRST((head));				\
	    (var) && ((tvar) = TAILQ_NEXT((var), field), 1);		\
        (var) = (tvar))

//Define the hash table for event watches

//Total buckets, must be a power of 2
#define EDA_WATCH_BUCKET_CNT 0x100U
#define EDA_WATCH_HASH(x)    ((x) & (EDA_WATCH_BUCKET_CNT-1))

// Use linked list buckets
static TAILQ_HEAD(_eda_watch_bucket_s, _eda_watch_s) watch_buckets[EDA_WATCH_BUCKET_CNT];
typedef struct _eda_watch_bucket_s *_eda_watch_bucket_t;

// Map event source types to internal watch types
static EDA_INLINE _eda_watch_type_t _eda_get_watch_type(eda_source_t src) {
    switch(src->type) {
        case EDA_READ_SOURCE:
        case EDA_WRITE_SOURCE:
            return EDA_WATCH_FD;
        case EDA_TIMER_SOURCE:
            return EDA_WATCH_TIMER;
        case EDA_SIGNAL_SOURCE:
            return EDA_WATCH_SIG;
    }
}

// Find watch bucket for source based on its corresponding watch type and identifier
static EDA_INLINE _eda_watch_bucket_t _eda_find_watch_bucket( eda_source_t src ) {

    _eda_watch_type_t type = _eda_get_watch_type(src);
    return &watch_buckets[EDA_WATCH_HASH(type ^ src->ident)];
}

// Given a watch bucket iterate over the items to find the watcher of this source
// Return null if not found
static EDA_INLINE _eda_watch_t _eda_find_watch( _eda_watch_bucket_t bucket , eda_source_t src) {

    _eda_watch_type_t type = _eda_get_watch_type(src);

    _eda_watch_t watch;
    TAILQ_FOREACH(watch, bucket, watch_list) {
        if (watch->type == type && watch->ident == src->ident) {
            return watch;
        }
    }

    return NULL;
}

// Get epoll event flags based on event source type
static EDA_INLINE uint32_t _eda_get_epoll_flags(eda_source_t src) {
    uint32_t event_flags = EPOLLEXCLUSIVE;

    switch(src->type) {
        case EDA_READ_SOURCE:
            // Standard epoll is level triggered, which we don't want for file descriptors,
            // as otherwise it will flood us with events
            event_flags |= EPOLLIN | EPOLLET;
            break;

        case EDA_WRITE_SOURCE:
            // Standard epoll is level triggered, which we don't want for file descriptors,
            // as otherwise it will flood us with events
            event_flags |= EPOLLOUT | EPOLLET;

            break;
        default:
            event_flags |= EPOLLIN;
            break;
    }

    return event_flags;
}

// Signals need to be masked on all threads,
// so only signalfd's registered with the event loop can receive them
static EDA_INLINE void _eda_mask_signals(){
    sigset_t set;
    sigfillset(&set);
    // Leave SIGINT unblocked initially
    sigdelset(&set, SIGINT);
    pthread_sigmask(SIG_BLOCK, &set, NULL);
}

// Linux signal semantics when multi-threading are
// such that any thread can receive and handle signals targeted at the entire process.
// We need signals to be handled only on signalfd's monitored by epoll on the event manager thread.
// To ensure this block signals lazily on other threads when they handle them,
// and fire signal back at event loop thread.
static void _eda_catch_and_block_signals(int signo) {
    _eda_mask_signals();
    pthread_kill(MANAGER_THREAD, signo);
}

// Create a watch for the given event,
// converting event source event identifier to a meaningful file descriptor for epoll to use
_eda_watch_t _eda_watch_create(eda_source_t src) {
    // Maintain a set of EDA watched signals
    static sigset_t watched_signals;
    static struct sigaction sa = {
        .sa_handler = _eda_catch_and_block_signals,
        .sa_flags = SA_RESTART,
    };

    int watch_fd;
    _eda_watch_type_t watch_type = _eda_get_watch_type(src);
    sigset_t sigset;

    switch(watch_type) {

        case EDA_WATCH_FD:
            // No special conversion in this case
            watch_fd = (int)src->ident;
            break;

        case EDA_WATCH_SIG:
            // Create a signalfd to be used by all event sources who want to receive the desired signal

            sigaddset(&sigset, src->ident);
            watch_fd = signalfd(-1, &sigset, SFD_CLOEXEC | SFD_NONBLOCK);

            // Check if it is already in the watched signals
            if(!sigismember(&watched_signals, src->ident)) {

                // Lazily block and redirect signals to manager, as we don't know
                // which threads have masked them. Otherwise we risk threads being
                // interrupted due to Linux signal delivery semantics.
                sigaddset(&watched_signals, src->ident);
                sigaction(src->ident, &sa, NULL);
            }

            // Something went terribly wrong
            if(watch_fd < 0) {
                return NULL;
            }
            break;

        case EDA_WATCH_TIMER:
            if(!src->config) {
                return NULL;
            }

            // Create a new timerfd based on configuration given
            _eda_timer_config_t config = src->config;
            watch_fd = timerfd_create(config->clockid, TFD_CLOEXEC | TFD_NONBLOCK);
            if(watch_fd < 0){
                return NULL;
            }

            // If successful set the timerfd
            timerfd_settime(watch_fd, TFD_TIMER_ABSTIME, &config->timerspec, NULL);
            break;
    }

    // Initialize thte rest of the watch's fields, like epoll event flags
    _eda_watch_t watch = calloc(1, sizeof(struct _eda_watch_s));
    watch->type = _eda_get_watch_type(src);
    watch->ident = src->ident;
    watch->fd = watch_fd;
    TAILQ_INIT(&watch->write_watchers);
    TAILQ_INIT(&watch->read_watchers);
    watch->event.events = _eda_get_epoll_flags(src);
    watch->event.data.ptr = watch;

    return watch;

}

void _eda_watch_free(_eda_watch_t watch) {
    // Close the file descriptors if they are internally created ones like signalfds or timerfds
    if (watch->type != EDA_WATCH_FD && watch->fd != watch->ident) {
        close(watch->fd);
    }
    free(watch);
}


eda_source_t eda_source_create(eda_source_type_t type, uintptr_t handle, eda_flags_t flags, eda_queue_t q) {
    // Timers ID's are taken from a monotonic sequence
    static atomic_uintmax_t timer_ids = 0;

    eda_source_t result = calloc(1, sizeof(struct eda_source_s));
    _eda_obj_init(result, &EDA_SOURCE_VTABLE);

    result->type = type;
    result->ident = handle;
    result->flags = flags;
    result->watcher.owner = result;

    // Set the right coalesce function
    switch(type){
        case EDA_TIMER_SOURCE:
            result->ident = ADD_RLXD(&timer_ids, 1);
            result->coalesce = _default_coalesce;
            break;
        case EDA_SIGNAL_SOURCE:
            result->coalesce = _default_coalesce;
            break;
        default:
            result->coalesce = _default_coalesce;
            break;
    }

    // Use a global queue of default priority if no queue is given
    if(!q) {
        uint32_t prio = EDA_QUEUE_PRIO_DEFAULT;
        if (!(result->inner_q = eda_queue_get_global(EDA_QUEUE_PRIO_DEFAULT, 0))) {
            abort();
        }
    }
    else {
        _eda_obj_retain(q);
        result->inner_q = q;
    }

    return result;
}

void _eda_source_free(eda_source_t src) {
    size_t i = 0;

    // Free handlers depending on they use closures or plain functions
    for(i = 0; i < EDA_HANDLER_CNT; i++) {
        _eda_handler_t handler = &src->handlers[i];

        if(!handler->func){
           continue;
        }

        int freeable = 1;
#ifdef __BLOCKS__
        if(handler->is_block) {
            Block_release(handler->ctxt);
            freeable = 0;
        }
#endif

        if(freeable && handler->ctxt)
            free(handler->ctxt);
    }

    if(src->config) {
        free(src->config);
    }

    free(src);
}

void _eda_source_install(eda_source_t src) {
    int rval = 0;

    // Find watch for source or create a new one
    _eda_watch_bucket_t bucket = _eda_find_watch_bucket(src);
    _eda_watch_t        watch = _eda_find_watch(bucket, src);

    uint32_t epoll_flags = _eda_get_epoll_flags(src);

    if(watch) {
        // If a watch exists, check if the event flags need updating
        if(~watch->event.events & epoll_flags){

            uint32_t old_flags = watch->event.events;
            watch->event.events |= epoll_flags;

            // Update the epoll instance to use the new flags
            if( (rval = epoll_ctl(EDA_EPFD, EPOLL_CTL_MOD, watch->fd, &watch->event)) ) {
                watch->event.events = old_flags;
                abort();
                //return rval;
            }
        }
    }
    else {
        // Need to create a new watch
        watch = _eda_watch_create(src);

        if(!watch) {
            abort();
            //return -1;
        }

        // If valid, register with epoll
        if( (rval = epoll_ctl(EDA_EPFD, EPOLL_CTL_ADD, watch->fd, &watch->event)) ) {
            _eda_watch_free(watch);
            //return rval;
            abort();
        }

        // Insert into hash table
        TAILQ_INSERT_TAIL(bucket, watch, watch_list);
    }

    // Add the source to the watcher lists it needs to be in on the watch
    src->watcher.watch = watch;
    if(epoll_flags & EPOLLIN) {
        TAILQ_INSERT_TAIL(&watch->read_watchers, &src->watcher, watcher_list);
    }
    if(epoll_flags & EPOLLOUT) {
        TAILQ_INSERT_TAIL(&watch->write_watchers, &src->watcher, watcher_list);
    }

    XCHG_ACQREL(&src->installed, 1);

    // Dispatch registration handler if it is set
    if(src->handlers[EDA_HANDLER_REG].func) {
        STORE(&src->running, 1);
        eda_async_f(src->inner_q, (eda_func_t) _eda_source_register, src);
    }
}

void _eda_source_uninstall(eda_source_t src){

    // Make sure we uninstall only once
    _eda_watcher_t watcher = &src->watcher;
    _eda_watch_t watch = watcher->watch;
    uint32_t *epoll_flags = &watch->event.events;

    // Remove source  from relevant watcher list on watch
    _eda_watcher_list_t list =
        (src->type == EDA_WRITE_SOURCE) ? &watch->write_watchers : &watch->read_watchers;
    TAILQ_REMOVE(list, watcher, watcher_list);

    // Update epoll flags as appropriate
    if (TAILQ_EMPTY(&watch->write_watchers)) {
        *epoll_flags &= ~EPOLLOUT;
    }
    if (TAILQ_EMPTY(&watch->read_watchers)) {
        *epoll_flags &= ~EPOLLIN;
    }

    // Update epoll if reader / writer list went empty to disable corresponding events
    if (*epoll_flags & (EPOLLOUT | EPOLLIN)) {
        epoll_ctl(EDA_EPFD, EPOLL_CTL_MOD, watch->fd, &watch->event);
    } else {
        // Or stop watching the file descriptor entirely, and remove the watch
        // When there are no more watchers left
        epoll_ctl(EDA_EPFD, EPOLL_CTL_DEL, watch->fd, NULL);
        TAILQ_REMOVE(_eda_find_watch_bucket(src), watch, watch_list);
        _eda_watch_free(watch);
    }

    // Release retained references to target queue and source itself
    XCHG_ACQ(&src->uninstalled, 1);
    _eda_obj_release(src->inner_q);
    _eda_obj_release(src);
}

// Calling the register handler and rescheduling
static EDA_INLINE void _eda_source_register(eda_source_t src){
    _eda_handler_t handler = &src->handlers[EDA_HANDLER_REG];
    if (handler->func) {
        handler->func(handler->ctxt, 0);
    }

    STORE_REL(&src->registered, 1);

    // Check if source was cancelled in which case execute the cancel handler as well and return
    if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
        _eda_source_cancel_inner(src);
        return;
    }

    // Enable events to be delivered and event handler dispatched
    STORE(&src->running, 0);

    // Used for CAS
    uint32_t nil = 0;

    // If there are no coalesced events
    if(!_eda_source_peek(src)) {
        // If cancelled and no other handlers are running execute cancel handler
        if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
            if(CAS_STR(&src->running, &nil, 1)){
                _eda_source_cancel_inner(src);
            }
        }
        return;
    }

    // There was coalesced data try acquiring the running lock
    if(!CAS_STR(&src->running, &nil, 1)){
        return;
    }

    // Check for cancellation and execute cancel handler if needed
    if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
        _eda_source_cancel_inner(src);
        return;
    }

    // No cancellation, but there was event data and we acquired the running lock,
    // so dispatch the event handler and return
    _eda_obj_retain(src);
    eda_async_f(src->inner_q, (eda_func_t)_eda_source_handle_event, src);
}

static EDA_INLINE void _eda_manager_push(eda_func_t f, void* src) {

    // Apply same logic as serial queues but replace runlock with the eventfd
    _eda_task_t task = _eda_task_create(f, src);
    if(LIKELY(task != NULL)) {
        _eda_queue_push(&_eda_manager_queue, task);

        /*
         * Ensure that the eventfd is updated AFTER the enqueue,
         * otherwise race conditions in the event loop will occur and
         * some tasks might stay on the queue indefinitely
         */
        atomic_thread_fence(memory_order_acq_rel);
        eventfd_write(EDA_MANAGER_EVENTS_FD, 1);
    }
}

void eda_source_start(eda_source_t src) {
    if(LIKELY(src != NULL)){
        _eda_obj_retain(src);
        // Don't start more than once
        if( !XCHG_ACQ(&src->started, 1) ) {
            _eda_manager_push((eda_func_t)_eda_source_install, src);
        }
    }
}

// Call cancel handler if set and dispatch an uninstall request to event manager
static EDA_INLINE void _eda_source_cancel_inner(eda_source_t src) {
    _eda_handler_t handler = &src->handlers[EDA_HANDLER_CNCL];
    if (handler->func) {
        handler->func(handler->ctxt, 0);
    }
    _eda_manager_push((eda_func_t) _eda_source_uninstall, src);
    // No other handler will hold this reference
    _eda_obj_release(src);
}

void eda_source_cancel(eda_source_t src) {

    if(LIKELY(src != NULL)){

        _eda_obj_retain(src);

        // Can't cancel a source that is not installed
        if( UNLIKELY( !LOAD_ACQ(&src->installed) ) ) {
            _eda_obj_release(src);
            return;
        }

        // Don't cancel more than once
        uint8_t expected = 0;
        if (!CAS_STR_EXPL(&src->cancelled, &expected, 1, memory_order_acquire, memory_order_relaxed)) {
            _eda_obj_release(src);
            return;
        }

        // From this point onwards we know that the reference retained in the beginning will be released by the
        // manager even if the race for the running lock is lost.
        uint32_t nil = 0;
        if(!CAS_STR_EXPL(&src->running, &nil, 1, memory_order_acquire, memory_order_relaxed)) {
            return;
        }

        // Dispatch cancel handler
        eda_async_f(src->inner_q, (eda_func_t) _eda_source_cancel_inner, src);
    }
}

static EDA_INLINE void _eda_source_handle_event(eda_source_t src) {
    _eda_handler_t handler = &src->handlers[EDA_HANDLER_EV];

    // If cancelled run cancel handler instead and return
    if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
        _eda_source_cancel_inner(src);
        return;
    }

    // Consume data and unless there was a race in the previous handler invocation causing events to be empty,
    // call the event handler with the consumed data
    intptr_t data = _eda_source_consume(src);
    if(LIKELY(data)){
      handler->func(handler->ctxt, data);
    }

    // Release running lock so the event manager thread can dispatch the next handler
    STORE(&src->running, 0);

    // If there is no data coalesced again yet this invocation can quit
    if(!_eda_source_peek(src)) {
        // Just ensure that the cancel handler is called if source was canceled
        if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
            if(!XCHG_ACQREL(&src->running, 1)) {
                _eda_source_cancel_inner(src);
                return;
            }
        }
        _eda_obj_release(src);
        return;
    }

    // There was new data, try grabbing the running lock again and return on failing
    uint_fast8_t nil = 0;
    if(!CAS_STR(&src->running, &nil, 1)){
        _eda_obj_release(src);
        return;
    }

    // Need to check cancellation yet again
    if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
        _eda_source_cancel_inner(src);
        return;
    }

    // Successfully dispatch the next invocation of the ecent handler
    eda_async_f(src->inner_q, (eda_func_t)_eda_source_handle_event, src);
}

void _eda_notify_watchers(_eda_watcher_list_t watchers, intptr_t data) {

    _eda_watcher_t watcher, tmp;

    // Error condition on file descriptor, cancel all event sources for the watch immediately
    if(UNLIKELY(data < 0)) {

        TAILQ_FOREACH_SAFE(watcher, watchers, watcher_list, tmp) {
            eda_source_t src = watcher->owner;
            eda_source_cancel(src);
        }
    }

    // Normal data coming in
    TAILQ_FOREACH_SAFE(watcher, watchers, watcher_list, tmp) {
        eda_source_t src = watcher->owner;
        // Retain a reference for the dispatched event handler
        _eda_obj_retain(src);

        // Cancelled sources do not receive event notifications!
        if(UNLIKELY(LOAD_ACQ(&src->cancelled))) {
            //_eda_source_uninstall(src);
            _eda_obj_release(src);
            continue;
        }

        // Coalesce new event data with old event data and check.
        _eda_source_coalesce(src, data);

        // If already consumed release the source reference and return
        if(!_eda_source_peek(src)){
            _eda_obj_release(src);
            continue;
        }

        // Try grabbing the running lock, return on failure
        uint_fast8_t nil = 0;
        if(!CAS_STR(&src->running, &nil, 1)) {
            _eda_obj_release(src);
            continue;
        }

        // Dispatch the event handler, if the source was cancelled at this point the handler will take care of that
        eda_async_f(src->inner_q, (eda_func_t) _eda_source_handle_event, src);
    }
}

void _eda_notify_watch_sig(_eda_watch_t watch, uint32_t event_mask) {
    (void)event_mask;

    struct signalfd_siginfo siginfo;

    // Try reading the signalfd and if it couldn't fill the siginfo field something wrong is happening
    if(UNLIKELY(sizeof(siginfo) != read(watch->fd, &siginfo, sizeof(siginfo)))) {
        syslog(LOG_ALERT | LOG_USER, "[ERROR] Losing signals due to some thread not masking signals!");
        return;
    }
    else {
        // Notify watchers of 1 signal fire
        _eda_notify_watchers(&watch->read_watchers, 1);
    }
}

void _eda_notify_watch_fd(_eda_watch_t watch, uint32_t event_mask) {

    // Cancel sources on any error condition
    if(event_mask & (EPOLLRDHUP | EPOLLPRI | EPOLLERR | EPOLLHUP)) {
        // Some error / exception has ocurred
        _eda_notify_watchers(&watch->read_watchers, -1);
        _eda_notify_watchers(&watch->write_watchers, -1);
        return;
    }

    // Notify readers
    if(event_mask & EPOLLIN) {
        // Even if Linux supports getting remaining buffer size for all file descriptors
        // it is racy to read it and pass it onto the handler here since
        // another invocation might be reading the FD right now.
        _eda_notify_watchers(&watch->read_watchers, 1);
    }

    // Notify writers
    if(event_mask & EPOLLOUT) {
        _eda_notify_watchers(&watch->write_watchers, 1);
    }
}

void _eda_notify_watch_timer(_eda_watch_t watch, uint32_t event_mask) {
    (void)watch, (void)event_mask;

    uint64_t times_fired;

    // Read how many expirations the timer has had and use that as event data to be coalesced.
    // No timer expirations will be lost as epoll is level triggered for this timerfd
    read(watch->fd, &times_fired, sizeof(times_fired));
    _eda_notify_watchers(&watch->read_watchers, times_fired);
}

int _eda_manager_process_events(struct epoll_event *events, int cnt) {
    int i, rval = 0;

    for(i = 0; i < cnt; i++) {

        // The manager queue has pending requests, handle them after notifying watches
        if(events[i].data.fd == EDA_MANAGER_EVENTS_FD) {
            rval = 1;
        }
        else {

            _eda_watch_t watch = (_eda_watch_t)events[i].data.ptr;
            // Use the right notification function for each watch
            switch(watch->type) {

                case EDA_WATCH_SIG:
                    _eda_notify_watch_sig(watch, events[i].events);
                    break;

                case EDA_WATCH_TIMER:
                    _eda_notify_watch_timer(watch, events[i].events);
                    break;

                case EDA_WATCH_FD:
                    _eda_notify_watch_fd(watch, events[i].events);
                    break;

                default:
                    syslog(LOG_ALERT | LOG_USER, "[ERROR] Garbage watch at: %p", watch);
            }
        }
    }
    return rval;
}

void _eda_manager_process_queue() {
    _eda_task_t next = NULL;

    // Consume notification for sources being registered/canceled/etc...
    uint64_t pending; // Eventfd buffer size for reads must be at least 8 bytes
    eventfd_read(EDA_MANAGER_EVENTS_FD, &pending);

    uint64_t iter = 0;
    // Consuming only that many requests is fine.
    // If there were any others we can read them on the next event loop iteration.
    for(iter=0; iter < pending; iter++) {
        next = _eda_queue_pull_sc(&_eda_manager_queue);
        _eda_task_invoke(next);
        _eda_obj_release(next);
    }

}

void _eda_manager_event_loop(eda_queue_t q) {
    (void)q;
    struct epoll_event events[64];

    MANAGER_THREAD = pthread_self();

    while(1) {
        // Block on epoll with no timeout
        int rval, ev_cnt= epoll_wait(EDA_EPFD, events, sizeof(events) / sizeof(events[0]), -1);

        switch(ev_cnt) {
            case -1:
                if(errno == EBADF){
                    syslog(LOG_ERR | LOG_USER, "[FATAL] EDA manager EPOLL set closed!");
                    abort();
                }
            case 0:
                // Shouldn't really happen!
                continue;
            default:
                // Notify watches of events and check whether we have items on the manager queue
                rval = _eda_manager_process_events(events, ev_cnt);
        }

        // Make sure we process the update items on the queue after dispatching events!
        if(rval == 1) {
            _eda_manager_process_queue();
        }
    }
}

int _eda_manager_init() {
    int res = 0;

    static union _eda_task_u DUMMY_CONT = {
        .refcnt = INT_FAST64_MAX,
        .finalizer = NULL,
        .func = NULL,
        .queue = NULL,
        .group = NULL
    };
    _eda_obj_init(&_eda_manager_queue, &EDA_QUEUE_VTABLE);
    _eda_manager_queue.head = &DUMMY_CONT;
    _eda_manager_queue.tail = &DUMMY_CONT;


    // In order for signal handlers to work correctly all signals must be masked
    // on all threads in the process. Otherwise the Linux semantics of a random
    // thread receiving the signals kick in causing lost signals and interrupts.
    // For process wide signals at least.
    _eda_mask_signals();

    EDA_EPFD = epoll_create1(O_CLOEXEC);

    if(EDA_EPFD < 0) {
        return -1;
    }

    // Evetnfd has to be non-blocking
    EDA_MANAGER_EVENTS_FD = eventfd(0U, O_NONBLOCK | O_CLOEXEC);

    if(EDA_MANAGER_EVENTS_FD < 0) {
        return -1;
    }

    struct epoll_event ev = {
        .events = EPOLLIN,
        .data.fd = EDA_MANAGER_EVENTS_FD
    };

    // Integrate the eventfd for the manager queue with the epoll event loop
    res = epoll_ctl(EDA_EPFD, EPOLL_CTL_ADD, EDA_MANAGER_EVENTS_FD, &ev);
    if(res) {
        return res;
    }

    // Somewhat weird way of getting a dedicated thread for the event loop
    res = _eda_sched_schedule_queue(&_eda_manager_queue);
    if(res) {
        epoll_ctl(EDA_EPFD, EPOLL_CTL_DEL, EDA_MANAGER_EVENTS_FD, NULL);
        return res;
    }

    // Initialize hash table
    for(size_t i = 0; i < EDA_WATCH_BUCKET_CNT; i++) {
        TAILQ_INIT(watch_buckets + i);
    }

    return 0;
}

static EDA_INLINE int _eda_source_set_handler(eda_source_t src, int handler_type, _eda_handler_t handler) {
    if(UNLIKELY(src == NULL)){
        errno = EINVAL;
        return -1;
    }

    // Prevent handlers from being set after startin the source
    if(LOAD_ACQ(&src->started)) {
        errno = EINPROGRESS;
        return -1;
    }

    // Set it to the right location in the array
    _eda_handler_t target = &src->handlers[handler_type];

    // Free the old ones
    int freeable = 1;
#ifdef __BLOCKS__
    if (target->is_block) {
        Block_release(target->ctxt);
        freeable = 0;
    }
#endif

    if(freeable && target->ctxt){
        free(target->ctxt);
    }

    memcpy(target, handler, sizeof(*target));
    return 0;
}

// Setters for each handler...

int eda_source_set_event_handler_f(eda_source_t src, eda_funcev_t handler, void *ctxt) {
    struct _eda_handler_s handler_obj = {
        .is_block = 0,
        .func = handler,
        .ctxt = ctxt
    };
    _eda_obj_retain(src);
    int result = _eda_source_set_handler(src, EDA_HANDLER_EV, &handler_obj);
    _eda_obj_release(src);
    return result;
}

int eda_source_set_register_handler_f(eda_source_t src, eda_funcev_t handler, void *ctxt) {

    struct _eda_handler_s handler_obj = {
        .is_block = 0,
        .func = handler,
        .ctxt = ctxt
    };

    _eda_obj_retain(src);
    int result =_eda_source_set_handler(src, EDA_HANDLER_REG, &handler_obj);
    _eda_obj_release(src);
    return result;
}

int eda_source_set_cancel_handler_f(eda_source_t src, eda_funcev_t handler, void *ctxt) {

    struct _eda_handler_s handler_obj = {
        .is_block = 0,
        .func = handler,
        .ctxt = ctxt
    };

    _eda_obj_retain(src);
    int result =_eda_source_set_handler(src, EDA_HANDLER_CNCL, &handler_obj);
    _eda_obj_release(src);
    return result;
}

int eda_source_set_timer(eda_source_t src, eda_time_t start, uint64_t interval) {

    if(LOAD_ACQ(&src->started)) {
        errno = EINPROGRESS;
        return -1;
    }

    // Make sure the wrong event source type doesn't get this
    if(src->type != EDA_TIMER_SOURCE){
        errno = EINVAL;
        return -1;
    }

    // Prevent garbage from getting in the system
    if(start == EDA_TIME_NEVER) {
        errno = EINVAL;
        return -1;
    }

    // Retain the source to prevent it from being freed
    _eda_obj_retain(src);

    // Get the clock and the nanoseconds of the timevalue
    uint64_t real_start = STRIPPED_TIME(start);
    eda_clock_t clock = GET_CLOCK(start);

    _eda_timer_config_t config = malloc(sizeof(struct _eda_timer_config_s));
    config->clockid = clock;

    // Convert to native linux time value structures
    struct timespec* initial_spec = &config->timerspec.it_value;
    struct timespec* interval_spec = &config->timerspec.it_interval;

    // Get current time if needed
    if(start == EDA_TIME_NOW) {
        clock_gettime(clock, initial_spec);
    }
    else {
        initial_spec->tv_sec = real_start / NSEC_PER_SEC;
        initial_spec->tv_nsec = real_start % NSEC_PER_SEC;
    }
    interval_spec->tv_sec = interval / NSEC_PER_SEC;
    interval_spec->tv_nsec = interval % NSEC_PER_SEC;

    src->config = config;
    _eda_obj_release(src);

    return 0;
}

#ifdef __BLOCKS__

void _eda_call_event_block(eda_blockev_t handler, uintptr_t data) {
    handler(data);
}

// Handler setters for block / closure handlers

int eda_source_set_event_handler(eda_source_t src, eda_blockev_t handler) {

    struct _eda_handler_s handler_obj = {
        .is_block = 1,
        .func = (eda_funcev_t)_eda_call_event_block,
        .ctxt = Block_copy(handler)
    };
    return _eda_source_set_handler(src, EDA_HANDLER_EV, &handler_obj);
}

int eda_source_set_register_handler(eda_source_t src, eda_blockev_t handler) {

    struct _eda_handler_s handler_obj = {
        .is_block = 1,
        .func = (eda_funcev_t)_eda_call_event_block,
        .ctxt = Block_copy(handler)
    };

    return _eda_source_set_handler(src, EDA_HANDLER_REG, &handler_obj);
}

int eda_source_set_cancel_handler(eda_source_t src, eda_blockev_t handler) {

    struct _eda_handler_s handler_obj = {
        .is_block = 1,
        .func = (eda_funcev_t)_eda_call_event_block,
        .ctxt = Block_copy(handler)
    };

    return _eda_source_set_handler(src, EDA_HANDLER_CNCL, &handler_obj);
}

#endif
