//
// Created by spp on 12/04/18.
//
#include "task.h"
#include "group.h"

// Ensure we don't overuse memory in producer/consumer cases
#define MAX_CACHE_SIZE 256

// Only for cleanup
pthread_key_t _eda_task_cache_key;

// Task cache is a thread-local linked stack of recycled task structures
_Thread_local _eda_task_t _eda_task_cache = NULL;
_Thread_local intptr_t _eda_task_cache_size = 0;

void _eda_task_init_cache() {
    // Ensure threads clean-up task caches after terminating
    pthread_key_create(&_eda_task_cache_key, _eda_task_flush_cache);
    pthread_setspecific(_eda_task_cache_key, 1);
}

void _eda_task_flush_cache(intptr_t full) {
    // Do either a complete flush of the cache or just discard half of the stack
    intptr_t count = _eda_task_cache_size / 2;
    if(full){
        count = 0;
    }

    _eda_task_t next;
    while(_eda_task_cache_size > count) {
        next = _eda_task_cache->next;
        free(_eda_task_cache);
        _eda_task_cache = next;
        _eda_task_cache_size--;
    }
}

void _eda_task_free(_eda_task_t cont) {
    // If the task was a part of a group release the reference to it
    if(LIKELY(cont->group != NULL)) {
        _eda_obj_release(cont->group);
    }
    // The reference to the queue is weak so we don't release it

    // Overflowing cache free half of it before pushing this task
    if(_eda_task_cache_size >= MAX_CACHE_SIZE) {
        _eda_task_flush_cache(0);
    }

    STORE_RLXD(&cont->next, _eda_task_cache);
    _eda_task_cache = cont;
    _eda_task_cache_size++;
}

static EDA_INLINE
_eda_task_t _eda_task_alloc(){

    // No items in cache, do an allocation
    if(_eda_task_cache_size < 1){
        return calloc(1, sizeof(union _eda_task_u));
    }

    // Pop topmost cached task and set it to all zeroes.
    _eda_task_t res = _eda_task_cache;
    _eda_task_cache = res->next;
    memset(res, 0, sizeof(*res));
    _eda_task_cache_size--;
    return res;
}

_eda_task_t _eda_task_create(eda_func_t func, void *ctxt) {

    // Hold 2 references initially.
    // 1 for the queue itself, until this task is no longer at the head
    // 1 for the executing thread, as it might still be running the task even if it is no longer on the queue
    static struct _eda_object_s _EDA_CONT_VTABLE = {
        .refcnt = 2,
        .finalizer = _eda_task_free
    };

    // Get a task either from the cache or malloc and initialize it
    _eda_task_t res = _eda_task_alloc();
    _eda_obj_init(res, &_EDA_CONT_VTABLE);

    res->func = func;
    res->ctxt  = ctxt;

    return res;
}

_eda_task_t _eda_task_create_dummy() {
    // Same as the normal task creation, but for initial items in queues.
    // These are never executed hence why the single reference
    static struct _eda_object_s _EDA_DUMMY_VTABLE = {
        .refcnt = 1,
        .finalizer = NULL
    };

    _eda_task_t res = _eda_task_alloc();
    _eda_obj_init(res, &_EDA_DUMMY_VTABLE);

    return res;
}

void _eda_task_invoke(_eda_task_t task) {
    if(LIKELY(task != NULL)) {
        task->func(task->ctxt);
        // Make sure we leave the group after running
        if(task->group){
            eda_group_leave(task->group);
        }
    }
}
