//
// Created by spp on 12/04/18.

#include "apply.h"
#include "task.h"
#include "queue.h"
#include "scheduler.h"

// Tells us if the current thread is already running a (parallelized) apply loop
_Thread_local void* _EDA_IN_APPLY_LOOP = NULL;

// The function which iterates through the loop and is actually run in parallel
static EDA_INLINE void _eda_apply_sequential(_eda_apply_t loop){
    // Setup
    size_t start = loop->start;
    size_t end = loop->end;
    size_t stride = loop->stride;
    eda_func2_t body = loop->body;
    void* ctxt = loop->ctxt;

    // Make sure no future loop parallelizations will take place on this thread.
    if(!loop->nesting_ctl) {
        _EDA_IN_APPLY_LOOP = loop;
    }

    // Are we running on a static schedule?
    if(UNLIKELY(!loop->iteration)) {
        // If yes, iterate over the loop range we're given with the specified stride
        size_t i = start;
        for (; i < end; i += stride) {
            body(ctxt, i);
        }
    }
    else {
        // Otherwise try incrementing the pending iteration counter atomically and executing
        // the iteration at the pre-increment value until we reach the loop end.
        size_t i = 0;
        while((i = ADD_RLXD(loop->iteration, 1)) < end) {
            body(ctxt, i);
        }
    }

    // Allow parallel loops again
    if(!loop->nesting_ctl) {
        _EDA_IN_APPLY_LOOP = NULL;
    }

    // If actually running in parallel signal back to progenitor thread
    if(loop->sem) {
        sem_post(loop->sem);
    }
}

void eda_apply_sched_f(eda_queue_t q, size_t iterations, eda_apply_sched_t sched, eda_func2_t func, void *ctxt) {
    _eda_obj_retain(q);

    // Need this to be done, otherwise _EDA_PROCS_AVAILABLE is some garbage value
    eda_init();

    const size_t proc_avail = _eda_sched_procs_avail();
    const size_t max_parallel = ((proc_avail < iterations) ? proc_avail : iterations);
    void* in_loop =  _EDA_IN_APPLY_LOOP;

    if( UNLIKELY(!iterations) ){
        return;
    }

    // Make sure we don't try parallelizing a loop too small, or split it more than we have processors,
    // or if already in an apply loop, or if target queue is serial
    if(in_loop != NULL || (q->flags & EDA_QUEUE_FLAG_SERIAL) || max_parallel < 2) {
        // Run it like a regular for loop over a range
        struct _eda_apply_s loop = {
            .body = func,
            .ctxt = ctxt,
            .stride = 1,
            .start = 0,
            .end = iterations,
            .nesting_ctl = in_loop,
            .iteration = NULL,
            .sem = NULL
        };
        eda_sync_f(q, (eda_func_t)_eda_apply_sequential, &loop);
        _eda_obj_release(q);
        return;
    }

    _eda_task_t tasks[max_parallel];
    struct _eda_apply_s subloops[max_parallel];

    sem_t sem;
    sem_init(&sem, 0, 0);

    _Atomic size_t iteration = ATOMIC_VAR_INIT(0);

    // Prepare parallel tasks.
    // If using static scheduling distribute iterations in a cyclic manner.
    // If using dynamic scheduling use the pending iteration atomic variable for distributing iterations.
    for(size_t i = 0; i < max_parallel; i++) {
        subloops[i].body = func;
        subloops[i].start = i;
        subloops[i].end = iterations;
        subloops[i].stride = max_parallel;
        subloops[i].ctxt = ctxt;
        subloops[i].nesting_ctl = NULL;
        subloops[i].sem = &sem;
        if(sched == EDA_SCHED_DYNAMIC)
            subloops[i].iteration = &iteration;
        else
            subloops[i].iteration = NULL;

        // Build a list for just 1 enqueue operation
        if( LIKELY(i > 0) ) {
            tasks[i] = _eda_task_create(_eda_apply_sequential, &subloops[i]);
            if( LIKELY(i > 1) )
                STORE_RLXD(&tasks[i - 1]->next, tasks[i]);
        }
    }

    // Push only the last P-1 subloop tasks
    size_t parallel = _eda_queue_push_list(q, tasks[1], tasks[max_parallel-1]);
    ADD_REL(&q->runlock, parallel);
    // Schedule them all since we don't know their individual length.
    for(size_t i=0; i<parallel; i++)
        _eda_sched_schedule_queue(q);

    // Run the 0-th task locally.
    _eda_apply_sequential(&subloops[0]);

    // Wait on the parallel subloops to finish
    do {
        sem_wait(&sem);
    } while(--parallel + 1);


    sem_destroy(&sem);
    _eda_obj_release(q);
}

void eda_apply_f(eda_queue_t q, size_t iterations, eda_func2_t func, void *ctxt) {
    eda_apply_sched_f(q, iterations, EDA_SCHED_STATIC, func, ctxt);
}

#ifdef __BLOCKS__

static EDA_INLINE void _eda_call_iter_block(eda_block2_t block, uint32_t iter){
    block(iter);
}

void eda_apply_sched(eda_queue_t q, size_t repetitions, eda_apply_sched_t sched, eda_block2_t block) {
    eda_apply_sched_f(q, repetitions, sched, (eda_func2_t) _eda_call_iter_block, block);
}

void eda_apply(eda_queue_t q, size_t repetitions, eda_block2_t block) {
    eda_apply_f(q, repetitions, (eda_func2_t) _eda_call_iter_block, block);
}

#endif
