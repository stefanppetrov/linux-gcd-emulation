#include "commons.h"
#include "queue.h"
#include "event_source.h"
#include "scheduler.h"

#include <errno.h>

eda_completion_t _EDA_INITIALIZED = 0;

void _eda_init(void* unused){
    (void)unused;
    // Initialize scheduling and thread pool
    int res = _eda_sched_init();

    if (res != 0){
        abort();
    }

    // Initialize the event loop
    res = _eda_manager_init();

    if(res) {
        abort();
    }

    // Initialize the task cache
    _eda_task_init_cache();

    // Be careful of CPU instruction reordering
    atomic_thread_fence(memory_order_seq_cst);
}

// Inlined one-time execution internal implementation
static EDA_INLINE void eda_once_inner_f(eda_completion_t* completion, eda_func_t func, void* ctxt) {
    _eda_completion_t DONE = ((uintptr_t) ~0),
                      old = NULL;

    struct eda_completion_s local;

    if (CAS_STR_EXPL(completion, &old, &local, memory_order_acquire, memory_order_acquire)) {
        // No one has executed this function yet
        func(ctxt);

        // We need at least an acquire-release (full-barrier) here in order to ensure that
        // no out-of-order execution, branch speculation, weak memory ordering causes
        // competing threads to see the completion flag set to DONE before the above call
        // has actually completed.
        old = XCHG_ACQREL(completion, DONE);

        // If there is a contention over the completion condition the threads which lost the
        // race for the condition, will attempt to build a stack of semaphores which this
        // thread must signal on. Only this thread can pop items off the stack and the end of
        // the stack is the pointer to its own semaphore
        while (old != &local) {
            // Someone has is pushing item on the stack, spin while they restore the stack

            // If we observe a value on the stack we know that its next value is correct
            // This is true due to the push CAS having acq-rel ordering
            // and the next pointer being set before the CAS
            _eda_completion_t tmp = old;
            old = old->next_waiter;

            sem_post(&tmp->sem);
        }
        return;
    }

    sem_init(&local.sem, 0, 0);

    while (DONE != old) {
        local.next_waiter = old;
        // Try pushing our semaphore on the stack of waiters
        if (CAS_WEAK_EXPL(completion, &old, &local, memory_order_acq_rel, memory_order_acquire)) {
            // If successful restore stack invariant and wait
            sem_wait(&local.sem);
        }
    }
    sem_destroy(&local.sem);
}

void eda_once_f(eda_completion_t* completion, eda_func_t func, void* ctxt) {
    const uintptr_t DONE = ((uintptr_t) ~0);
    // Do a simple check here to avoid multiple atomic operations in the inner call.
    if(UNLIKELY(DONE != LOAD_RLXD(completion)) ) {
        eda_once_inner_f(completion, func, ctxt);
    }
}

#ifdef __BLOCKS__

void eda_once(eda_completion_t* complete, eda_block_t block){
    eda_once_f(complete, (eda_func_t)_eda_call_sync_block, block);
}
#endif

// Semaphores start with 1 reference and must destroy their internal semaphore
static const struct _eda_object_s EDA_SEM_VTABLE = {
    .refcnt = 1,
    .finalizer = _eda_sem_finalize
};

void eda_sem_post(eda_sem_t sem) {
    _eda_obj_retain(sem);
    sem_post(&sem->sem);
    _eda_obj_release(sem);
}

int eda_sem_wait(eda_sem_t sem, eda_time_t timeout) {
    // Make sure the semaphore remains valid while we wait
    _eda_obj_retain(sem);
    struct timespec ts;
    int err=0;

    switch(timeout) {
        case EDA_TIME_NOW:
            err = sem_trywait(&sem->sem);
            _eda_obj_release(sem);
            return err;

        case EDA_TIME_NEVER:
            // Do not let interrupts get seen by the user
            do{
                err = sem_wait(&sem->sem);
            } while(err && errno == EINTR);
            _eda_obj_release(sem);
            return err;

        default:
            ts.tv_sec = STRIPPED_TIME(timeout) / NSEC_PER_SEC;
            ts.tv_nsec = STRIPPED_TIME(timeout) % NSEC_PER_SEC;

            // Do not let interrupts get seen by the user
            while((err = sem_timedwait(&sem->sem, &ts)) && errno == EINTR);
            _eda_obj_release(sem);
            return err;
    }
}

eda_sem_t eda_sem_create(unsigned int val) {
    eda_sem_t sem = malloc(sizeof(struct eda_sem_s));
    _eda_obj_init(sem, &EDA_SEM_VTABLE);
    sem_init(&sem->sem, 0, val);
    return sem;
}

void _eda_sem_finalize(eda_sem_t s) {
    sem_destroy(&s->sem);
    free(s);
}

void eda_obj_retain(eda_obj_t obj) {
    _eda_obj_retain(obj);
}

void eda_obj_release(eda_obj_t obj) {
    _eda_obj_release(obj);
}

eda_time_t eda_time(eda_clock_t clockid, eda_time_t when, uint64_t delta) {
    struct timespec timespec1;
    clockid_t original_clock;
    eda_time_t result;

    switch(when) {
        // Can't offset never
        case EDA_TIME_NEVER:
            return EDA_TIME_NEVER;

        // Offset the current clock time
        case EDA_TIME_NOW:
            clock_gettime(clockid, &timespec1);
            result = (timespec1.tv_sec*NSEC_PER_SEC + timespec1.tv_nsec + delta);

            if(clockid == EDA_CLOCK_WALL) {
                return WALL_TIME(result);
            }

            return SYS_TIME(result);

        default:
            // Make sure the initial time values was given by the same clock
            original_clock = GET_CLOCK(when);
            if(original_clock != clockid){
                // Returns 0b10 or 0b11 (1 in either clock)
                return when & 3;
            }

            if (clockid == EDA_CLOCK_WALL)
                return WALL_TIME(STRIPPED_TIME(when) + delta);
            else {
                return SYS_TIME(STRIPPED_TIME(when) + delta);
            }
            break;
    }
}

uint64_t eda_time_diff(eda_time_t t1, eda_time_t t2) {
    if(GET_CLOCK(t1) != GET_CLOCK(t2))
        return ~0;

    //Get the raw times and perform, and subtract them in an order independent manner
    uint64_t t1strip = STRIPPED_TIME(t1), t2strip = STRIPPED_TIME(t2);
    return t1strip > t2strip ? (t1strip - t2strip) : (t2strip - t1strip);
}
