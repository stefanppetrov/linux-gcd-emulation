//
// Created by spp on 12/04/18.
//

#ifndef EDA_CONT_H
#define EDA_CONT_H

#include "commons.h"

typedef union _eda_task_u {
    struct {
        EDA_OBJ_HEADER(union _eda_task_u);
        eda_func_t func;
        void *ctxt;
        eda_queue_t queue; // Which queue are we supposed to be on?
        eda_group_t group; // Does the task belong to a group?
        _Atomic(union _eda_task_u*) next; // Linkage field
    };
    unsigned char pad[64]; // Ensure cacheline alignment
}*_eda_task_t;


// Create the task cache per thread
void _eda_task_init_cache();

// Flush the cahce partially, or fully
void _eda_task_flush_cache(intptr_t);

// Create task from cache if possible
_eda_task_t _eda_task_create(eda_func_t func, void *ctxt);

// A dummy task, useful for initial item in a queue
_eda_task_t _eda_task_create_dummy();

// Ensure freeing tasks respects cache logic
void _eda_task_free(_eda_task_t cont);

// Execute a task and additional operations based on task fields
void _eda_task_invoke(_eda_task_t task);



#endif //EDA_CONT_H
