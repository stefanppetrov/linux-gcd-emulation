//
// Created by spp on 04/02/18.
//

#ifndef EDA_BACKEND_H
#define EDA_BACKEND_H

#include "commons.h"

int _eda_sched_init(void);
int _eda_sched_schedule_queue(eda_queue_t queue);

extern size_t _EDA_PROCS_AVAIL;

static EDA_INLINE size_t _eda_sched_procs_avail() {
    return _EDA_PROCS_AVAIL;
}

#endif //EDA_BACKEND_H
