//
// Created by spp on 12/04/18.
//
#include "group.h"
#include "queue.h"

#include <Block.h>
#include <errno.h>
#include <syslog.h>

static const struct _eda_object_s EDA_GROUP_VTABLE = {
    .refcnt = 1,
    .finalizer = _eda_group_finalize
};

static void _eda_group_wakeup(eda_group_t grp);
static void _eda_group_dispatch_deferred(eda_group_t grp);

eda_group_t eda_group_create(void) {

    eda_group_t result = calloc(1, sizeof(struct eda_group_s));
    if(!result) {
        return result;
    }
    _eda_obj_init(result, &EDA_GROUP_VTABLE);

    // Ensure these are 0
    sem_init(&result->sem, 0, 0);
    STORE_RLXD(&result->deferred_stack, NULL);

    return result;
}

void _eda_group_finalize(eda_group_t g) {
    // Need to destroy the semaphore
    sem_destroy(&g->sem);
    free(g);
}

static EDA_INLINE void _eda_group_join(eda_group_t grp) {
    _eda_obj_retain(grp);
    // Increment and truncate the waiter bits by converting to 32-bit
    uint32_t running = ADD_REL(&grp->state, 1);

    // Too many tasks / join calls
    if(running == UINT32_MAX) {
        syslog(LOG_ERR | LOG_USER, "[FATAL] Group pending tasks overflow!");
        abort();
    }

}

void eda_group_join(eda_group_t grp) {
    _eda_group_join(grp);
}

void eda_group_leave(eda_group_t grp) {

    uint32_t was_running = SUB_REL(&grp->state, 1);

    // This was the last running task / manual leave call
    // Wakeup waiters and dispatch deferred
    if(was_running == 1) {
        _eda_group_wakeup(grp);
    }
}

static void _eda_group_dispatch_deferred(eda_group_t grp) {
    _eda_task_t head = NULL;

    // Reset the stack pointer to avoid doing CAS loops for every pop operation
    head = XCHG_ACQREL(&grp->deferred_stack, NULL);

    // The pop operations are now thread safe and we don't require release/acquire barriers
    while(head != NULL) {
        _eda_task_t next = head->next;
        // Make sure the queue does not get garbage data written to it and dispatch
        STORE_RLXD(&head->next, NULL);
        _eda_async_task(head->queue, head);
        _eda_obj_release(head->queue);
        _eda_obj_release(grp);
        head = next;
    }
}

static void _eda_group_wakeup(eda_group_t grp) {

    // Ensure we don't lose the running count, as it might have increased afterwards
    uint_fast64_t mask = (1UL << 32UL) - 1UL;
    uint32_t waiters = AND_ACQREL(&grp->state, mask) >> 32;

    // Wakeup waiters
    while(waiters--){
        sem_post(&grp->sem);
    }

    // Dispatch deferred
    _eda_group_dispatch_deferred(grp);
}

void eda_group_async_f(eda_group_t group, eda_queue_t q, eda_func_t func, void* ctxt) {
    if(UNLIKELY(!(func && group && q)))
        return;
    _eda_obj_retain(q);
    _eda_obj_retain(group);

    _eda_task_t task = _eda_task_create(func, ctxt);

    // Might be running out of memory
    if(UNLIKELY(!task)) {
        abort();
    }

    // Join the group and dispatch
    task->group = group;
    _eda_group_join(group);

    _eda_async_task(q, task);
    _eda_obj_release(group);
    _eda_obj_release(q);
}

int eda_group_wait(eda_group_t group, eda_time_t timeout) {
    // Retain the group so it doesn't get released while waiting
    _eda_obj_retain(group);

    // No running tasks?
    if( ((uint32_t)LOAD_ACQ(&group->state)) == 0) {
        // Return immediately
        _eda_obj_release(group);
        return 0;
    }
    else if (timeout == EDA_TIME_NOW) {
        // Don't wait if there are runners and the timeout is instant
        _eda_obj_release(group);
        errno = ETIMEDOUT;
        return -1;
    }
    else {

        int semrval;
        struct timespec timeout_native;
        // Waiters are in the higher 32bits of the state field
        uint_fast64_t increment = ((uint_fast64_t) 1) << 32;

        // Get the state now
        uint_fast64_t state = LOAD_ACQ(&group->state);
        uint32_t running, waiters;

        // Try incrementing the waiter count
        do {
            running = state;
            waiters = state >> 32;

            // There are no running tasks, return immediately
            if (!running) {
                _eda_obj_release(group);
                return 0;
            }
            // In case we fail the CAS, the current state is written to the local variable
        }while(!CAS_WEAK_EXPL(&group->state, &state, state + increment, memory_order_release, memory_order_acquire));

        // Never timeout, ignore interrupts, wakeup on semaphore signaled
        if(timeout == EDA_TIME_NEVER) {
            do {
                semrval = sem_wait(&group->sem);
            } while (semrval == -1 && errno == EINTR);

            _eda_obj_release(group);
            return semrval;
        }

        // timeout is not a special value
        timeout_native.tv_sec = STRIPPED_TIME(timeout) / NSEC_PER_SEC;
        timeout_native.tv_nsec = STRIPPED_TIME(timeout) % NSEC_PER_SEC;
        do {
            semrval = sem_timedwait(&group->sem, &timeout_native);
        } while (semrval == -1 && errno == EINTR);

        // We timed out, have to undo our changes to the group structure
        if (semrval == -1 && errno == ETIMEDOUT) {

            // Try undoing the increment to the waiters
            // This was the last known state
            state = state+increment;
            waiters = state >> 32;
            running = state;

            // Keep trying until we succeed or the group gets woken up part way or fully
            while(waiters > 0 && running) {
                if(CAS_WEAK_EXPL(&group->state, &state, state-increment,
                                 memory_order_release, memory_order_acquire)) {

                    // Successful decrement return with timeout code
                    _eda_obj_release(group);
                    return -1;
                }
                waiters = state >> 32;
                running = state;
            }
            if(UNLIKELY(!running)){
                // A thread which was running the last group task has managed to win the race for the state
                // It is about to signal the semaphore according to the number of waiters it observed
                // We didn't manage to decrement in time before the running count dropped to 0.
                // In order to ensure the semaphore ends up having a correct value we need to wait on it here.
                // It's ok to do so as the wait won't be long.
                while(sem_trywait(&group->sem));
            }
        }

        _eda_obj_release(group);
        return 0;
    }
}

void eda_group_defer_f(eda_group_t group, eda_queue_t q, eda_func_t func, void *ctxt) {
    if(!(group && q && func))
        return;

    _eda_task_t old, task = _eda_task_create(func, ctxt);
    if(UNLIKELY(!task)) {
        abort();
    }

    // Retain queue and group so they don't get released
    _eda_obj_retain(q);
    _eda_obj_retain(group);
    task->queue = q;

    // We have to try adding the task to the stack of deferred tasks
    // However the group might have finished running by the time we attempt the stack push,
    // which might livelock the task on the deferred stack if not checked against.

    _eda_task_t orig = LOAD_ACQ(&group->deferred_stack);
    do {
        if(UNLIKELY(!(uint32_t)LOAD_ACQ(&group->state))) {
            // We've lost the race, reset the next pointer or suffer the terrible terrible data race
            XCHG_RLXD(&task->next, NULL);
            _eda_async_task(q, task);
            _eda_obj_release(q);
            _eda_obj_release(group);
            return;
        }
        STORE_RLXD(&task->next, orig);
    } while( !CAS_WEAK_EXPL(&group->deferred_stack, &orig, task, memory_order_release, memory_order_acquire) );

    // Even if the stack push was successful we might have lost the race with the wakeup.
    // This is a case of the ABA problem as it happens only when we observe an empty stack (NULL),
    // before CAS-ing, a wakeup occurs, we CAS on the still empty stack and succeed.
    // The task is now effectively lost unless the group starts running again. So we need to check for this case.
    if(UNLIKELY(orig != NULL)){
        return;
    }

    // Deferred tasks dispatched, nothing left to do
    int callbacks_pending = LOAD_ACQ(&group->deferred_stack) == NULL;
    if (!callbacks_pending) {
        return;
    }

    // Group is running again, some later wakeup call will dispatch this task
    uint32_t running = (uint32_t)LOAD_ACQ(&group->state);
    if(running){
        return;
    }

    // Worst case, we have to dispatch the all the stuck deferred tasks.
    _eda_group_dispatch_deferred(group);
}

#ifdef __BLOCKS__
void eda_group_async(eda_group_t group, eda_queue_t q, eda_block_t func){
    // Make sure a heap copy of the closure is made
    eda_group_async_f(group, q, (eda_func_t)_eda_call_async_block, Block_copy(func));
}
void eda_group_defer(eda_group_t group, eda_queue_t q, eda_block_t func){
    // Make sure a heap copy of the closure is made
    eda_group_defer_f(group, q, (eda_func_t) _eda_call_async_block, Block_copy(func));
}
#endif
