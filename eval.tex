\chapter{Evaluation}\label{chap:eval}
This chapter aims to provide answers to the 2 central questions posed by the project. Firstly, to determine the performance and usability of parallelism frameworks implementing the GCD metaphor of queue dispatching. Secondly, finding out if the Linux GCD implementation suffers any overheads from using compatibility layers by comparing it to our alternative queue dispatching library EDA. This will also double as a performance evaluation and testing for EDA.
\par
We'll begin by defining the experimental design used for the evaluation, including metric choice and definition, benchmark problems and their implementation, and the process used to obtain the results. After that we'll review and analyse the resulting data for the usability and the performance metrics. Finally we'll summarize our findings and how the metrics relate to each other and draw any relevant conclusions.

\section{Experimental design}\label{sec:experimental}
There are several works which investigate the performance and usability of parallel languages and frameworks \cite{wilson1995cowichan, nanz2013benchmarks, paudel2011cowichan, korch2003}, which regardless of their recency are still relevant. As a result instead of developing our experimental design from scratch we will draw heavy inspiration form the methodology of previous researchers  adapting it to suite the needs of the project.

\subsection{Metrics}
We want to measure usability and performance of a parallel programming framework $X$, but we haven't defined what those terms mean yet. Usability is how easy it is to code a \textit{correct}solution for some problem using framework $X$, and performance is how efficient is our \textit{correct} solution for the problem when using framework $X$. It is important to stress on the correctness of the produced solution as otherwise comparisons are more or less meaningless, and therefore we treat it as a prerequisite.
\par
Having the abstract definitions of usability and performance, we still have to define some concrete metrics which to represent these properties based on our output data. The metrics used for our experiment are as follows:
\begin{description}
	\item [Source code size] Source code size in lines of code (LoC) for a given problem solution using framework $X$.
	\item [Auxiliary constructs] How many auxiliary constructs are required to achieve a parallel solution to the problem when using framework $X$.
	\item [Execution time] How long does it take to execute the problem solution using framework $X$. Measured in seconds as a floating point.
	\item [Memory consumption] How much memory does the problem solution use over the course of its execution when is written using framework $X$. Measured in maximum heap size.
\end{description}
Source code size has been found to correlate strongly to usability especially if measured in conjunction with coding time\cite{nanz2013benchmarks}. Unfortunately, coding time was not really applicable in our case due to most of the solutions being written by the author and thus it is too biased with too few data points to be a reliable metric. 
\par
In order to make the source code size more meaningful we'll use the number of auxiliary constructs, required to arrive at a parallel solution. Essentially, this metric represents a simplified cognitive walk-through method\cite{spencer200cgw} employed in usability testing when statistical methods are too demanding. As the cognitive walk-through itself produces crude results, this metric as a simplification of it should be treated carefully as a rough guideline. Some specific examples auxiliary constructs are additional structure and function definitions.
\par
The choice of execution time for a metric should be more than clear. What might be puzzling to some readers is the omission of speed-up as a metric here. Unlike in the study of Nanz et al\cite{nanz2013benchmarks}, we only compare parallelism frameworks for the C programming which makes measurement noise introduced by the sequential nature of the language less impactful. Nonetheless, it is somewhat of a glaring which should be addressed as part of future work.
\par
Memory consumption was included as a metric due to the need to measure any potential overhead introduced by the compatibility layers of the Linux implementation of GCD, making it a performance metric. Another reason for why it is useful to consider as a performance metric is that as was discussed in section \ref{subsubsec:malloc}, memory allocations can be a performance bottleneck in parallel programs. Therefore we believe there will be a correlation between memory use and fluctuations in it and overall performance.

\subsection{Evaluated frameworks}
The main comparison we consider is between the Linux implementation of GCD, EDA, as the alternative to it, and PThreads. To be more specific GCD and EDA will be compared against one another mostly in terms of performance, as they represent the same parallelism paradigm of message passing via queues.  Of course they will also be compared to PThreads in both performance and usability terms.
\par
As there are a number of slightly different implementations of GCD on Linux, we had to select one of them. The one which was chosen in the end was the \textbf{libdispatch} Debian package\cite{deb-package}, primarily due to its ease of access on Ubuntu Linux distribution used. The official Apple  sources are also available and provide a more recent programming interface, but they require many intermediate steps to install correctly\cite{apple-gcd-install}.
\par
EDA is as specified in this report, and PThreads are using the underlying Native POSIX Thread Library for Linux (NPTL)\cite{nptl}.
\par
Including the parallelism frameworks and languages mentioned in subsection \ref{subsec:related} was also contemplated, but ultimately we decided against it due to time constraints.

\subsection{Problem set}
Some of the problems we'll consider for benchmarking are not novel in any way, and in fact most of them are borrowed from some variant of the Cowichan problems\cite{wilson1995cowichan, nanz2013benchmarks, paudel2011cowichan}. The main advantage of this problem set is that it covers a sufficient variety of parallel programming patterns, which is important for comparative purposes. Another plus is that these are relatively small problems which is important in the context that this project is not solely experimental but also includes design and engineering work.
\par
Two of the benchmarks are synthetic, in the sense that they do not attempt solving a toy problem but rather try to emulate a specific workload which is difficult to reproduce reliably using some existing problem. These two benchmarks are intended specifically for performance testing EDA against GCD but they will be presented along with the rest regardless.
\par
The most significant disadvantage of these problems is them being toy problems which cover only a set of basic parallelism patterns and usage patterns, and as such the results will not be fully representative of large real-world applications.

\subsubsection{Matrix multiplication (matmult)}
Given 2 matrices of size $N \times N$ calculate their product. This is the most basic problem examined as it models a single fork and join calculation over a loop with no data dependencies if extra memory is used.
\begin{description}
	\item [Input constraints :] $100 \leq N \leq 1500$. Matrix contents irrelevant as execution time is dependent solely on matrix size.
\end{description}

\subsubsection{Gaussian elimination (gauss)}
Given some matrix of size $N \times N$ of floating point numbers use Gaussian elimination to reduce it to row echelon form. Compared to matrix multiplication, this problem is not as nearly as easy to parallelize due to data dependencies between rows, but multiple solutions including ones with pipelining exist.
\begin{description}
	\item [Input constraints :] $100 \leq N \leq 1500$. Matrix has to be symmetric and row-dominant to ensure correct execution\cite{wilson1995cowichan}.
\end{description}

\subsubsection{Histogram generation (threshold)}
Given some matrix of size $N \times N$ of integers find its intensity histogram. This is a modified version of the histogram thresholding problem\cite{wilson1995cowichan}. That problem is about reducing the matrix to a histogram and then using the histogram to discard some percentage of the matrix elements based on the histogram distribution and a threshold percentage.
\par
We consider calculating the histogram to be the more interesting part of the problem as its essentially 2-dimensional parallel reduction. Finding where the threshold is on the histogram is not  worth parallelizing since the number of bins is not likely to be very large, and discarding pixels afterwards is embarrassingly parallel. As a result we only consider the histogram generation.
\begin{description}
	\item [Input constraints :] $100 \leq N \leq 5000$. No restrictions on matrix contents.
\end{description}

\subsubsection{Parallel graph traversal}
Given an undirected graph $(V,E)$ generate a traversal for it from a given starting node $S$. 
\par
Although based on a real problem this is a synthetic algorithm, where we simulate contention over some shared resource which is represented by the set of visited nodes. Also unlike the Cowichan problems which are more oriented towards data parallelism this problem allows for exploring task parallelism solutions.
\par
Something else to note about the correctness testing of this benchmark is that its output is non-deterministic - a different traversal tree can be built each run. As such it can't be tested by simply running it against a sequential traversal, but we'll have to verify that the traversal tree is possible instead.
\begin{description}
	\item [Input constraints :] $100 \leq |V| \leq 5000$. Graph has to be connected and to have a density $D \approx \frac{|N|}{log_{2}|V|}$, but can be otherwise random. The density requirement is so as to force sufficient contention without requiring a full graph.
\end{description}
\subsubsection{Fibonacci dispatch (synth)}
This is a purely performance focused benchmark for comparing GCD and EDA, based on the mostly unchanged synthetic benchmark suggested by Korch and Rauber\cite[p. 15]{korch2003}, for evaluating the performance of different types of thread pools.
\par
We define a task $A$ as:
\begin{equation}
   		A(i,f) = \begin{cases}
   		\{100f\} & \text{for } i \leq 0 \\
   		\{10f\}A(i-2)\{50f\}A(i-1)\{100f\} & \text{for } i > 0 
		\end{cases}
\end{equation}
Where $\{N\}$ means a loop with $N$ iterations and invocations of $A$ are asynchronous. The input are 2 integers $K,F$. We dispatch $A$ asynchronously $K$ with its first argument ranging from $K-1$ to 0, and the second being $F$. We repeat the process for every priority level in ascending order from low to high priority and wait for all asynchronous tasks to complete. The number of tasks grows exponentially with $K$ according to the Fibonacci sequence, hence the name Fibonnaci dispatch.
\par
As the paradigm of GCD and EDA is around the thread pool pattern this benchmark is good for measuring their performance regardless of them using a thread pool internally. The other attractive properties of this benchmark is that it does not introduce any noise in the performance metrics as there is no actual computation and resource use aside from that of the library benchmarked itself.
\begin{description}
	\item [Input constraints :] $10 \leq K \leq 35$, due to spawning an exponential on a constant number of processors. $F=5$ for all values of $K$ so as to prevent too long execution times. 
\end{description}

\subsubsection{Rapid timer fires (drift)}
This is another performance benchmark only for GCD and EDA which tests their event handling subsystems. For a given input integer $N$ we create $6$ groups of $N$ timers. The expiration interval for timers in group $k, 0 \leq k < 6,$ is $1^{-k}sec$. We let the timers run for 20 seconds and terminate the process.   
\par
As this test has a set execution time we can't really use the execution time metric for it. In order measure its performance we count the number of caught timer events, equal to the event handler calls, missed timer events, coalesced timer events minus caught events, and the average drift of all timers. Not that missed events could alternatively be defined as expected events minus caught events, but since both libraries report how many timer events occurred between handler calls we can use that instead.
\par
This benchmark intends to simulate a large number of small requests coming to a server in a rapid pace. In order to avoid accounting for I/O performance and noise introduced by it or any other structures we simply use timers. A downside of this benchmark is that the load is too regular and there are no spikes in the number of incoming requests.
\begin{description}
	\item [Input constraints :] $1 \leq N \leq 5$, as this is is sufficient to find any divergence in performance. 
\end{description}

\subsection{Benchmarking process}
For every benchmark we iterate over admissible range of input sizes and generate input data, which can be just the input size in some cases, of that size. The input data is then passed to each of the benchmark implementations which run in sequence as isolated processes.
\par
In order to get a more stable estimate and suppress outliers for execution time, every benchmark runs the solution over the input for a fixed number of iterations and take the average execution time of all iterations. In this way we also account for interference from other system processes which might have been consuming processor time. The \textbf{drift} benchmark is the only exception to this as it does not use execution time.
\par
For measuring memory use we use the Massif heap profiler from the Valgrind tool suite, see discussion in subsection \ref{subsec:valgrind}. We profile the memory use of a benchmark run for a sufficiently large input size with respect to the benchmark, but only once due to prohibitive runtime when running under the profiler.
\par
In order to achieve a better understanding of why the benchmarks behave in a certain way and give certain results we use Valgrind's Callgrind profiler. This is run on the same input as the memory profiler, and again only once due to significant order-of-magnitude slowdowns. Call graph figures for some of the benchmarks are available in appendix \ref{appdx:extra-figs}.
\par
All benchmarks were run on a Intel i7-7700HQ (3.8 GHz, 4 physical cores, 8 virtual cores) machine with 16 GB RAM, using Linux Ubuntu 17.10 distribution with a generic 4.14 Linux kernel.
\par

\subsection{Implementation}\label{subsec:eval-impl}
\subsubsection{Code files and structure} 
We present the code files and structure along with the experimental design to facilitate reproducibility of results.
\begin{description}
\item [examples/shared/ :] Code which may be shared by multiple benchmarks. Currently this includes a file for matrix operations, another for graph related ones, and finally a header with benchmarking macros.
\item [examples/template/, genexam.sh :] The template directory and this script are used for generating new benchmarks of a specified name by providing skeleton source files.
\item [examples/BENCHMARK/ :] All other subdirectories contain files for a specific benchmark these directories have the following structure:
	\begin{description}
	\item [examples/BENCHMARK/FRAMEWORK.*.c :] The implementation of the benchmark using a specific framework. Generally, all benchmarks will have EDA, GCD (dispatch) implementations and most will also have a PThreads one. 
	\item [examples/BENCHMARK/gen-benchmark.c :] Generates benchmark input data based on input parameters.
	\item [examples/BENCHMARK/limits.cfg :] Describes the range of input sizes to be used for the current benchmark.
	\end{description}
\item [benchmark.sh :] This shell script is responsible for running and profiling a benchmark over the range of input parameters specified in the \textbf{limits.cfg} file of the benchmark.
\item [results/BENCHMARK :] Contains the results for an individual benchmark. Output files will be of the \textit{[PREFIX.]FRAMEWORK.BENCHMARK.out} with prefix being denoting a Massif or a Callgrind profile.
\end{description}
\subsubsection{Benchmark problem solutions}
Here we'll briefly outline the solution algorithms for each of the benchmark problems when using the evaluated frameworks. To ensure a fair comparison all implementations will try to use the same parallel algorithms if possible, or approximations thereof with differences noted. The \textbf{synth} and \textbf{drift} synthetic benchmarks will not be covered as their definition specifies the implementation anyway.
\paragraph{matmult} Regardless of benchmark we use the naive $O(n^{3})$ matrix multiplication algorithm with a parallelized outer loop. 
\paragraph{gauss} Similarly to matrix multiplication we parallelize the classical $O(n^{3})$ algorithm. Due to data dependencies the outer loop can't be parallelized easily and so we parallelize the inner loop of subtracting rows. The same parallelization scheme is used for all implementations.
\paragraph{threshold} In order to find the range for each bin correctly we use parallel reduction to find the minimum and maximum element. After the bins are constructed we use parallel reduction to fill them. The reduction use thread/task local reductions which are then combined. Unlike in previous cases we can't use parallel loops in the EDA and GCD implementations for the reductions as we don't know which iterations are allocated to which thread. Instead we manually divide the work to indexed tasks.
\paragraph{dfs} The solution is essentially breadth-first search in parallel by dispatching tasks for node to be traversed asynchronously. This is done naturally in EDA and GCD with queues. For PThreads we use a simple mutex protected queue and a team of worker threads. In order to protect the set of visited nodes from data races we use synchronous dispatch to a serial queue in GCD and EDA, and a mutex for PThreads. For performance measures we will also use alternative EDA and GCD implementations using asynchronous dispatch to the serial queue, to be referred to as \textbf{dfsalt}.
\section{Results}
In this section we will present the results from the experiment as defined in section \ref{sec:experimental}. 

\subsection{Usability metrics}\label{subsec:usability}
Figure \ref{fig:usemetrics} displays both the absolute lines of code and the number of auxiliary constructs for the benchmark solutions across using the different frameworks. We'll review both metrics together since number of auxiliary constructs augments the data for code size.
\par
\begin{figure}[htb]
\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=0.9\textwidth]{results/lengths}
	\caption{Source code size in LoC}
	\label{fig:loc}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=0.9\textwidth]{results/extras}
	\caption{Number of auxiliary constructs}
	\label{fig:nac}
\end{subfigure}
\caption{Usability metrics}
\label{fig:usemetrics}
\end{figure}
Unsurprisingly, the EDA and GCD solutions are very closely matched, and although there are some differences across the different benchmarks but they are inconsistent and too small to be significant. PThreads scores significantly worse on this metric, around 1.4-2.0 times the LoC when using either GCD or EDA. This is primarily due to having to setup multiple additional data structures to pass information to threads.
\par
While for most of the benchmarks the number of extra constructs seems to keep to the code line trends, for \textbf{threshold} the PThreads implementation requires just 6 constructs against the 5 of GCD and EDA. This is also the only benchmark problem for which we use reduction, which might be the cause of this deviation from the trend. And indeed as we've mention in section \ref{subsec:eval-impl}, the EDA and GCD solutions can't make use of the parallel loop functions to achieve efficient parallelization without mutual exclusion, so we have to manually partition the input for different tasks. Which is exactly what is done in the PThreads solution, and hence the closeness in results for this metric.
\par
On the other end of the spectrum we have the \textbf{dfs} benchmark, where for PThreads we have to implement a parallel queue, use a number of mutex locks, a condition variable, a semaphore, and manually free queue resources to achieve what EDA and GCD provide directly. This explains the consistent result across the metrics.
\par
Likewise for \textbf{matmult} and \textbf{gauss} where GCD and EDA give us loop parallelization freely, while in PThreads we have to implement it per benchmark, which gives more LoC and auxiliary structures.
\par
The synthetic \textbf{synth} and \textbf{drift} were not measured for usability, due to being synthetic problems for measuring only performance metrics.

\subsection{Execution time} \label{subsec:exectime}
The next metric we'll examine is execution time, as visualized by figure \ref{fig:extime}. 
\par
\begin{figure}[!htb]
\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/gauss/gauss}
	\caption{gauss}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/matmult/matmult}
	\caption{matmult}
\end{subfigure}
~
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/threshold/threshold}
	\caption{threshold}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/synth/synth-log}
	\caption{synth}
	\label{fig:extime-synth}
\end{subfigure}
~
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/dfs/dfs}
	\caption{dfs}
	\label{fig:extime-dfs}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/dfsalt/dfsalt}
	\caption{dfsalt}
	\label{fig:extime-dfsalt}
\end{subfigure}

\caption{Execution times, lower is better}
\label{fig:extime}
\end{figure}

On \textbf{matmult} and \textbf{gauss}, GCD and EDA performance appears to be nearly identical disregarding some noise, which leaves little to nothing for analysis.
\par
On the \textbf{threshold} benchmark we can observe that EDA performs 1.1 times worse than GCD, with execution time growing more quickly despite near identical performance for small inputs. This is somewhat surprising considering both implementations use simply 2 fork-joins for the histogram reduction and locating the minimum and maximum. Profiling with Callgrind did not provide useful information regarding this disparity.
\par
In figure \ref{fig:extime-synth} we present the execution time in a $log_{10}$ scale due to its range spanning several orders of magnitude. This allows us to clearly see that although the EDA implementation runs slower for smaller values of $K$, the execution times converge and meet at larger values. Profiling at $K=25$ where GCD is still clearly faster and there are a significant number of tasks dispatched, shows EDA scheduling queues 2.5 times more often than GCD, see figures \ref{fig:synth-cg-dispatch} and \ref{fig:synth-cg-eda} in appendix \ref{appdx:extra-figs}. Similarly to thread context switching, this wastes processing time and it is the likely cause of the slowness.
\par
The biggest divergence in the execution time of the GCD and EDA implementations was in the \textbf{dfs} benchmark. The GCD implementation was 5 times slower for most inputs, while it was expected that GCD and EDA execution times would be closer. Profiling revealed that the time spent in the synchronous dispatch calls to the serial queue in GCD was roughly 4 times longer than in EDA , as shown in figures \ref{fig:dfs-cg-dispatch} and \ref{fig:dfs-cg-dispatch} in appendix \ref{appdx:extra-figs}.
\par
In order to further investigate this result we also repeated the benchmark on the \textbf{dfsalt} implementations of \textbf{dfs} where the access to the serial queue is asynchronous. As we can see   in figure \ref{fig:extime-dfsalt} the execution times are much closer and consistent with what was observed in the other benchmarks so far with \textbf{GCD} outperforming \textbf{EDA} by a factor of 1.4 on most inputs, further confirming the bad scheduling behaviour.
\par
The performance of PThreads implementations is the most volatile, as we can observe it being the fastest by far for \textbf{dfs}, to being considerably slower than the others in \textbf{gauss}, to being either the longest and shortest running in \textbf{matmult} and \textbf{threshold}, but with comparatively small gaps. For \textbf{dfs} and \textbf{gauss} the difference can be explained by the fact that for \textbf{dfs} we keep reusing the same threads until the graph traversal completes, and for \textbf{gauss} we keep creating new ones for every iteration. Thread reuse in \textbf{gauss} would have likely resulted in outperforming the GCD and EDA solutions.

\subsection{Events throughput}\label{subsec:evhandled}
Due to using different metrics to measure performance and efficiency we examine the \textbf{drift} benchmark separately. Figure \ref{fig:evdrift} shows the events handled, events missed and average timer drift over 20 seconds.
\par 
\begin{figure}[t]
\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/drift/drift-missed}
	\caption{Missed events, lower is better}
	\label{fig:evdrift-missed}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\textwidth]{results/drift/drift-drift}
	\caption{Timer drift (sec), lower is better}
	\label{fig:evdrift-drift}
\end{subfigure}
~
\begin{subfigure}[b]{0.6\textwidth}
	\includegraphics[width=\textwidth]{results/drift/drift-caught}
	\caption{Handled events, higher is better}
	\label{fig:evdrift-caught}
\end{subfigure}
\caption{Event metrics per 20 seconds (drift)}
\label{fig:evdrift}
\end{figure}
While the EDA event handling mechanism will handle significantly more events in the cases with less timers, as we add timers the graph of events handled( fig. \ref{fig:evdrift-caught}, will take a logarithmic shape, plateauing at 3 timers per frequency group and starting to degrade slightly after. GCD in contrast handles less events in the smaller cases, but retains a roughly linear growth in the events handled as we add more timers.
\par
EDA still performs better for missed events initially before starting to miss more events than GCD when more timers are added. What's interesting is that in both cases considerably more events are missed than handled and the number of missed events of GCD appears to grow linearly with timers, leading to a smaller gap for this metric.
\par
The timer metric drift seems to grow exponentially for EDA, meaning that not only we will miss more events, and handle roughly the same number as the number of event sources increases, but events will be handled with larger latency as well. As with the other metrics the drift for GCD is roughly linear with respect to the test size, resulting in a more gracefully increasing latency.
\par
Profile data indicates that the issue is once again the queue scheduler, with calls for scheduling a queue being nearly 10 times more frequent in the EDA case, as seen in figures \ref{fig:drift-cg-dispatch} and \ref{fig:drift-cg-eda} in appendix \ref{appdx:extra-figs}. Due to the drastically higher number of queue scheduling operations we can also expect more memory allocations for thread pool tasks.
\par
Additionally, this benchmark causes the EDA task cache to behave badly, which is due to the cache recycling tasks only threads which consume them. The event loop thread of EDA is a nearly exclusive tasks producer, which means it will allocate memory from the system allocator almost every time. The other threads being exclusive consumers will have their task caches very frequently being overflowed and freed. Overall, the task cache is more of an overhead in this case as we perform normal allocations anyway and do the ineffective caching operations on top.

\subsection{Memory consumption}\label{subsec:memory}
The final metric we have to review results for is memory usage in terms of maximum heap size per benchmark run for a specific input size. Space allocated for inputs is factored out. For the non-synthetic benchmark the maximum heap sizes in kilobytes (KB) are in figure \ref{fig:heaps}, while for \textbf{synth} and \textbf{drift} memory usage is provided in larger detail in figures \ref{fig:heap-synth} and \ref{fig:heap-drift}.
\par
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.7\textwidth]{results/heap}
	\caption{Maximum heap size in KB}
	\label{fig:heaps}
\end{figure}
For all benchmarks the PThreads implementations use the smallest amount of heap space. In most cases the difference is not big, but for \textbf{dfs} EDA and GCD consume an order of magnitude more heap space. This is also the only non-synthetic benchmark which makes extensive use of the task dispatch and queue functionality directly, potentially meaning that this GCD and EDA functionality might have large memory costs associated.
\par
Both GCD and EDA follow the same trends throughout the non-synthetic benchmarks, with EDA implementations using marginally less memory in some cases. Figures \ref{fig:heap-synth} and \ref{fig:heap-drift} in appendix \ref{appdx:extra-figs} provide a more in-depth view for the synthetic benchmarks which shows that there are more differences in memory use between EDA and GCD than is visible in figure \ref{fig:heaps}.
\par
For \textbf{synth} the EDA implementation had a drastically lower maximum heap size of 25MB as opposed to GCD's 37MB. What's more the heap usage patter is more or less the same staying below 12MB most of the time. This suggests that under normal loads both the GCD and EDA implementation should behave similarly well, but under higher loads GCD might consume much more memory. We can also speculate that this is one of the causes for EDA having better execution time than GCD on large inputs of \textbf{synth}, as the latter might be slowed down by allocating large amounts of memory in small chunks.
\par
For \textbf{drift} we can also observe a similar difference in maximum heap size on a smaller scale, with EDA consuming 14KB and GCD consuming 23KB at most. The pattern of memory allocations is even more comparable as both release a small amount of memory after reaching the maximum, and staying mostly flat afterwards at 20KB for GCD, and 13KB for EDA.
\par
Based on the discussion from subsection \ref{subsec:evhandled}, it is possible that this is due to memory being reclaimed more frequently. However, the memory consumption graphs in figure \ref{fig:heap-drift} does not suggest this is the case as we would have observed a graph with more valleys and peaks for EDA, and the memory consumption pattern in both is nearly identical.
\par
A more detailed examination of the top 5 allocator functions for each heap profile( see figures \ref{fig:heap-drift-eda-details} and \ref{fig:heap-drift-gcd-details} in appendix \ref{appdx:extra-figs}) revealed that the primary cause of the larger heap in the GCD case was the \textbf{kqueue} component which allocates 8KB, roughly $\frac{1}{3}$ of the entire 23KB heap. This is the event notification compatibility layer\cite{linux-kqueue, deb-package, xnu-kqueue} used by GCD, which confirms our hypothesis that using it introduces some overheads in the GCD implementation for Linux albeit only memory-wise. Regretfully, due to the limited scope we could not devise additional benchmarks with which further to evaluate the extent of this overhead.

\section{Summary}
To begin with, GCD, and similar frameworks such as EDA, do indeed provide a usability advantage over using threads directly, as indicated by our results in subsection \ref{subsec:usability}. Both EDA and GCD consistently required significantly less code or additional programming constructs in most cases. The only case where there wasn't a significant usability disadvantage to using PThreads was the \textbf{threshold} case which required parallel reduction, an algorithmic pattern not supported well by either GCD or EDA. This might be an indication that other algorithmic patterns, like the closely related prefix scan, might not be expressed conveniently using the metaphors of GCD or EDA.
\par
Conversely, as shown by the experimental data in subsections \ref{subsec:exectime} and \ref{subsec:memory}, the usability advantage of GCD and EDA does not translate to a definitive improvement in efficiency. While they would outperform poorly written PThreads programs in terms of execution time, more complex but better designed code can be both substantially faster and have a smaller memory footprint. For an example of the first case see the \textbf{gauss} results in subsection \ref{subsec:exectime}, and for the latter see the \textbf{dfs} results in sections \ref{subsec:exectime} and \ref{subsec:memory}. Moreover, memory consumption with GCD or EDA was consistently higher across all benchmarks, reaching an order of magnitude difference in the \textbf{dfs} benchmark. Overall, these libraries are a compromise between complexity and efficiency.
\par
Finally, evaluating EDA against the Linux GCD port has given mixed and varied results. On one hand we've found that EDA's queue scheduling algorithm is wasteful, causing performance to be up to 1.4 times worse compared to GCD in some cases, despite performance being identical under very high system load. Additionally, experimental data has shown that the task memory allocation cache used in EDA, is indeed not suitable for use cases where it is possible to have a consumer / producer imbalance. Furthermore, while the event notification subsystem has better throughput than its GCD equivalent for smaller loads, its performance degrades faster as event frequency increases. This might be solely due to previous issues, or due to some other yet undiscovered factor in addition. Overall, this makes the fulfillment of the \ref{nfreq:performance}, \ref{nfreq:events} and \ref{nfreq:latency} performance requirements questionable.
\par
On the other hand, there are areas where EDA was more efficient than GCD, the main example of which is memory use. Under heavy load EDA's maximum heap size was roughly 66\% that of the equivalent GCD program, while overall retaining the same memory consumption patterns in other cases. We've also the \textbf{kqueue} event notification compatibility layer used by Linux ports of GCD does indeed introduce overheads expressed as a larger memory footprint, although due to the time scope of the project we could not assess its full extent. Another minor case where we found EDA to be superior was synchronous dispatch where the evaluated GCD implementation would perform consistently worse by a large margin.