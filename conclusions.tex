\chapter{Conclusions}
Having covered every aspect of the project individually, in this chapter we'll reflect on the project and our approach to it in their entirety. 
\par
We began the project by introducing Apple's GCD parallelism and questioning the utility of its implementation on Linux, due to the efficiency concerns of using compatibility libraries, and its reduced potential for adoption stemming from licensing differences. To address that we proposed the alternative framework implementing GCD's parallelism model, whose design and implementation was the primary goal of this project. The secondary goal we set for the project was verifying the claims about GCD being more user-friendly and efficient than using threads directly.
\par
We'll assess how effective the project was in achieving these goals, starting with the respects in which it was successful, moving onto examining areas which were subject to limitations and could be improved, and finally pointing out directions for future work. Aside from assessing the overall success of the project, we'll also reflect on what was learned throughout the duration of the project.

\section{Successes}
Based on the literature reviewed, we specified, designed and implemented the EDA library, using almost exclusively non-blocking lockless parallel data structures based on atomic primitives. From the results of the functional conducted testing in \ref{chap:testing}, we can conclude that all the high priority requirements of asynchronous and synchronous task dispatch to serial and parallel FIFO queues, task group synchronization, asynchronous event notification handling and loop parallelization were met. Additionally, we introduced several configuration options not present in GCD like OpenMP inspired parallel loop scheduling and tuning queue scheduling behaviour. As a result we can consider EDA to be a functional alternative implementation of GCD's parallelism paradigm of on Linux.
\par
In terms of memory consumption we found that EDA is an improvement over GCD, as shown by the results in subsection \ref{subsec:memory}. While in most benchmarks peak heap size in EDA would be only slightly lower than that in GCD, under high loads the EDA peak heap size was approximately 30\% lower than the GCD one with the same or lower baseline heap size. In one of the benchmarks it was found that the increased memory use was almost exclusively due to the \textbf{kqueue} compatibility layer, thus confirming the hypothesis that it was an overhead, at least in one respect.
\par
In terms of performance our results showed that EDA synchronous dispatch to serial queues was at least 4 times faster than the GCD analogue when under heavy contention, although we could not determine the exact root cause for this difference.
\par
From the results from the evaluation of GCD and EDA against PThreads in subsection \ref{subsec:usability} we established that the GCD (and by extension EDA) parallel programming model decidedly simplified most benchmark programs from their PThreads equivalents, with the exception of the one requiring parallel reduction where the code was only marginally simpler. As a result we could confirm that the GCD and EDA parallelism paradigm does indeed reduce code complexity.
\par
However, we also found that despite the usability advantage, well written programs using threads directly can be significantly faster than equivalent GCD or EDA programs and that both GCD and EDA can require substantially more memory, see subsections \ref{subsec:exectime} and \ref{subsec:memory}. Therefore we successfully disproved the claim that GCD (and EDA) is definitively more efficient than using threads directly. A more appropriate statement would be that it offers a compromise between performance and usability.

\section{Areas for improvement}
Although the project achieved its goal of producing EDA as a Linux alternative to GCD, confirming that Linux GCD port could be more efficient without its compatibility layers, and evaluating GCD and EDA for performance and usability, we are far from considering it completely successful.

\subsection{Implementation}
EDA was intended to be an overall more efficient that GCD on Linux. However, the evaluation data from subsection \ref{subsec:exectime} evidenced that EDA was only either as fast as GCD or slower across all benchmarked cases, being up to 1.8 times slower in some cases, excluding the synchronous dispatch benchmark. Similarly, for event handling we found that even though it could handle more events initially it scaled worse than GCD, as seen in subsection \ref{subsec:evhandled}. Although, some of the data indicates that for very high system loads there would be no measurable difference in performance. This is still clearly undesirable, as requirements \ref{nfreq:performance}, \ref{nfreq:events} and \ref{nfreq:latency} are met only partially.
\par
The primary reason for EDA underperforming was the poor scheduling algorithm, which results in threads switching between queues more often than in GCD, wasting processing time. While we noted that the current queue scheduling algorithm could behave badly in \ref{subsubsec:adispatchimpl}, we did not expect it to affect performance to this extent. As such it is the first part of the implementation which should be revised, and in retrospect more attention should have been given to it than implementing some of the low-priority functional requirements.
\par
The other identified cause for the subpar performance, especially in the event handling benchmark, was the task allocation cache. As noted in section in section \ref{sec:tcache}, it does not offer any benefits in a producer / consumer scenario, which is exactly the case with the event subsystem . The event loop thread is a nearly exclusive producer and the others threads are almost exclusive consumers. Another drawback regardless of use case is that the task cache has poor spatial locality, which results in poor utilization of the hardware cache. Consequently, the task cache should be the next component to be reworked.
\par
Additionally, while we did not observe any errors related to the ABA problem during testing and evaluation and explained why we believe it should occur extremely rarely in chapter \ref{ch:impl} for all susceptible structures, it is not eliminated and may cause some hard to diagnose errors when using EDA. Considering there are known solutions to the ABA problem, it is simply a matter of adapting one for the project's purpose.

\subsection{Evaluation}
Due to the majority of time spent on the project going towards engineering work of designing, implementing and testing EDA, the evaluation experiment was rather limited.
\par
Firstly, as the experiment included usability measurements it should have been conducted in a manner similar to the experiment of Nanz et al\cite{nanz2013benchmarks}. By asking multiple participants to write code for the benchmarks, we would have gotten more accurate and objective usability than the ones used, which could have potentially given us different results not only the usability metrics but the performance ones as well.
\par
Secondly, the benchmark problem set was limited in size and some of the benchmarks were flawed. Benchmarks covering more parallelism patterns such as pipelines, parallel prefix scan, more dynamic workloads and event handling should have been used for more representative results. Moreover, even though we found that there was an increased memory usage from compatibility layers in GCD, it was just for one specific benchmark, whereas it should have surveyed more thoroughly.
\par
Finally, some important parallelism metrics such as speed-up over the sequential algorithm, and efficiency in terms of processor cores utilization were not included. This could have revealed some additional information about overall performance which would not be visible solely from execution times.

\section{Future work}
Of course, even after concluding the project there is still more work and research to be done, which was beyond its scope for the given time frame. There is a distinction to be made from the areas for improvement, since those are within the scope of the project but were found to be lacking in some respects. The list of directions for future work is as follows:
\begin{itemize}
	\item Satisfying the remaining low priority requirements like supporting file system event sources, parallel loop granularity settings and setting a hard upper limit on parallel queue consumers.
	\item Future implementations of EDA could use a work stealing thread pool or one with private task queues, as those have been shown to outperform work sharing thread pools with a central queue\cite{korch2003} like the one used in EDA, see section \ref{sec:thrpoolimpl} and appendix \ref{appdx:pwq}.
	\item Adding an interface for parallel reduction, as that was one of the use cases where neither EDA nor GCD was significantly easier to use than PThreads.
	\item Expanding the evaluation to compare GCD and EDA to other parallelism frameworks or languages such as the ones listed in \ref{subsec:related}, but not limited to them.
	\item Providing formal proofs of correctness for the original algorithms presented in chapter \ref{ch:impl}.
\end{itemize}

\section{Personal retrospective}
\subsection{Low-level parallelism is challenging}
At the beginning of the project the only practical experience with parallelism was with small toy problems where no significant engineering work had to be done. Even after reading literature on the topic it was still difficult to fully grasp some concepts of parallel programming, for example memory ordering of normal and atomic operations and lock-free algorithms, without failing a few times during the implementation and testing cycle.
\par
Nonetheless, over the course of the implementation and testing we've come to understand these concepts better and appreciate the challenges associated with parallel programming including the ABA problem, load balancing and scheduling, the relationship between memory allocation and performance, and the difference between weak and strong memory operation orderings. Of course, reaching that point would have been impossible without the foundations provided by the literature survey.

\subsection{Keeping the scope focused}
Keeping the scope of the project limited is important as it ensures amount of work needed is feasible to carry out before the deadlines. Both the engineering part of the project and the experimental evaluation were affected by scope creep. With regards to engineering EDA, we prioritized requirements to avoid this problem, but there were simply far too many functional requirements and less time was spent on crucial non-functional requirements, which lead to the inefficient queue scheduling and tasks caching implementations. At the same time the experimental evaluation was also trying to answer too many questions, and as a result the experiment conducted was incomplete, despite still giving some results.

\subsection{Thinking things through}
For large portions of the project implementation algorithms were written without contemplating much besides their basic purpose, and were tested and reworked multiple times to fix edge-case errors. While this was effective in producing a correct implementation in the end, it was a vastly time-consuming process. In the cases where time was spent thinking and identifying potential issues, noticeably less time was spent in testing and fixing errors.

\subsection{Avoiding perfectionism}
At many points during the project we've had to decide whether to continue trying to improve a particular aspect of the implementation. This was a difficult decision to make especially during the early stages of the implementation when there was no immediate pressure from deadlines, and implementation details were often polished more than they should have been. Later on during testing and evaluation there was the temptation to go back and improve a correct but unsatisfactory implementation of a component, despite having significantly less time. Ultimately, completing the project and evaluating it was more important than perfecting parts of it.

\section{Conclusion}
Overall, we believe that the project was successful in producing a Linux GCD alternative in the form of EDA, which was better in terms of memory consumption. We found that there was still more to be desired performance-wise, which we believe that with sufficient time we could have improved on.
\par
We also confirmed that the programming model of GCD was simpler than using threads directly, although potentially less efficient for its GCD and EDA implementations. However, due to the incompleteness of the experiment we gave directions for future research.