\chapter{PThread Workqueues library (libpwq)}\label{appdx:pwq}
The libpwq package offers a multi-priority thread pool by providing access to one blocking work queue for each of the 4 priorities supported. All of the work queues are arranged in a shared array with access to each of the queues protected by a spinlock. That being said, this thread pool is an example of a centralized work-sharing thread pool. The underlying queue structure is simply a serial singly-linked queue. In order to determine which queues have pending work items an atomic bitmask is used, with the position of a set bit corresponding to queue of that priority having pending work.
\par
As far are as thread management is concerned the work queues are divided into 2 classes - normal and over-commit - so there are actually 8 work queues in total and 2 global queue lists. The task scheduling algorithm used for normal queues respects resource constraints, while for over-commit the only thing considered is whether there are idle threads available. It should be noted that threads used by one of the queue classes will not be used by the other. We'll first focus on over-commit queues as their implementation is simpler than the normal queues.

\section{Over-commit queues}
Over-commit queues as mentioned previously use a very simple scheduling scheme and rather inefficient thread management strategy of just checking whether there are idle over-commit threads available. If there are none a new one is spawned, which will attempt to consume items from all over-commit queues until they are empty at which point it blocks waiting on a shared condition variable for a short period, exiting the thread on timing out. If idle threads are available the condition variable is signaled to wake up one of them. In order not to lose work items a signal count is maintained, similar to a semaphore. Refer to algorithm \ref{alg:ocwq} for more details. 
\par
One thing to note is that the both the enqueue operation and the worker thread will attempt to get a lock over the shared array of over-commit queues and its associated structures. This will result in multiple threads having to wait under contention further reinforcing the notion this should be used rarely, on top of the resource intensive thread management strategy.

\begin{algorithm}
\caption{Over-commit work queues thread management}
\label{alg:ocwq}

\begin{algorithmic}[1]
\Function{add\_work}{$ocwq, witem$}
\State $index\_bit \gets (1 << ocwq.index)$
\Lock ocwq\_list
	\SpinLock ocwq
	\State \Call{enqueue}{$ocwq, witem$}
	\Unlock ocwq
	\State \Call{set}{$ocwq\_bitmask, index\_bit$}
	\If{$ocwq\_idle > 0$}
		\State \Call{signal}{$ocwq\_condvar$}
		\State $ocwq\_idle \gets ocwq\_idle - 1$
		\State $ocwq\_signals \gets ocwq\_signals + 1$
	\Else
		\State \Call{spawn}{$ocwq\_worker, NULL$}
	\EndIf
\Unlock ocwq\_list
\EndFunction

\State

\Function{ocwq\_worker}{$unused$}
\Lock ocwq\_list
\Loop
	\State $ocwq\_index \gets$ \Call{find\_first\_set}{ocwq\_bitmask}
	\If{$ocwq\_index \neq 0$}
		\State $ocwq \gets ocwq\_list_{ocwq\_index}$
		\If{\Call{head}{ocwq} $\neq NULL$}
				\State $witem \gets$ \Call{dequeue}{ocwq}
				\If{\Call{empty}{ocwq}}
					\State \Call{unset}{$ocwq\_bitmask, ocwq\_index$}
				\EndIf
				\Unlock ocwq\_list			
				\State \Call{work}{witem}
				\Lock ocwq\_list
				\State \textbf{continue}
		\EndIf
	\EndIf
	\State $ocwq\_idle \gets ocwq\_idle + 1$
	\State $timed\_out \gets$ \Call{timed\_wait}{$ocwq\_condvar, \mathbf{TIMEOUT}$}
	\If{$timed\_out$}
		\Unlock ocwq\_list
		\Return
	\EndIf
	\If{ $ocwq\_signals = 0$ }
		\State \textbf{continue}
	\EndIf
	\State $ocwq\_signals \gets ocwq\_signals - 1$	
\EndLoop
\EndFunction
\end{algorithmic}
\end{algorithm}

\section{Normal queues}
Normal queues have a significantly more complex thread management logic than over-commit queues. The first difference is that for normal queues threads spawning and termination is not managed by the threads themselves and when submitting new work items. There is a background manager thread, which operates as follows:
\begin{itemize}
\item Is created at system initialization and spawns an appropriate initial number of worker threads. After which it enters a endless loop waiting on a semaphore with a timeout of 1 second.
\item Is woken up if there are no idle workers check if a new worker thread should be spawned. This is done by tracking how workers are currently alive, the number of available processors, the and the current run queue of the CPU.
\item If the wait on the semaphore times out and there are idle threads, the manager calculates accumulated idle time based on the number of idle threads. A capped number of idle threads proportional to accumulated idle time are then stopped by sending them an empty work item.
\end{itemize}
\par
Adding work to a normal work queue is somewhat more involved than the equivalent operation for over-commit queues, as it does not involve the shared global lock for the list of work queues and the non-empty queues bitmask, unless signaling an idle thread. It has 3 main steps, with the first 2 protected by the queue's spinlock, as shown by \ref{alg:wq-enq}.
\par
\begin{algorithm}
\caption{Normal work queues enqueue}
\label{alg:wq-enq}
\begin{algorithmic}[1]
\Function{add\_work}{$wq, witem$}
\State $index\_bit \gets (1 << wq.index)$
	\SpinLock wq
	\If{\Call{empty}{$wq$}}
		\Repeat
			\State $newmask \gets$ \Call{atomic\_set}{$wq\_bitmask, index\_bit$} 
		\Until \Call{is\_set}{$wq\_bitmask, index\_bit$}
	\EndIf

	\State \Call{enqueue}{$wq, witem$}
	\Unlock wq

	\If{$wq\_idle > 0$}
		\Lock wq\_list
		\State \Call{signal}{$wq\_condvar$}
		\Unlock wq\_list
	\EndIf
\EndFunction
\end{algorithmic}
\end{algorithm}
As this doesn't require the global lock it should result in less contention across threads when trying to access multiple queues. Using a spinlock shouldn't be a bottleneck in the case when there aren't extreme amounts of contention as the enqueue operation is not expected to take significant amounts of time. The atomic update loop on the bitmask as other threads might modify it due to unrelated queues and because there is no synchronization with some of the operations when consuming.
\par
The worker threads attempting to consume from work queues take a tiered approach to dequeueing items. Firstly, a thread tries taking an item from the highest priority non-empty queue available.  In case of failure, the thread resorts to spinning while retrying this operation unless there are too many spinning threads. If no item could be obtained then the final option is a loop using a condition variable to wait for work items to be produced. Refer to algorithm \ref{alg:wq-deq} for more details.
\par
\begin{algorithm}[!htb]
\caption{Normal work queues dequeue}
\label{alg:wq-deq}
\begin{algorithmic}[1]

\Function{get\_work\_fast}{}
\State $wq\_index \gets$ \Call{find\_first\_set}{wq\_bitmask}
\If{$wq\_index = 0$}
	\Return NULL
\EndIf
\State $wq \gets wq\_list_{wq\_index}$
\SpinLock wq
\State $witem \gets$ \Call{dequeue}{$wq$}
\If{$witem = NULL \lor witem.func = NULL$}
	\Unlock wq
	\Return NULL
\EndIf
\If{\Call{empty}{$wq$}}
	\Repeat
		\State $newmask \gets$ \Call{atomic\_unset}{$wq\_bitmask, wq\_index$} 
	\Until \Call{is\_unset}{$wq\_bitmask, index\_bit$}
\EndIf
\Unlock wq
\Return witem
\EndFunction
\State

\Function{get\_work\_spin}{}
\If{\Call{atomic\_add}{$threads\_spinning, 1$} $\leq MAX\_SPINNING\_THREADS$}
	\Repeat
		\State $witem \gets$ \Call{get\_work\_fast}{}
	\Until{$witem \neq NULL$}
	\If{$idle\_threads == 1$}
		\Call{semaphore\_post}{manager\_sem}
	\EndIf
\EndIf
\Call{atomic\_sub}{$threads\_spinning, 1$}
\Return witem
\EndFunction
\State
\Function{get\_work\_wait}{}
\Lock wq\_list
\Repeat
	\State $witem \gets$ \Call{get\_work\_fast}{}
	\If{$witem = NULL$}
		\Call{wait}{wq\_condvar}
	\EndIf
\Until{$witem \neq NULL$}
\Unlock wq\_list
\If{$idle\_threads = 1$}
	\Call{semaphore\_post}{manager\_sem}
\EndIf
\Return witem
\EndFunction
\end{algorithmic}
\end{algorithm}	
The rationale behind this is to avoid having to lock all of the queues using the global lock and contend on that by doing an initial unprotected read on the ready queues bitmask. The consumer thread then only has to call these functions in the order presented in order to ensure that empty items used to terminate thread are received only after beginning to spin or wait. The semaphore is used to notify the manager thread that this was the last idling thread thus potentially causing new workers to be started if possible.
