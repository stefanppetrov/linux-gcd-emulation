\chapter{Design}

Having stated the requirements we can begin considering how the system should be designed and implemented in order to fulfill those requirements as effectively and efficiently as possible. Before discussing the design of the system we'll briefly discuss the choice of tooling for the project such as programming language and compiler. 
\par
To keep the design description as clean as possible we'll first consider a high-level architecture overview of the system showing the different modules of the system and how they are connected, and then move on to a more detailed description of the modules and why this specific architecture was chosen. 

\section{Tooling choice}
\subsection{Language}
The strengths of the implementation programming language should reflect the problem being solved when designing a system. Though there is certainly a large selection of programming languages to pick from, the best course of action is to consider which of them offer features suitable for the task at hand and which fit the project requirements.
\par
For this project we are rather limited in the selection of programming languages. As per the non-functional requirements in section \ref{sec:nfreq}, the system must be implemented as a shared or static library using the C programming language (using the GNU C11 standard), and optionally providing C++ implementation and interface.
\par
The first reason C11 was chosen was due to the support for atomic operations as part of the language\cite{c11-atomics}. The presence of standardized atomics allows for an easier implementation of important lock-free structures such as parallel queues, by virtue of abstracting the hardware instructions set specific atomic operations. Other reasons to choose C are that it is sufficiently low level to afford the programmer the opportunity for drastic optimizations and interfaces directly with the underlying operating system\rq{}s calls. Moreover, all Linux system services required by the project, such as \textbf{epoll}, are available only through C programming interfaces\cite{linux-epoll}. And finally, existing implementations of GCD are also written in C, even if providing interfaces for other languages such as Swift. In order to have a fair comparison the implementation of our library also needs to be written in C.
\par
Using C has several drawbacks nonetheless. As mentioned previously being unmanaged allows for optimizations, but also results in the language allowing unsafe constructs. This makes it easy to make errors leading to problems during execution such as heap corruption, memory leaks and use after free and data races. Also being a low-level language means that a number of abstractions which are desirable for the project such as reference counting are not available and would have to be implemented as a part of it. 
\par
As an alternative the C++ language was considered. While it is still an unmanaged compiled language, it allows the reuse of C code and libraries with C interfaces with minimal overhead and introduces several low-cost abstractions. As a result it is still capable of accessing system interfaces directly as C code would. Recent standards, ISO C++11 or newer, also provide everything included in the C11 standard, while also adding introducing language support for concepts such as class closures and shared pointers. Closures are important as they allow us to ignore the non-standard Block language extension for C, which is functionally the same. Shared pointers in particular also provide a form of reference counting as a part of the language, removing the need for it. Also if used correctly with atomics this prevents the ABA problem from occurring thus solving another potential issue.
\par
Another alternative considered was the Rust programming language. It is mostly compatible with C code and thus capable of using system interfaces, while also being compiled and unmanaged. However, its most distinctive advantage is that due to its novel type system it ensures that no data races can occur. Despite this it was considered mainly for future work due to the author's lack of experience with it and the language's relative immaturity.

\subsection{Compiler}
The compiler will be any version of the Clang compiler of the 4.0 series. This compiler was chosen as it supports the GNU C11 standard (the default setting) and also supports the \textit{blocks} language extension as used in GCD\cite{clang-man}. Although The GNU C Compiler (GCC) has support for a nested functions extension of its own, it is not suitable for parallel computation since calling a nested function after its enclosing lexical scope has returned results in undefined behaviour\cite{gcc-nested}. It should be noted that in order to get a fully functional support for \textit{blocks} in Clang one needs to link against the \textit{BlocksRuntime} static library, which is readily available in the package repository for the target Linux distribution\cite{libblocksrt}. Clang 4.0 also can compile C++ under ISO C++11 or more recent standards, which affords us more freedom should we choose C++ as the implementation language.

\section{System architecture}
Considering the high-level decomposition of the system into separate subsystems is an important software engineering practice as it allows the implementations of different subsystems to be independent of each other and thus replaceable without affecting the entire system.

\subsection{Analysis}\label{subsec:arch-analysis}
To design a system architecture we need to consider the different use cases and their flows of control in the system, based on the requirements from chapter \ref{chap:reqs}. That being said, the following primary use cases and their flows are then as follows:
\begin{enumerate}[label=\textbf{Use case \arabic*}, leftmargin=*]
	\item Task dispatch. \label{list:1}
	\begin{enumerate}[leftmargin=0pt, itemindent=0pt]
		\item Schedule a task on a queue.
		\item Schedule queue, if possible and needed, onto a thread according to their priority;
		\item If no idle thread is available decide whether a new one should be created based on load and resources.
		\item Run scheduled tasks on the queue and reschedule if needed based on queue type.
	\end{enumerate}
	\item Task groups. \label{list:2}
	\begin{enumerate}[leftmargin=0pt, itemindent=0pt]
		\item Schedule tasks to queues, as a part of a task group.
		\item \textit{(Optional)} Defer scheduling a task until the group has been completed.
		\item Wait for group to be completed.
		\item \textit{(Optional)} Schedule tasks deferred to completion.
	\end{enumerate}
	\item Parallel loops. \label{list:3}
	\begin{enumerate}[leftmargin=0pt, itemindent=0pt]
		\item Calculate task distribution depending on policy.
		\item Schedule tasks to a given queue and await completion.
	\end{enumerate}
	\item Event handling. \label{list:4}
	\begin{enumerate}[leftmargin=0pt, itemindent=0pt]
		\item Creating an event source and starting it attached to some queue;
		\item Event source gets registered with the system event notification mechanism;
		\item \textit{(Optional)} Registration handler begins to run;
		\item Events start being delivered to the source and handlers are scheduled, see \ref{list:1};
		\item Event source canceled intentionally or due to errors. Cancellation handler is scheduled( see \ref{list:1});
		\item Cancellation handler executes, and the event sources is removed from the the system event notification mechanism;
	\end{enumerate}
\end{enumerate}
This information is sufficient to identify that we need at least 1 component for each of the 4 use cases, with all of them depending on the queue component. Additionally a queue to thread scheduler, thread manager and event manager components will be required to satisfy the needs of the queue and the event source modules. 
\par
Although the flows already give us a minimal set of components, it is by no means exhaustive. For performance or complexity management reasons we might need to have additional subsystems.

\subsection{Overview}
As per the analysis carried out in the previous section we now propose the following layered architecture for EDA as shown in figure \ref{fig:arch-overview}. Discussion of the responsibilities of each module and it's relation to others will be given in section \ref{subsec:components}.
\begin{figure}[!htb]
\centering
\includegraphics[width=\columnwidth]{arch}
\caption{System architecture overview}
\label{fig:arch-overview}
\end{figure}

\subsection{Components}\label{subsec:components}
Having provided an architectural overview we now discuss the different modules as displayed in figure \ref{fig:arch-overview} in more detail, by stating exactly what their responsibilities are and giving possible directions for their implementation.

\subsubsection{Queue manager}
Queue management is one of the central components of the library as the majority of the functionality it provides is found within this component, in addition to other components depending on it. Additionally, if not implemented efficiently it is a potential bottleneck for the entire functionality of the library. This component is concerned with providing the user interface for queue operations and maintaining the correct internal state of queues.
\par
This module needs to provide at least an user-facing interfaces for accessing system defined public queues, creating new queues according to a specified set of attributes, and submitting tasks for asynchronous and synchronous execution. Optionally, capabilities for suspending the execution of tasks on the queue and setting a limit on parallel consumers can also be supported. An important attribute that should be supported by this interface is the scheduling type of the queue - whether it is cooperative, non-cooperative or greedy as defined in the requirement section for queues \ref{list:freqs-queue}. Choosing the right scheduling type can affect efficiency when using a queue - for example an non-contended low priority queue will impact performance negatively if it uses the non-cooperative or the greedy scheduling attribute, but a frequently used queue can benefit from being of those types as yielding it will not have to yield threads frequently slowing its execution.
\par
From a correctness perspective this component has to guarantee that no task items will be lost as a result from parallel enqueues and dequeues to a given queue, no tasks remain on a queue indefinitely while there are consumers, and that constraints such as strictly serial execution of queue tasks or the upper bound of parallel queue consumers are maintained.
\par
It is clear that in order to implement the functionality for queues correctly some parallel queue structure will need to be used. Additionally, it will have to be sufficiently efficient so as not to bottleneck the rest of the system.

\subsubsection{Event manager}\label{subsubsec:arch-ev}
The event manager is the second most important component which provides user-facing functionality. It provides the interfaces for managing event sources, namely defining an event source, specifying its event handler and other associated callbacks, and canceling the event sources. It should also provide support for at least the event types specified in \ref{subsec:freqs}.
\par
Internally to the library this module is also responsible for maintaining the mapping between the user-facing event source objects and the native system events supported by \textbf{epoll}. A key design decision is what structures should be used for the mapping. Allowing multiple threads to modify them will require use of structures safely usable in a parallel context, but will result in increased complexity and thus the margins for error.
\par
A more preferable approach would be giving a single thread exclusive ownership to the mapping structures, and allowing modification only by sending messages to that thread. This allows the use of simpler sequential constructs and using a specialized queue for message passing, however potentially being a bottleneck on event handling. It is possible to scale this by having multiple threads with private event infrastructure, but event sources would have to be load balanced across event loops. The problem with load balancing event sources is that for events from sources such as TCP sockets or signals, the events might arrive at irregular and unpredictable schedules making it an undertaking beyond the current scope of the project.
\par
An important and closely related consideration is that \textbf{epoll} is a blocking system call. As it is unacceptable to run it on threads whose purpose is to service other queues, since it might block for an unspecified amount of time. This mandates having to use an event loop on its own dedicated thread or multiple such. However, having 2 or more event loop threads is not desirable as they would require shared access to event mapping structures as discussed in the paragraph above. 
\par
More problems with having more than one event loop thread, is the system being susceptible to the ``thundering herd'' \cite{thunder, epoll-ctl} and starvation issues, especially if sharing an \textbf{epoll} instance. If \textbf{epoll} instances are private there is still the problem of fair event source distribution. Therefore the preferred solution is still a single event management thread, but this means that update messages will have to be integrated with the event loop in some manner.

\subsubsection{Thread manager}\label{subsubsec:arch-tm}
This is the first core component which the potential user of the library has no direct no interaction with, however it is one of the most important as it determines how efficiently the processing resources of the underlying hardware will be utilized. The most suitable metaphor for this specific module is a thread pool as it is maintaining a reasonable number of threads with respect to application and system load given resource constraints.
\par
Regardless of how exactly it is implemented it should support scheduling queues on multiple priority levels, and allow threads to be spawned beyond resource constrains if required, as specified by queue attributes. Having different task priorities is a requirement which should be considered careful as it has implications on the efficiency / complexity trade-off in the thread pool design. Thread pools with a central task queue are generally less complex, but at the price of  having a bottleneck in the shared queue. 
\par
Private queue thread pools, such as the ones based on work stealing\cite{blumofe99steal} can be more efficient due to not being bottlenecked, but are more challenging to implement. Adapting complex data structures to provide a specific behaviour they were not originally intended to have, and doing so without losing any of their beneficial qualities is challenging if not impossible in some cases. Therefore for this project mainly centralized thread pool designs will be considered.
\par
It is important to note that even though this module is concerned with thread management it will still rely on the native PThreads implementation for providing basic threading operations such as creating and starting new threads.

\subsubsection{Queue scheduling}
In the light of the discussion about the queue management and thread management components, the queue scheduling component appears superfluous. The thread manager handles the details of mapping queues to threads, while the queue module itself handles other scheduling constraints of the queues.
\par
Nonetheless, it is still useful to consider a separate queue scheduling layer. The thread manager can be treated as a black box oblivious to the higher layer queue operations, thus requiring either the queue module to communicate directly with it potentially increasing the complexity of that module or adding an intermediate layer to hide the details of what thread management implementation is used.
\par
One more reason to have an intermediate layer for queue scheduling is that queues may not necessarily be scheduled directly to the underlying thread management mechanism. Within the scope of the project an example of this is the optional hierarchical organization of queues where user defined queues are scheduled by submitting them to a parent queue, which is either a system defined queue or another user-defined one. Another example which might require additional logic based on queue information is taking the cooperative, non-cooperative and greedy queue attributes into account.

\subsubsection{Task cache}
Something which is not evident from the work flows is that every time that a thread wants to schedule a task to queue, memory to hold that item has to be allocated. It was discussed in the technology review (section \ref{subsubsec:malloc}) that dynamic allocations are a performance bottleneck in a parallel context.
\par
Task items will be the most frequently allocated objects and as it stands we have the most to gain from optimizing their allocation. The purpose of this subsystem is to implement some form of application specific allocation for task items. In order to avoid going beyond the scope of the project this will not be a full-blown parallel allocator (although the underlying system allocator might be parallel), but rather some form of allocation cache / memory pool.

\subsubsection{Loop parallelization}
This module is primarily built on the functionality of the queue module. The most significant challenge it needs to address is the loop parallelization strategies which will be used. A good loop parallelization strategy should have good load balancing between the executing threads, but should also keep communication between them minimal after the initial loop distribution.
\par
The two policies of work distribution considered are distributing loop iterations statically before beginning execution, and dynamically during execution. The static approach allows minimal communication between executing threads, but with poor load balancing in some use cases, while the  dynamic one requires more communication but an improved load balance. This was inspired by OpenMP's  loop scheduling parameters\cite{intel-omp-sched}. 
\par
Granularity should also be considered, but determining it automatically is a difficult if not impossible task without compiler support as the loop body can be any block of code, containing multiple nested loops or recursion. As such it is largely out of the scope of the project, aside from potentially supporting user-specified granularity.

\subsubsection{Group manager}
As with the loop parallelization module this one extends the functionality of the queue module, but by providing a new synchronization tool - the task group. The main use case of task groups is waiting on their constituent tasks to finish execution, either in a blocking or non-blocking fashion. As such task groups can be thought of as ``inverted'' semaphores, where a waiting on the semaphore blocks only when its value is higher than 0. However, there is also the difference that groups offer synchronization in a non-blocking fashion by allowing users to specify callback tasks to be submitted to a queue, after the groups has finished execution.
\par
As such group objects will have to maintain 3 control structures - tasks which are a part of the group and are currently executing, tasks which are blocked on the group completing, and callbacks whose scheduling and execution is deferred until group completion. The first 2 do not require any specific constructs beyond thread-safe counters and perhaps a semaphore or a condition variable to wake-up waiters, but the callbacks need to be stored as some parallel collection in the group object. It can be done by reusing the same structures as the queues, but since there are no ordering requirements other simpler and potentially more efficient structures which can be used.
\par
A pitfall of the task group concept is that it allows deadlocks regardless of the implementation. Consider the case where a task submitted as a part of the group does a blocking wait on the group completion. Since it is a part of the group it will wait until program termination, deadlocking other threads which wait on the same group, and preventing any of its deferred callbacks from being scheduled.

\subsubsection{Other}
It should be noted that there are some requirements that we have not covered in the analysis subsection (\ref{subsec:arch-analysis}) and are not represented by any module in figure \ref{fig:arch-overview}. The most notable such are reference counting of library objects, library managed semaphores, and one-time execution of parallel sections of code. 
\par
This is not due to neglecting to consider them, but due to them being too small to be considered as stand-alone modules, in the case of one-time execution functionality, being pervasive to all other functionality, as with reference counting of library objects, or being low-priority and orthogonal to the rest of the library in the case of managed semaphores.
\par
Manual reference counting was found to be a desirable functionality as determining the lifetime of objects used in parallel and safely reclaiming resources used by them is a difficult task. For example consider freeing a dispatch queue - it can be freed safely only in the scope which created it, after we are certain that it is no longer used it in parallel which is not always possible. It also cannot be freed during an execution of a task associated with it as it would still be in use at that point. Therefore some form of garbage collection is needed, and reference counting is the least intrusive way to do it since it does not require writing a complete garbage collector.
\par
Managed semaphores are needed in order to have reference counted semaphores and an abstraction over whether we are using system provided semaphores or a different implementation.
\par
Guaranteed one-time execution is useful when it is possible to have multiple threads racing to initialize a module or an object and the initialization is not safe to re-enter.
